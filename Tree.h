#ifndef M_EPS
#define M_EPS 0.00001
#endif
#pragma once
#include <deque>
#include <Geometry/BoundingBox.h>
#include "tbb/task_group.h"
#include "Geometry/Material.h"

class Tree
{
	
private:
	//etat feuille, racine + profondeur du noeud
	bool m_leaf;
	bool m_root;
	unsigned int m_depth;

	//compteur qui permet de savoir le nombre de noeuds existants dans le programme
	static unsigned int cpt;

	//les primitives du noeud
	::std::vector<const Geometry::Primitive*> m_primitives;

	unsigned int m_nbPrimitives;

	//la boite englobante du noeud
	Geometry::BoundingBox m_box;

	//fils droit et fils gauche
	Tree* Tr;
	Tree* Tl;


	struct CompPrimitiveAxis{
		int axis;
		CompPrimitiveAxis(int axis) { this->axis = axis; }
		bool operator () (const Geometry::Primitive * t1, const Geometry::Primitive * t2) {
			return (t1->center()[axis] < t2->center()[axis]);
		}
	};


public:

	//creation d'un noeud seul
	//(primitives = deque vide, boite englobante = espace vide)
	Tree()
	{
		//::std::cout << "Le tree d'adresse " << this << " a ete cree (numero " << cpt << ")" << ::std::endl;
		++cpt;
		m_leaf = true;
		m_root = true;
		m_depth = 0;
		m_nbPrimitives = 0;
		m_box = Geometry::BoundingBox();
	}

	//etat feuille, racine + profondeur du noeud
	bool leaf() { return m_leaf; }
	bool root() { return m_root; }
	unsigned int depth(){ return m_depth; }
	
	//remplace les primitives par celles donnees en parametre
	void setPrimitives(const ::std::vector<const Geometry::Primitive*> & prim) {
		m_primitives = prim;
		for (const Geometry::Primitive* t : m_primitives) {
			m_box.update(t);
		}
		m_nbPrimitives = m_primitives.size();
	}

	//ajoute une primitive donnee en parametre
	void addPrimitive(const Geometry::Primitive * p) {
		assert(m_leaf && "On ne peut ajouter des primitives qu'a une feuille");
		m_primitives.push_back(p);
		m_box.update(p);
		++m_nbPrimitives;
	}

	//hauteur maximale du tree
	int height() const{
		return m_leaf ? 0 : ::std::max(Tr->height(),Tl->height()) + 1;
	}
	//nombre de noeuds du tree
	int size() const{
		return m_leaf ? 1 : Tr->size() +  Tl->size() + 1;
	}
	//nombre de feuilles du tree
	int leaves() const {
		return m_leaf ? 1 : Tr->leaves() + Tl->leaves();
	}

	//volume de la boite englobante du tree
	int nbTriangles() const {
		return m_leaf ? m_nbPrimitives : Tr->nbTriangles() + Tl->nbTriangles();
	}
	


	/*Construit recursivement la descendence du tree tant que le nombre de primitive n'est inferieur a celui donne en parametre
	*
	*param min_vertices : nombre de primitive qui est le seuil d'arret de la construction
	*/
	void build_median(unsigned int min_vertices) {
		assert(m_leaf && "Un arbre doit etre construit a partir d'un noeud sans fils");
		//si le nombre de primitives est assez faible, on s'arrete
		if (m_primitives.size() <= min_vertices) return;
		//allocations des fils
		Tr = new Tree(); Tl = new Tree();
		//decoupage -------------------------------------------
		//l'axe de decoupe choisit en fonction de la profondeur du noeud
		int axis = m_depth % 3;
		::std::sort(m_primitives.begin(), m_primitives.end(), CompPrimitiveAxis(axis));
		for (int i = 0; i < m_nbPrimitives; ++i)
			(i >= m_nbPrimitives / 2 ? Tl : Tr)->addPrimitive(m_primitives[i]);
		
		//actualisations--------------------------------------
		//...sur les fils
		//leur profondeur est incr�ment�e
		Tr->m_depth = m_depth + 1;
		Tl->m_depth = m_depth + 1;
		//ils ne sont pas des racines
		Tr->m_root = false;
		Tl->m_root = false;
		//...sur ce tree
		//ce tree n'est plus une feuille
		m_leaf = false;
		//et donc ce tree n'a plus besoin d'avoir des pointeurs vers des triangles
		m_primitives.clear();
		//on construit les fils
				//CONSTRUCTION DES FILS
		tbb::task_group g;
		g.run([&] {Tr->build_median(min_vertices); });
		g.run([&] {Tl->build_median(min_vertices); });
		g.wait();
		//Tr->build_median(min_vertices);
		//Tl->build_median(min_vertices);
		Tr->m_box.bump();
		Tl->m_box.bump();
	}

	/*Construit recursivement la descendence du tree tant que le nombre de primitive n'est inferieur a celui donne en parametre
	*en utilisant la SAH (surface area heuristic)
	*param c_t : 
	*/
	void build_SAH(double c_t, double c_i, unsigned int min_vertices) {
		assert(m_leaf && "Un arbre doit etre construit a partir d'un noeud sans fils");
		if (m_nbPrimitives <= min_vertices) {
			return;
		}

		//initialisation des meilleurs param�tres que l'on recherche et de la surface de la boite actuelle
		int best_axis = 0;
		int best_index = 0;
		double c_best = ::std::numeric_limits<double>::max();
		double Sb = m_box.surface_area();
		
		//RECHERCHE DE L'AXE DE DECOUPE ET DE L'INDEX DANS m_triangles QUI MINIMISE LE COUT
		//pour chaque axe de decoupe possible
//#pragma omp parallel for schedule(dynamic)//, 10)//guided)//dynamic)
		for (int axis = 0; axis < 3; ++axis) {

			//On construit au pr�alable toutes les BoundingBox que l'on va utiliser 
			::std::vector<Geometry::BoundingBox> boxes_left(1);
			::std::vector<Geometry::BoundingBox> boxes_right(1);
			::std::sort(m_primitives.begin(), m_primitives.end(), CompPrimitiveAxis(axis)); // On trie les triangles en fonction de l'axe de decoupe courant
			for (int i = 0; i < m_nbPrimitives; ++i) {
				Geometry::BoundingBox bl = boxes_left.back();
				bl.update(m_primitives[i]);
				boxes_left.push_back(bl);
				Geometry::BoundingBox br = boxes_right.back();
				br.update(m_primitives[m_nbPrimitives - i - 1]);
				boxes_right.push_back(br);
			}

			//Pour chaque indice de triangle, on calcule le cout, si il est inferieur au record, on sauve les param�tres
			for (int k = 0 ; k < m_nbPrimitives ; ++k) {
				int Nr = m_nbPrimitives-k, Nl = k+1;
				double Sbl = boxes_left[k+1].surface_area();
				double Sbr = boxes_right[m_nbPrimitives - k].surface_area();
				double c = c_t + (c_i / Sb)*(Sbl*Nl + Sbr * Nr);
				if (c < c_best) {
					c_best = c;
					best_axis = axis;
					best_index = k;	
				}
			}
		}
		//CAS D'ARRET
		//Si la decoupe "ne vaut pas le coup", on s'arrete
		if (c_best >= m_nbPrimitives * c_i) {
			return;
		}

		//RECONSTRUCTION DES PARTITIONS QUI MINIMISENT LE COUT
		//on a l'axe de decoupe et l'indice de decoupe, on n'a plus qu'a r�partir les triangles dans les fils
		::std::sort(m_primitives.begin(), m_primitives.end(), CompPrimitiveAxis(best_axis));
		Tr = new Tree(); Tl = new Tree();	
		for (int i = 0; i < m_nbPrimitives; ++i) {
			(i >= best_index ? Tr : Tl)->addPrimitive(m_primitives[i]);
		}
		//ACTUALISATIONS DES ATTRIBUTS
		//...sur les fils
		
		//leur profondeur est incr�ment�e
		Tr->m_depth = m_depth + 1;
		Tl->m_depth = m_depth + 1;
		//ils ne sont pas des racines
		Tr->m_root = false;
		Tl->m_root = false;
		//...sur ce tree
		//ce tree n'est plus une feuille
		m_leaf = false;
		//et donc ce tree n'a plus besoin d'avoir des pointeurs vers des triangles -> optimisation de la quantit� de memoire utilis�e
		m_primitives.clear();

		//CONSTRUCTION DES FILS
		tbb::task_group g;
		g.run([&] {Tr->build_SAH(c_t, c_i, min_vertices); });
		g.run([&] {Tl->build_SAH(c_t, c_i, min_vertices); });
		g.wait();
//#pragma omp parallel
//		{
//#pragma omp single 
//			Tr->build_SAH(c_t, c_i, min_vertices);
//#pragma omp single
//			Tl->build_SAH(c_t, c_i, min_vertices);		
//		}
		Tr->m_box.bump();
		Tl->m_box.bump();
	}

	/*Methode remplissant le casted ray donn�
	* Renvoie vrai si le cray a touche un triangle
	* param cr : casted ray
	* param [t0, t1] : intervalle de recherche
	* param t_i : t d'intersection
	*
	*/
	void intersect(Geometry::CastedRay & cr, double t0, double t1, double & t_i) const {
		if (m_leaf)
		{
			for (const Geometry::Primitive * P : m_primitives)
				cr.intersect(P);
		}
		else
		{
			double t_entry_r, t_exit_r, t_entry_l, t_exit_l, t_i;
			bool inter_r = Tr->m_box.intersect(cr, t0, t1, t_entry_r, t_exit_r);
			bool inter_l = Tl->m_box.intersect(cr, t0, t1, t_entry_l, t_exit_l);
			if(inter_l)
			{
				if (inter_r)
				{
					if (t_entry_l < t_entry_r) //la boite de gauche est touch�e en premi�re
					{
						Tl->intersect(cr, t_entry_l, t_exit_l, t_i);
						if (cr.validIntersectionFound())
						{
							if (cr.intersectionFound().tRayValue() > t_entry_r)
							{
								Tr->intersect(cr, t_entry_r, t_exit_r, t_i);
							}
						}
						else
						{
							Tr->intersect(cr, t_entry_r, t_exit_r, t_i);
						}
					}
					else //la boite de droite est touch�e en premi�re
					{
						Tr->intersect(cr, t_entry_r, t_exit_r, t_i);
						if (cr.validIntersectionFound())
						{
							if (cr.intersectionFound().tRayValue() > t_entry_l)
							{
								Tl->intersect(cr, t_entry_l, t_exit_l, t_i);
							}
						}
						else
						{
							Tl->intersect(cr, t_entry_l, t_exit_l, t_i);
						}
					}			
				}
				else
				{
					Tl->intersect(cr, t_entry_l, t_exit_l, t_i);
				}
			}
			else
			{
				if (inter_r)
				{
					Tr->intersect(cr, t_entry_r, t_exit_r, t_i);
				}
				else
				{
					return;
				}
				
			}		
		}
	}

	/*Methode d�terminant si une primitive est pr�sente dans l'intevalle min , max
	* Renvoie vrai si le cray a touche une primitive
	* param cr : casted ray
	* param [t0, t1] : intervalle de recherche
	* param t_i : t d'intersection
	* param min, max : intervalle de recherche global
	*/
	bool intersectObstructed(Geometry::CastedRay & cr, double t0, double t1, double & t_i, double min, double max, const Geometry::Material * mat = nullptr) const {
		if (m_leaf)
		{
			for (const Geometry::Primitive * P : m_primitives) {
				cr.intersect(P);
				if (cr.validIntersectionFound()) {
					Geometry::RayPrimitiveIntersection rpi = cr.intersectionFound();
					double dist = rpi.tRayValue();
					const Geometry::Material * mat_ = rpi.primitive()->material();
					//On ne veut pas dire que c'est obstru� si on recroise le meme material !
//					if (!rand() && mat_ == mat && mat) {
//#pragma omp critical 
//						std::cout << mat << std::endl;
//					} 
					if(mat_ != mat && dist > min && dist < max)
						return true;
				}			
			}
			return false;
		}
		else
		{
			double t_entry_r, t_exit_r, t_entry_l, t_exit_l, t_i;
			bool inter_r = Tr->m_box.intersect(cr, t0, t1, t_entry_r, t_exit_r);
			bool inter_l = Tl->m_box.intersect(cr, t0, t1, t_entry_l, t_exit_l);
			if (inter_l)
			{
				if (inter_r)
				{
					if (t_entry_l < t_entry_r) //la boite de gauche est touch�e en premi�re
					{
						bool found = Tl->intersectObstructed(cr, t_entry_l, t_exit_l, t_i, min, max, mat);
						if (found) return true;
						return Tr->intersectObstructed(cr, t_entry_r, t_exit_r, t_i, min, max, mat);
					}
					else //la boite de droite est touch�e en premi�re
					{
						bool found = Tr->intersectObstructed(cr, t_entry_r, t_exit_r, t_i, min, max, mat);
						if (found) return true;
						return Tl->intersectObstructed(cr, t_entry_l, t_exit_l, t_i, min, max, mat);
					}
				}
				else
				{
					return Tl->intersectObstructed(cr, t_entry_l, t_exit_l, t_i, min, max, mat);
				}
			}
			else
			{
				if (inter_r)
				{
					return Tr->intersectObstructed(cr, t_entry_r, t_exit_r, t_i, min, max, mat);
				}
				return false;
			}
		}
	}


	int NbBoxHit(Geometry::Ray const & ray, Tree * T) {
		if (T->m_leaf) 
			return 1;
		else {
			double a, b;
			bool left = T->Tl->m_box.intersect(ray, 0, ::std::numeric_limits<double>::max(), a, b);
			bool right = T->Tr->m_box.intersect(ray, 0, ::std::numeric_limits<double>::max(), a, b);
			
			if(left && right) 
				return 1+ NbBoxHit(ray, T->Tl)+ NbBoxHit(ray, T->Tr);
			
			else if (left) 
				return 1 + NbBoxHit(ray, T->Tl);
			
			else if (right) 
				return 1 + NbBoxHit(ray, T->Tr);
			
			else 
				return 1;
		}
	}

	//destructeur
	~Tree() {
		if (!m_leaf) {
			delete Tr;
			delete Tl;
		}
		--cpt;
	}
};

