#ifndef _Geometry_TesselatedCylinder_H
#define _Geometry_TesselatedCylinder_H

#include <Geometry/Geometry.h>
#include <Geometry/Material.h>

namespace Geometry
{
	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// \class	TessellatedCylinder
	///
	/// \brief	A cylinder centered in (0,0,0), 1.0 height, 1.0 diameter.
	///
	/// \author	F. Lamarche, Université de Rennes 1
	/// \date	04/12/2013
	////////////////////////////////////////////////////////////////////////////////////////////////////
	class TessellatedCylinder : public Geometry
	{
	public:

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	TessellatedCylinder::TessellatedCylinder(int nbDiv, double scaleDown, double scaleUp, Material * material)
		///
		/// \brief	Constructor
		///
		/// \author	F. Lamarche, Université de Rennes 1
		/// \date	04/12/2013
		///
		/// \param	nbDiv	 	Number of base circle subdivisions.
		/// \param	scaleDown	Radius of the bottom circle.
		/// \param	scaleUp  	Radius of the top circle.
		/// \param	material 	The material.
		////////////////////////////////////////////////////////////////////////////////////////////////////
		TessellatedCylinder(int nbDiv, double scaleDown, double scaleUp, Material * material)
		{
			TessellatedDisk disk1(nbDiv, material) ;
			disk1.scale(scaleUp) ;
			disk1.translate(Math::makeVector(0.0f, 0.0f, 0.5f)) ;

			TessellatedDisk disk2(nbDiv, material) ;
			disk2.scale(scaleDown) ;
			disk2.translate(Math::makeVector(0.0f, 0.0f, -0.5f)) ;

			merge(disk1) ;
			merge(disk2) ;

			for(int cpt=0 ; cpt<nbDiv ; cpt++)
			{
				addTriangle(disk1.getVertices()[cpt], disk1.getVertices()[(cpt+1)%nbDiv], disk2.getVertices()[cpt], material) ;
				addTriangle(disk1.getVertices()[(cpt+1)%nbDiv], disk2.getVertices()[cpt], disk2.getVertices()[(cpt+1)%nbDiv], material) ;
				//addTriangle(new Triangle(disk1.getVertices()[cpt], disk1.getVertices()[(cpt+1)%nbDiv], 
				//			disk2.getVertices()[cpt], material)) ;
				//addTriangle(new Triangle(disk1.getVertices()[(cpt+1)%nbDiv], disk2.getVertices()[cpt], 
				//			disk2.getVertices()[(cpt+1)%nbDiv], material)) ;
			}
		}
	};
}

#endif
