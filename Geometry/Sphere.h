#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#ifndef M_TAU
#define M_TAU 6.28318530717958647692
#endif
#pragma once
#include "Primitive.h"
#include "math.h"
#include <cmath>
#include <random>
#include <Math/Quaternion.h>
namespace Geometry
{
	class Sphere final : public Primitive
	{

	protected:
		Math::Vector3f m_center;
		double m_radius;
		double m_radius2;
		double m_surface;

	public:


		Sphere()
		{
			m_vertex = { &m_center };
		}
		Sphere(Math::Vector3f * center, double radius = 1, Material * mat = nullptr)
			:Primitive(mat),
			m_center(*center),
			m_radius(radius)
		{
			m_vertex = { center };
			m_radius2 = pow(m_radius, 2);
			m_surface = 2 * M_TAU * m_radius;
		}
		virtual ~Sphere() {

		}

		Primitive* create(const std::vector<Math::Vector3f*> & v,
			const std::vector<Math::Vector2f*> & t,
			Material * material,
			const Math::Vector3f* normals
		) const
		{
			return new Sphere(v[0], m_radius,material);	
		}
		Primitive* clone() const {
			return new Sphere(*this);
		}

		void update()
		{
			m_center = *m_vertex[0];
			m_radius2 = pow(m_radius, 2);
			m_surface = 2 * M_TAU * m_radius;
		}

		void scale(double s)
		{
			m_radius *= s;
			m_radius2 = pow(m_radius, 2);
		}

		Math::Vector3f const & vertex(int i) const
		{
			return m_center;
		}


		Math::Vector3f center() const
		{
			return m_center;
		}

		RGBColor sampleTexture(double u, double v, const Math::Vector3f & intersection) const
		{
			if (m_material->hasTexture())
			{
				Math::Vector3f n = (intersection - m_center).normalized();
				// Rotation autour de x implicite pour faire correspondre le "haut" (min v) de la texture et le "haut" (max z) de la sphere 
				u = atan2(-n[1], -n[0]) / M_TAU + 0.5;
				v = 0.5 - asin(n[2]) / M_PI;
				//u = atan2(n[0], n[2]) / M_TAU + 0.5;
				//v = 0.5 - asin(n[1]) / M_PI;
				RGBColor texel = m_material->getTexture().pixel(Math::makeVector(u,v));
				return texel;
			}
			return RGBColor(1.0f, 1.0f, 1.0f);
		}

		Math::Vector3f sampleNormalMap(const Math::Vector3f & intersection, double u, double v, const Math::Vector3f & normal) const
		{
			Math::Vector3f new_normal = normal;
			if (m_material->hasNormalMap()) {
				Math::Vector3f n = (intersection - m_center).normalized();
				u = atan2(-n[1], -n[0]) / M_TAU + 0.5;
				v = 0.5 - asin(n[2]) / M_PI;
				RGBColor col = m_material->getNormalMap().pixel(Math::makeVector(u,v));
				Math::Vector3f shifted;
				Math::Vector3f up = Math::makeVector(0, 0, 1);
				shifted[0] = col[0];
				shifted[1] = col[1];
				shifted[2] = col[2];
				shifted = shifted * 2 - 1;
				shifted = shifted.normalized();
				double cos = up * normal;
				double angle = acos(cos);
				Math::Vector3f axis = up ^ normal;
				Math::Quaternion<double> quat = Math::Quaternion<double>(axis, angle);
				new_normal = quat.rotate(shifted);
			}
			return new_normal;
		}

		Math::Vector3f samplePoint(double u, double v) const
		{
			return Math::makeVector(0, 0, 0);
		}

		Math::Vector3f sampleNormal(const Math::Vector3f & intersection, double u, double v) const
		{
			Math::Vector3f result = (m_center - intersection).normalized();
			//if ((m_center - ray.source()).norm() > m_radius) normal = -normal;
			//if (result*ray.direction() > 0.0) result = -result;
			return result;
		}




		bool intersection(Ray const & r, double & t, double & u, double & v) const
		{
			const Math::Vector3f & s = r.source();
			const Math::Vector3f & d = r.direction();
			const Math::Vector3f & diff = s - m_center;
			
			double b = d*diff*2;
			double c = s.norm2() + m_center.norm2() - (s * m_center) * 2 - m_radius2;
			double det = pow(b, 2) - 4 * c;
			if (det < 0)
			{
				return false;
			}
			const double sqrtdet = sqrt(det);
			t = (-b - sqrtdet) / 2.;
			if (t < 0)
			{
				t = (-b + sqrtdet) / 2.;
			}
			return t >= 0;

		}

		double surface() const
		{
			return m_surface;
		}

		Math::Vector3f randomUV(const Math::Vector3f & viewer) const
		{
			return randomPoint(viewer); // On ment : on renvoie un point 3d de la sphere
		}
		Math::Vector3f randomUV(double ubase, double vbase, double step, const Math::Vector3f & viewer) const
		{
			return randomPoint(ubase, vbase, step, viewer); // On ment : on renvoie un point 3d de la sphere
		}

		Math::Vector3f UVFromPoint(double u, double v, const Math::Vector3f & intersection) const
		{
			return intersection; //mensonge
		}


		Math::Vector3f pointFromUV(const Math::Vector3f & point3d) const
		{
			return point3d; // On ment: on renvoie le point 3d recu en parametre
		}

		


		RGBColor sampleTexture(const Math::Vector3f & point3d ) const  // On ment : le parametre est un point3d et non un uv
		{
			return sampleTexture(0, 0, point3d);
		}

		Math::Vector3f randomPointCosinus(double ubase, double vbase, double step, const Math::Vector3f & dir, double m, const Math::Vector3f & viewer) const
		{
			Math::Vector3f res;
			int cpt = 0;
			do {
				// pas d'interet d'utiliser sobol car depend de la position du viewer
				// Phi
				const double ksi1 = ubase + (step*(double)rand() / (RAND_MAX));
				const double phi = M_TAU * ksi1;
				// Theta
				const double ksi2 = vbase + (step*(double)rand() / (RAND_MAX));
				const double theta = acos(pow(ksi2, 1. / (m + 1.))); // Pond�ration cosinus de facteur m
				// sin et cos
				const double sinTheta = sin(theta);
				const double cosTheta = cos(theta);
				const double cosPhi = cos(phi);
				const double sinPhi = sin(phi);
				// x y z
				const double x = sinTheta * cosPhi;
				const double y = sinTheta * sinPhi;
				const double z = cosTheta;
				// Point echantillonn� dans l'hemisphere 0 0 1 :
				Math::Vector3f point = Math::makeVector(x, y, z);
				// Il faut placer ce point dans l'hemisphere du viewer :
				const Math::Vector3f up = Math::makeVector(0, 0, 1);
				// Axe normal � up et � la direction voulue
				const Math::Vector3f axis = (dir ^ up).normalized();
				// Angnle entre les deux
				const double angle = acos(axis*up);
				// Rotation pour passer de up � dir, sur le point
				Math::Quaternion<double> rotation = Math::Quaternion<double>(axis, -angle);
				res = rotation.rotate(point);
			} while (res*(viewer - (m_center + res * m_radius)) < 0 && ++cpt <10);
			

			return m_center + res * m_radius; 
		}


		Math::Vector3f randomPoint(const Math::Vector3f & viewer) const
		{
			Math::Vector3f normal = (viewer - m_center).normalized();
			return randomPointCosinus(0,0,1,normal, 1, viewer);
		}
		Math::Vector3f randomPoint(double ubase, double vbase, double step, const Math::Vector3f & viewer) const
		{
			Math::Vector3f normal = (viewer - m_center).normalized();
			return randomPointCosinus(ubase, vbase, step, normal, 1, viewer);
		}

		virtual void boundingPoints(Math::Vector3f & min, Math::Vector3f & max) const
		{
			min = m_center - m_radius;
			max = m_center + m_radius;
		}

		bool isTriangle() const
		{
			return false;
		}
		
	};

}