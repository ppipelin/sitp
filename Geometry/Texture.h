#ifndef _Geometry_Texture_H
#define _Geometry_Texture_H

#include <string>
#include <SOIL.h>
#include <Geometry/RGBColor.h>
#include <Math/Vectorf.h>

namespace Geometry
{
	/// <summary>
	/// A texture class. This class loads a texture from the disk.
	/// </summary>
	class Texture
	{
	private:
		int m_width;
		int m_height;
		unsigned char * m_data;

	public:
		Texture(const ::std::string & filename)
		{
			m_data = SOIL_load_image(filename.c_str(), &m_width, &m_height, 0, SOIL_LOAD_RGB);
			if (m_data == NULL)
			{
				::std::cout << "Invalid texture file: " << filename << ::std::endl;
			}
		}

		~Texture()
		{
			SOIL_free_image_data(m_data);
		}

		bool isValid() const
		{
			return m_data != NULL && m_data != nullptr;
		}

		RGBColor pixel(int x, int y) const
		{
			while (x < 0) { x += m_width; }
			while (y < 0) { y += m_height; }
			x = x%m_width;
			y = y%m_height;
			int offset = y * 3 * m_width + x * 3;
			unsigned char r = m_data[offset];
			unsigned char g = m_data[offset + 1];
			unsigned char b = m_data[offset + 2];
			return RGBColor(double(r) / 255.0f, double(g) / 255.0f, double(b) / 255.0f);
		}

		

		RGBColor pixel(Math::Vector2f const & v) const
		{
			return pixel(int(v[0] * m_width), int(v[1] * m_height));
		}

		RGBColor pixel_bilinear(Math::Vector2f const & v) const
		{
			// Positions sur l'image (exacte)
			double x = v[0] * m_width;
			double y = v[1] * m_height;

			// Depassement en x et en y du pixel precedent
			double dx = x - std::floor(x);
			double dy = y - std::floor(y);
			// Marge en x et en y du pixel suivant
			double dx_ = 1 - dx;
			double dy_ = 1 - dy;
			
			// Coordonnées des quatres pixels
			double v0x = std::floor(x);
			double v0y = std::floor(y);
			double v1x = std::ceil(x);
			double v1y = std::floor(y);
			double v2x = std::floor(x);
			double v2y = std::ceil(y);
			double v3x = std::ceil(x);
			double v3y = std::ceil(y);

			// Couleurs des quatres pixels
			RGBColor v0 = pixel(v0x, v0y);
			RGBColor v1 = pixel(v1x, v1y);
			RGBColor v2 = pixel(v2x, v2y);
			RGBColor v3 = pixel(v3x, v3y);


			// Interpolation bilineaire
			return v0 * dx_ * dy_+ v1 * dy_ * dx + v2 * dy * dx_ + v3 * dx * dy;
		}
	};
}

#endif