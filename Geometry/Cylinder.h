
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#ifndef M_TAU
#define M_TAU 6.28318530717958647692
#endif
#pragma once
#include "Geometry/Primitive.h"
#include <limits>

namespace Geometry
{
	class Cylinder final : public Primitive
	{
	protected:
		Math::Vector3f m_base;
		Math::Vector3f m_center;
		Math::Vector3f m_orientation;
		Math::Vector3f u_axis;
		Math::Vector3f v_axis;
		Math::Vector3f m_top;
		double m_h;
		double m_h2;
		double m_r;
		double m_r2;
		double m_surface;



	public:
		Cylinder() {
			m_vertex = { &m_base };
		}

		Cylinder(Math::Vector3f * base, Math::Vector3f * top, double r = 1.0, Material * mat = nullptr)
			:Primitive(mat),
			m_base(*base),
			m_top(*top),
			m_r(r)	
		{
			m_vertex = { base, top };
			update();
		}

		virtual ~Cylinder()
		{}

		Primitive* create(const std::vector<Math::Vector3f*> & v,
			const std::vector<Math::Vector2f*> & t,
			Material * material,
			const Math::Vector3f* normals
		) const
		{
			return new Cylinder(v[0],v[1], m_r, material);
		}
		Primitive* clone() const {
			return new Cylinder(*this);
		}

		void update() {
			Math::Vector3f base_to_top = m_top - m_base;
			// height
			m_h = base_to_top.norm();
			m_h2 = base_to_top.norm2();
			m_r2 = pow(m_r, 2);

			//Orientation
			m_orientation = (base_to_top).normalized();

			// Surface :
			m_surface = M_TAU * m_r * m_h;

			// Centre :
			m_center = (m_base + m_top) / 2.;

			// axes u et v
			Math::Vector3f rand_dir = Math::makeVector(double(rand()), double(rand()), double(rand())).normalized();
			u_axis = (rand_dir ^ m_orientation).normalized();
			v_axis = (u_axis ^ m_orientation).normalized();
			//u_axis = v_axis ^ m_orientation;

			//std::cout << "=========================" << std::endl;
			//std::cout << "base : " << m_base << std::endl;
			//std::cout << "top : " << m_top << std::endl;
			//std::cout << "orientation : " << m_orientation << std::endl;
			//std::cout << "u_axis : " << u_axis << std::endl;
			//std::cout << "v_axis : " << v_axis << std::endl;
			//std::cout << "h : " << m_h << std::endl;
			//std::cout << "h2 : " << m_h2 << std::endl;
			//std::cout << "r : " << m_r << std::endl;
			//std::cout << "r2 : " << m_r2 << std::endl;
			//std::cout << "m_surface : " << m_surface << std::endl;
			//Math::Vector3f min, max;
			//boundingPoints(min, max);
			//std::cout << "min : " << min << std::endl;
			//std::cout << "max : " << max << std::endl;


		}

		void scale(double s)
		{
			m_r *= s;
			m_h *= s;
			update();
		}

		Math::Vector3f const & vertex(int i) const
		{
			return m_base;
		}


		Math::Vector3f center() const
		{
			return m_center;
		}

		

		Math::Vector3f samplePoint(double u, double v) const
		{
			return pointFromUV(Math::makeVector(u,v,0.0));
		}

		Math::Vector3f sampleNormal(const Math::Vector3f & intersection, double u, double v) const
		{
			double l = (intersection - m_base) * m_orientation;
			Math::Vector3f c = m_base + m_orientation * l;
			return (intersection - c).normalized();
		}


		bool intersection(Ray const & r, double & t, double & u, double & v) const
		{
			Math::Vector3f d = r.direction();
			Math::Vector3f s = r.source();
			Math::Vector3f A = m_base;
			Math::Vector3f B = m_top;
			Math::Vector3f AB = B - A;
			Math::Vector3f AO = s - A;
			Math::Vector3f AOxAB = AO ^ AB;
			Math::Vector3f VxAB = d ^ AB;
			double ab2 = AB.norm2();
			double a = VxAB.norm2();
			double b = (AOxAB * VxAB) * 2;
			double c = AOxAB.norm2() - (m_r2 * ab2);
			double det = b * b - 4 * a * c;
			
			if (det < 0)
				return false;

			double sqrtdet = sqrt(det);
			t = (- b - sqrtdet) / (2 * a);
			if (t < 0)
			{
				t = (-b + sqrtdet) / (2 * a);
			}
			if (t < 0)
				return false;
			
			//premi�re intersection du cylindre infini
			Math::Vector3f intersection = s + d * t;      
			double dist = (intersection - m_base) * m_orientation;
			if (dist >= 0 && dist <= m_h)
				return true;
			
			//deuxieme intersection du cylindre infini
			t = (-b + sqrtdet) / (2 * a);
			if (t < 0)
				return false;
			
			intersection = s + d * t;
			dist = (intersection - m_base) * m_orientation;
			return (dist >= 0 && dist <= m_h);
		}

		double surface() const
		{
			return m_surface;
		}

		Math::Vector3f randomUV(const Math::Vector3f & viewer) const
		{
			return randomUV(0, 0, 1, viewer);
		}
		Math::Vector3f randomUV(double ubase, double vbase, double step, const Math::Vector3f & viewer) const
		{
			double ksi1 = ubase + step * double(rand()) / RAND_MAX;
			double ksi2 = vbase + step * double(rand()) / RAND_MAX;
			return Math::makeVector(ksi1, ksi2, 0.0);
		}

		Math::Vector3f UVFromPoint(double u, double v, const Math::Vector3f & intersection) const
		{
			double v_ = ((intersection - m_base) * m_orientation) / m_h;
			Math::Vector3f on_axis = m_base + m_orientation * v_ * m_h;
			Math::Vector3f axis_to_point = (intersection - on_axis).normalized();
			double cos = axis_to_point * u_axis;
			double sin = axis_to_point * v_axis;

			double u_;
			if (cos > 0 && sin >= 0) {
				u_ = atan(sin / cos);
			}
			else if (cos > 0 && sin < 0) {
				u_ = atan(sin / cos) + M_TAU;
			}
			else{ // cos < 0
				u_ = atan(sin / cos) + M_PI;
			}

			// u de -pi � pi -> u de 0 � 1
			u_ =  (u_ / M_TAU) + 0.5; 
			
			return Math::makeVector(u_, v_, 0.0);
		}

		Math::Vector3f pointFromUV(const Math::Vector3f & uv) const
		{
			double u = uv[0];
			double angle = M_TAU * u;
			double v = uv[1];
			Math::Vector3f on_axis = m_base + m_orientation * m_h * v;
			Math::Vector3f axis_to_surface = u_axis  * cos(angle) + v_axis * sin(angle);
			return on_axis + axis_to_surface * m_r ;
		}

		RGBColor sampleTexture(const Math::Vector3f & uv) const  // On ment : le parametre est un point3d et non un uv
		{
			if (m_material->hasTexture())
			{
				return m_material->getTexture().pixel(Math::makeVector(uv[0], uv[1]));
			}
			return RGBColor(1.0f, 1.0f, 1.0f);
		}
		RGBColor sampleTexture(double u, double v, const Math::Vector3f & intersection) const
		{
			
			if (m_material->hasTexture())
			{
				Math::Vector3f uv = UVFromPoint(u, v, intersection);
				return m_material->getTexture().pixel(Math::makeVector(uv[0], uv[1]));
			}
			return RGBColor(1.0f, 1.0f, 1.0f);
		}

		Math::Vector3f sampleNormalMap(const Math::Vector3f & intersection, double u, double v, const Math::Vector3f & normal) const
		{
			Math::Vector3f new_normal = normal;
			if (m_material->hasNormalMap()) {
				Math::Vector3f uv = UVFromPoint(u, v, intersection);
				RGBColor col = m_material->getNormalMap().pixel(Math::makeVector(uv[0], uv[1]));
				Math::Vector3f shifted;
				Math::Vector3f up = Math::makeVector(0, 0, 1);
				shifted[0] = col[0];
				shifted[1] = col[1];
				shifted[2] = col[2];
				shifted = shifted * 2 - 1;
				shifted = shifted.normalized();
				double cos = up * normal;
				double angle = acos(cos);
				Math::Vector3f axis = up ^ normal;
				Math::Quaternion<double> quat = Math::Quaternion<double>(axis, angle);
				new_normal = quat.rotate(shifted);
			}
			return new_normal;
		}



		Math::Vector3f randomUniform(double ubase = 0.0, double vbase = 0.0, double step = 1.0) const
		{
			//double ksi1 = Math::Sobol::sample(m_index, 0, 0);
			//double ksi2 = Math::Sobol::sample(m_index, 1, 0);
			double ksi1 = double(rand()) / RAND_MAX;
			double ksi2 = double(rand()) / RAND_MAX;
			m_index++;
			double r1 = ubase + step * ksi1;
			double r2 = vbase + step * ksi2;
			double theta = r1 * M_TAU;
			double rho = r2 * m_h;
			Math::Vector3f start = m_orientation * rho;
			Math::Vector3f to_surface = (u_axis * cos(theta) + sin(theta) * sin(theta))*m_r;
			Math::Vector3f point = start + to_surface;
			Math::Vector3f global = point + m_base;
			return global;
		}

		Math::Vector3f randomPoint(const Math::Vector3f & viewer) const
		{
			return randomUniform();
		}
		Math::Vector3f randomPoint(double ubase, double vbase, double step, const Math::Vector3f & viewer) const
		{
			return randomUniform(ubase, vbase, step);
		}

		virtual void boundingPoints(Math::Vector3f & min, Math::Vector3f & max) const
		{

			Math::Vector3f e;
			e[0] = m_r * sqrt(1.0 - m_orientation[0] * m_orientation[0]);
			e[1] = m_r * sqrt(1.0 - m_orientation[1] * m_orientation[1]);
			e[2] = m_r * sqrt(1.0 - m_orientation[2] * m_orientation[2]);

			Math::Vector3f min_bottom = m_base - e;
			Math::Vector3f max_bottom = m_base + e;
			Math::Vector3f min_top = m_top - e;
			Math::Vector3f max_top = m_top + e;

			min[0] = std::min(min_bottom[0], min_top[0]);
			min[1] = std::min(min_bottom[1], min_top[1]);
			min[2] = std::min(min_bottom[2], min_top[2]);

			max[0] = std::max(max_bottom[0], max_top[0]);
			max[1] = std::max(max_bottom[1], max_top[1]);
			max[2] = std::max(max_bottom[2], max_top[2]);
		}

		bool isTriangle() const
		{
			return false;
		}


	};
}
