#ifndef _Geometry_Refraction_H
#define _Geometry_Refraction_H
#include <Math/Vectorf.h>

namespace Geometry
{
	// Calculer la Reflectance et la Transmittance � partir du mod�le de Fresnel (plus exact mais couteux)
	inline void computeRTFresnel(double n1, double n2, double cos_theta_i, double cos_theta_t, double & R, double & T)
	{
		double rn = (n1 * cos_theta_i - n2 * cos_theta_t) / (n1 * cos_theta_i + n2 * cos_theta_t);
		double rt = (n2 * cos_theta_i - n1 * cos_theta_t) / (n2 * cos_theta_i + n2 * cos_theta_t);
		rn *= rn;
		rt *= rt;
		R = (rn + rt)*0.5;
		T = 1.0 - R;
	}
	// Calculer la Reflectance et la Transmittance � partir du mod�le de Schlick (moins exact mais + rapide)
	inline void computeRTSchlick(double n1, double n2, double cos_theta_i, double cos_theta_t, double & R, double & T)
	{
		double R0 = pow((n1 - n2) / (n1 + n2), 2);
		double cos_theta = n1 <= n2 ? cos_theta_i : cos_theta_t;
		double X = pow(1 - cos_theta, 5);
		R = R0 + (1 - R0)*X;
		T = 1 - R;
	}
	// Calculer la Reflectance et la Transmittance de mani�re binaire si il y a reflection interne totale ou non
	inline void computeRTBinary(bool critical , double & R, double & T)
	{
		R = critical ? 1 : 0;
		T = 1 - R;
	}



	// Calcule la direction du rayon r�fract� 
	// i : vecteur incident. dirig� vers le point d'intersection.
	// n : la normale � la surface
	// n1, n2 : densit� des milieu 1 et 2
	inline Math::Vector3f refract(const Math::Vector3f & i, const Math::Vector3f & n, double n1, double n2, bool & critical, double & R, double & T, const Math::Vector3f & r)
	{

		if (n1 == n2) return i;
		double cos_theta_i =  -i * n;
		double eta = n1 / n2;
		double cos_theta_i_2 = pow(cos_theta_i, 2);
		double eta2 = pow(eta, 2);
		double sin_theta_t_2 = eta2 * (1 - cos_theta_i_2);
		critical = 1 - sin_theta_t_2 < 0;
		if (critical) return r;
		double k = std::max(0.0, 1 - sin_theta_t_2);

		
		double cos_theta_t = sqrt(k);	
		computeRTSchlick(n1, n2, cos_theta_i, cos_theta_t, R, T);
		//computeRTFresnel(n1, n2, cos_theta_i, cos_theta_t, R, T);
		//computeRTBinary(critical, R, T);

		Math::Vector3f refracted = i * eta + n * (eta*cos_theta_i - cos_theta_t);
		/*if (blur > ::std::numeric_limits<double>::epsilon())
		{
			Math::Vector3f refracted_noise = Math::RandomDirection(refracted, 5000 / blur).generate();
			return (refracted_noise * n < 0) ? refracted_noise : refracted;
		}*/
		return refracted;
	}

	
}
#endif
	