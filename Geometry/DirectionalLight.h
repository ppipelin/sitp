#ifndef _Geometry_DirectionalLight_H
#define _Geometry_DirectionalLight_H

#include <Geometry/Material.h>
#include <Math/RandomDirection.h>

namespace Geometry
{
	class DirectionalLight
	{
	protected:
		/// \brief	The light direction.
		Math::Vector3f m_direction;

		/// \brief	The light color.
		RGBColor m_color;

		/// \brief The strengh determines the hardness of the shadow
		double m_softness;

	public:
		
		DirectionalLight(Math::Vector3f const & direction = Math::makeVector(0.0, 0.0, -1.0), RGBColor const & color = RGBColor(), double softness = 1.f)
			: m_direction(direction.normalized()), m_color(color), m_softness(softness)
		{
		}

		Math::Vector3f generate() const
		{
			double eps = std::numeric_limits<double>::epsilon();
			return  m_softness < eps ?
				m_direction+Math::makeVector(eps,eps,eps) : 
				Math::RandomDirection(m_direction, 5000/m_softness).generate();
		}

		const RGBColor & color() const
		{
			return m_color;
		}
		const Math::Vector3f & direction() const
		{
			return m_direction;
		}
	};
}

#endif
