#ifndef _Geometry_LightSampler_H
#define _Geometry_LightSampler_H

#include <random>
#include <Geometry/Triangle.h>
#include <Geometry/Geometry.h>
#include <Geometry/PointLight.h>
#include <Geometry/Material.h>
#include <algorithm>

namespace Geometry
{
	/// <summary>
	/// A light sampler. To initialize this sampler, just add triangles or geometries. The sampler
	/// will keep triangles with a material that emits light.
	/// </summary>
	class LightSampler
	{
		
	protected:
		/// <summary> A random number generator. </summary>
		mutable std::mt19937_64 randomGenerator;
		::std::vector<double> surfaceSum;
		::std::vector<const Primitive *> allPrimitives;
		double currentSum ;

		
	public:
		/// <summary>
		/// Constructor
		/// </summary>
		LightSampler()
			: currentSum(0.0)
		{
		}


		/// <summary>
		/// Adds a triangle to the light sampler.
		/// </summary>
		/// <param name="triangle"></param>
		void add(const Primitive * prim)
		{
			if (!prim->material()->getEmissive().isBlack())
			{
				currentSum += prim->surface()/**it->material()->getEmissive().grey()*/;
				surfaceSum.push_back(currentSum);
				allPrimitives.push_back(prim);
			}
		}

		/// <summary>
		/// Adds a geometry to the light sampler.
		/// </summary>
		/// <param name="geometry"></param>
		void add(const Geometry & geometry)
		{
			auto & primitives = geometry.getPrimitives();
			add(primitives.begin(), primitives.end());
		}

		/// <summary>
		/// Adds a range of triangles or geometries to the light sampler.
		/// </summary>
		template <class iterator>
		void add(iterator begin, iterator end)
		{
			for (iterator it = begin; it != end; ++it)
			{
				add(*it);
			}
		}


		/// <summary>
		/// Genates a point light by sampling the kept triangles.
		/// </summary>
		/// <returns></returns>
		//PointLight generate(const Math::Vector3f & normal, const Primitive * prim , double u, double v, const Math::Vector3f & viewer = Math::makeVector(0, 0, 0)) const
		//{
		//	Math::Vector3f point;
		//	RGBColor color;
		//	if (!(prim->material()->getEmissive().isBlack())) { // Si le triangle touch� est emissif, on veut donner un pointlight juste a coter
		//		if (prim->material()->hasTexture()) {
		//			Math::Vector3f barycentric = prim->UVFromPoint(u, v, viewer);
		//			point = prim->pointFromUV(barycentric);
		//			color = prim->sampleTexture(barycentric);
		//		}
		//		else {
		//			point = viewer;
		//			color = RGBColor(1.0f, 1.0f, 1.0f);
		//		}
		//		Math::Vector3f start = point + normal * 0.00001;
		//		return PointLight(start, prim->material()->getEmissive()*color);
		//	}

		//	double random = double(randomGenerator()) / double(randomGenerator.max());
		//	random = random * currentSum;
		//	auto found = std::lower_bound(surfaceSum.begin(), surfaceSum.end(), random);
		//	size_t index = found - surfaceSum.begin();
		//	const Primitive * P = allPrimitives[index];

		//	if (P->material()->hasTexture()) {
		//		Math::Vector3f barycentric = P->randomUV(viewer);
		//		point = P->pointFromUV(barycentric);
		//		color = P->sampleTexture(barycentric);
		//	}
		//	else {
		//		point = P->randomPoint(viewer);
		//		color = RGBColor(1.0f, 1.0f, 1.0f);
		//	}
		//	return PointLight(point, P->material()->getEmissive()*color);
		//}


		/// <summary>
		/// Generates a point light by sampling the kept triangles.
		/// </summary>
		/// <returns></returns>
		PointLight generate(const Material * & emissiveMaterial, const Math::Vector3f & viewer, const Math::Vector3f & normal, const Primitive * prim, double u, double v, Math::Vector3f & light_normal, double ubase = 0.0,  double vbase = 0.0, double step=1.0) const
		{
			Math::Vector3f point;
			RGBColor color;
			if (!(prim->material()->getEmissive().isBlack())) { // Si le triangle touch� est emissif, on veut donner un pointlight juste a cote
				emissiveMaterial = prim->material();
				if (prim->material()->hasTexture()) {
					Math::Vector3f barycentric = prim->UVFromPoint(u, v, viewer);
					point = prim->pointFromUV(barycentric);
					color = prim->sampleTexture(barycentric);
				}
				else {
					point = viewer;
					color = RGBColor(1.0f, 1.0f, 1.0f);
				}
				
				light_normal = prim->sampleNormal(point, 0, 0);
				Math::Vector3f start = point + normal * 0.0001;
				RGBColor Il = prim->material()->getEmissive()*color;
				return PointLight(start, Il);
			}

			double random = double(randomGenerator()) / double(randomGenerator.max());
			random = random * currentSum;
			auto found = std::lower_bound(surfaceSum.begin(), surfaceSum.end(), random);
			size_t index = found - surfaceSum.begin();
			const Primitive*  P = allPrimitives[index];
			emissiveMaterial = P->material();
			if (P->material()->hasTexture()) {
				Math::Vector3f barycentric =  P->randomUV(ubase*step, vbase*step, step, viewer);
				point = P->pointFromUV(barycentric);
				color = P->sampleTexture(barycentric);
			}
			else {
				point =  P->randomPoint(ubase*step, vbase*step, step, viewer);
				color = RGBColor(1.0f, 1.0f, 1.0f);
			}
			light_normal = P->sampleNormal(point, 0, 0);
			return PointLight(point, P->material()->getEmissive()*color);
		}


		/// <summary>
		/// Generates nb point lights by sampling the kept triangles.
		/// </summary>
		template <class iterator>
		iterator generate(iterator output, size_t nb)
		{
			for (size_t cpt = 0; cpt < nb; ++cpt)
			{
				(*output) = generate();
				++output;
			}
			return output;
		}

		/// <summary>
		/// Generates end-begin point lights by sampling the kept triangles.
		/// </summary>
		template <class iterator>
		iterator generate(iterator begin, iterator end, const Math::Vector3f & viewer = Math::makeVector(0,0,0))
		{
			for (; begin != end; ++begin)
			{
				(*begin) = generate(viewer);
				++begin;
			}
			return begin;
		}

		/// <summary>
		/// Returns true if the light sampler can sample lights.
		/// </summary>
		/// <returns></returns>
		bool hasLights() const
		{
			return allPrimitives.size() > 0;
		}
	};
}

#endif