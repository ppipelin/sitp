#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#ifndef M_TAU
#define M_TAU 6.28318530717958647692
#endif
#pragma once
#include "Primitive.h"
namespace Geometry
{
	class Annulus final : public Primitive
	{
	private:
		void computeUVAxis() {
			Math::Vector3f first;
			if (abs(m_vertexNormals[0][0]) < 0.9)
				first = Math::makeVector(1, 0, 0);
			else 
				first = Math::makeVector(0, 1, 0);
			vAxis = m_vertexNormals[0] ^ first;
			uAxis = vAxis ^ m_vertexNormals[0];
			uAxis = -uAxis.normalized();
			vAxis = -vAxis.normalized();
		}

	protected:
		Math::Vector3f m_center;
		double m_radius;
		double m_radius2;
		double m_hole_radius;
		double m_hole_radius2;
		double m_surface;
		Math::Vector3f uAxis, vAxis;
	public:
		Annulus()
		{
			m_vertex = {&m_center};
		}
		Annulus(Math::Vector3f * center, const Math::Vector3f & normal, double radius = 1, double hole_radius = 0.5, Material * mat = nullptr)
			:Primitive(mat),
			m_center(*center),
			m_radius(radius),
			m_hole_radius(hole_radius)
		{
			m_vertexNormals[0] = normal;
			m_vertex = { center };
			m_radius2 = pow(m_radius, 2);
			m_hole_radius2 = pow(m_hole_radius, 2);
			m_surface = M_PI * m_radius2;
			computeUVAxis();
		}
		~Annulus()
		{
		}
		 
		virtual Primitive* create(const std::vector<Math::Vector3f*> & v,
			const std::vector<Math::Vector2f*> & t,
			Material * material,
			const Math::Vector3f* normals
		) const
		{
			return new Annulus(v[0],normals[0], m_radius, m_hole_radius, material);
		}

		virtual Primitive* clone() const 
		{
			return new Annulus(*this);
		}

		virtual void update() final
		{
			m_center = *m_vertex[0];
			m_radius2 = pow(m_radius, 2);
			m_hole_radius2 = pow(m_hole_radius, 2);
			m_surface = M_PI * (m_radius2 - m_hole_radius2);
			uAxis = uAxis.normalized();
			vAxis = vAxis.normalized();
		}

		virtual void scale(double s) final
		{
			m_radius *= s;
			m_radius2 = pow(m_radius, 2);
			m_hole_radius *= s;
			m_hole_radius2 = pow(m_hole_radius, 2);
		}

		Math::Vector3f const & vertex(int i) const final
		{
			return m_center;
		}

		Math::Vector3f center() const final
		{
			return m_center;
		}

		RGBColor sampleTexture(double u, double v, const Math::Vector3f & intersection) const final
		{
			if (m_material->hasTexture())
			{
				u = (uAxis * (intersection-m_center))/m_radius;
				v = (vAxis * (intersection-m_center))/m_radius;
				return m_material->getTexture().pixel(Math::makeVector((u + 1) / 2., (v + 1) / 2.));
			}
			return RGBColor(1.0f, 1.0f, 1.0f);
		}

		Math::Vector3f samplePoint(double u, double v) const  final
		{
			return uAxis * u + vAxis * v ;
		}

		Math::Vector3f sampleNormal(const Math::Vector3f & intersection, double u, double v) const final
		{	
			return m_vertexNormals[0];
		}
		Math::Vector3f sampleNormalMap(const Math::Vector3f & intersection, double u, double v, const Math::Vector3f & normal) const
		{
			Math::Vector3f new_normal = normal;
			if (m_material->hasNormalMap()) {
				u = (uAxis * (intersection - m_center)) / m_radius;
				v = (vAxis * (intersection - m_center)) / m_radius;
				RGBColor col = m_material->getNormalMap().pixel(Math::makeVector((u + 1) / 2., (v + 1) / 2.));
				Math::Vector3f shifted;
				Math::Vector3f up = Math::makeVector(0, 0, 1);
				shifted[0] = col[0];
				shifted[1] = col[1];
				shifted[2] = col[2];
				shifted = shifted * 2 - 1;
				shifted = shifted.normalized();
				double cos = up * normal;
				double angle = acos(cos);
				Math::Vector3f axis = up ^ normal;
				Math::Quaternion<double> quat = Math::Quaternion<double>(axis, angle);
				new_normal = quat.rotate(shifted);
			}
			return new_normal;
		}

		bool intersection(Ray const & r, double & t, double & u, double & v) const final
		{
			const Math::Vector3f d = r.direction();
			const Math::Vector3f s = r.source();
			const Math::Vector3f StoC = m_center - s;

			t = (StoC*m_vertexNormals[0]) / (d*m_vertexNormals[0]);

			if (t <= 0) return false;

			const Math::Vector3f intersection = s + d * t;
			double D2 = (intersection - m_center).norm2();
			return D2 <= m_radius2 && D2 >= m_hole_radius2;
		}

		double surface() const final
		{
			return m_surface;
		}

		virtual Math::Vector3f randomUV(const Math::Vector3f & viewer) const final
		{
			return randomUV(0, 0, 1, viewer);
		}

		virtual Math::Vector3f randomUV(double ubase, double vbase, double step, const Math::Vector3f & viewer) const final
		{
			//double ksi1 = Math::Sobol::sample(m_index, 0, 0);
			//double ksi2 = Math::Sobol::sample(m_index, 1, 0);
			double ksi1 = double(rand()) / RAND_MAX;
			double ksi2 = double(rand()) / RAND_MAX;
			m_index++;
			const double r1 = ubase + (step*ksi1);
			const double r2 = vbase + (step*ksi2);
			const double sqrtr = sqrt(r1*(m_radius2 - m_hole_radius2) + m_hole_radius2) / m_radius;
			const double theta = r2 * M_TAU;
			double u = sqrtr * cos(theta);
			double v = sqrtr * sin(theta);
			return Math::makeVector(u, v, 0.0);
		}

		// Annulus : � partir de l'intersection
		// rend (u,v) appartenant � [-1;1]�
		Math::Vector3f UVFromPoint(double u, double v, const Math::Vector3f & intersection) const
		{
			Math::Vector3f point = intersection - m_center;
			double u_ = point/m_radius * uAxis;
			double v_ = point/m_radius * vAxis;
			return Math::makeVector(u_, v_, 0.0);
		}

		Math::Vector3f pointFromUV(const Math::Vector3f & uv) const final
		{
			return (uAxis * uv[0] + vAxis * uv[1]) * m_radius + m_center;
		}

		RGBColor sampleTexture(const Math::Vector3f & uv) const final
		{
			if (m_material->hasTexture())
				return m_material->getTexture().pixel(Math::makeVector((uv[0]+ 1) / 2., (uv[1]+ 1) / 2.));

			return RGBColor(1.0f, 1.0f, 1.0f);
		}

		Math::Vector3f randomPoint(const Math::Vector3f & viewer) const final
		{
			const Math::Vector3f uv = randomUV(viewer);
			return uAxis * (uv[0]) + vAxis * (uv[1]) + m_center;
		}

		Math::Vector3f randomPoint(double ubase, double vbase, double step, const Math::Vector3f & viewer) const final
		{
			const Math::Vector3f uv = randomUV(ubase, vbase, step, viewer);
			return uAxis * (uv[0]) + vAxis * (uv[1]) + m_center;
		}

		void boundingPoints(Math::Vector3f & min, Math::Vector3f & max) const 
		{
			Math::Vector3f e;
			e[0] = m_radius * sqrt(1.0 - m_vertexNormals[0][0] * m_vertexNormals[0][0]);
			e[1] = m_radius * sqrt(1.0 - m_vertexNormals[0][1] * m_vertexNormals[0][1]);
			e[2] = m_radius * sqrt(1.0 - m_vertexNormals[0][2] * m_vertexNormals[0][2]);
			min = m_center - e;
			max = m_center + e;

		}

		bool isTriangle() const
		{
			return false;
		}
	};
}

