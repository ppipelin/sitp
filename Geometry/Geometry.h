#ifndef _Geometry_Geometry_H
#define _Geometry_Geometry_H

#include <Geometry/CastedRay.h>
#include <Geometry/Triangle.h>
#include <Geometry/Sphere.h>
#include <Geometry/Annulus.h>
#include <Geometry/Cylinder.h>

#include <Geometry/ComputeVertexNormals.h>
#include <Geometry/Material.h>
#include <Math/Quaternion.h>
#include <Math/Vectorf.h>
#include <vector>
#include <deque>
#include <map>
#include <System/aligned_allocator.h>
#include <Math/Constant.h>

namespace Geometry
{
	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// \class	Geometry
	///
	/// \brief	A 3D geometry.
	///
	/// \author	F. Lamarche, Université de Rennes 1
	/// \date	04/12/2013
	////////////////////////////////////////////////////////////////////////////////////////////////////
	class Geometry
	{
	protected:
	    /// \brief	The vertices.
	    std::deque<Math::Vector3f> m_vertices ;
		/// \brief The texture coordinates
		std::deque<Math::Vector2f> m_textureCoordinates;
		/// \brief	The triangles.
		std::deque<Primitive *> m_primitives ;

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	void Geometry::updateTriangles()
		///
		/// \brief	Updates all the triangles of the geometry (normals, u and v vectors). This method should
		/// 		be called if some transformations arer applied on the vertices of the geometry.
		///
		/// \author	F. Lamarche, Université de Rennes 1
		/// \date	04/12/2013
		////////////////////////////////////////////////////////////////////////////////////////////////////
		void updateTriangles()
		{
			for(int cpt=0 ; cpt<(int)m_primitives.size() ; cpt++)
			{
				m_primitives[cpt]->update() ;
			}
		}

	public:
		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	const std::deque<Math::Vector3> & Geometry::getVertices() const
		///
		/// \brief	Gets the vertices.
		///
		/// \author	F. Lamarche, Université de Rennes 1
		/// \date	04/12/2013
		///
		/// \return	The vertices.
		////////////////////////////////////////////////////////////////////////////////////////////////////
		const std::deque<Math::Vector3f> & getVertices() const
		{ return m_vertices ; }

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	const std::vector<Triangle> & Geometry::getTriangles() const
		///
		/// \brief	Gets the triangles.
		///
		/// \author	F. Lamarche, Université de Rennes 1
		/// \date	04/12/2013
		///
		/// \return	The triangles.
		////////////////////////////////////////////////////////////////////////////////////////////////////
		const std::deque<Primitive *> & getPrimitives() const
		{ return m_primitives ; }

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	Geometry::Geometry()
		///
		/// \brief	Default constructor.
		///
		/// \author	F. Lamarche, Université de Rennes 1
		/// \date	04/12/2013
		////////////////////////////////////////////////////////////////////////////////////////////////////
		Geometry()
		{}

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="geometry">The geometry.</param>
		Geometry(const Geometry & geometry)
		{
			merge(geometry);
		}

		~Geometry() {
			for (Primitive * p : m_primitives) delete p;
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	unsigned int Geometry::addVertex(const Math::Vector3 & vertex)
		///
		/// \brief	Adds a vertex.
		///
		/// \author	F. Lamarche, Université de Rennes 1
		/// \date	04/12/2013
		///
		/// \param	vertex	The vertex.
		///
		/// \return	The index of the added vertex.
		////////////////////////////////////////////////////////////////////////////////////////////////////
		unsigned int addVertex(const Math::Vector3f & vertex)
		{ 
			m_vertices.push_back(vertex) ; 
			return m_vertices.size()-1 ;
		}

		/// <summary>
		/// Adds the texture coordinates.
		/// </summary>
		/// <param name="coord">The coordinates.</param>
		/// <returns></returns>
		unsigned int addTextureCoordinates(const Math::Vector2f & coord)
		{
			m_textureCoordinates.push_back(coord);
			return m_textureCoordinates.size() - 1;
		}

		/// <summary>
		/// Adds a triangle.
		/// </summary>
		/// <param name="i1">Zero-based index of the first vertex.</param>
		/// <param name="i2">Zero-based index of the second vertex.</param>
		/// <param name="i3">Zero-based index of the third vertex.</param>
		/// <param name="material">The material.</param>
		/// <param name="normals">The normals.</param>
		void addTriangle(int i1, int i2, int i3, Material * material, const Math::Vector3f * normals = nullptr)
		{
			if(m_textureCoordinates.empty())
			{
				m_primitives.push_back(new Triangle(&m_vertices[i1], &m_vertices[i2], &m_vertices[i3], material, normals));
			}
			else
			{
				m_primitives.push_back(new Triangle(&m_vertices[i1], &m_vertices[i2], &m_vertices[i3], &m_textureCoordinates[i1], &m_textureCoordinates[i2], &m_textureCoordinates[i3], material, normals));
			}
		}

		/// <summary>
		/// Adds the triangle.
		/// </summary>
		/// <param name="i1">The index of the first vertex.</param>
		/// <param name="i2">The index of the second vertex.</param>
		/// <param name="i3">The index of the third vertex.</param>
		/// <param name="t1">The index of the first texture coordinates.</param>
		/// <param name="t2">The index of the second texture coordinates.</param>
		/// <param name="t3">The index of the third texture coordinates.</param>
		/// <param name="material">The material.</param>
		/// <param name="normals">The vertices normals if any, nullptr means that normals must be  set to the face normal.</param>
		void addTriangle(int i1, int i2, int i3, int t1, int t2, int t3, Material * material, const Math::Vector3f * normals = nullptr)
		{
			m_primitives.push_back(new Triangle(&m_vertices[i1], &m_vertices[i2], &m_vertices[i3], &m_textureCoordinates[t1], &m_textureCoordinates[t2], &m_textureCoordinates[t3], material, normals));
		}

		void addSphere(const Math::Vector3f & center, double radius, Material * material)
		{
			int i = addVertex(center);
			Sphere * sph = new Sphere(&m_vertices[i], radius, material);
			m_primitives.push_back(sph);
		}

		void addDisk(const Math::Vector3f & center, const Math::Vector3f normal, double radius, Material * material)
		{
			int i = addVertex(center);
			Annulus * d = new Annulus(&m_vertices[i], normal, radius, 0.0, material);
			m_primitives.push_back(d);
		}

		void addAnnulus(const Math::Vector3f & center, const Math::Vector3f normal, double radius, double hole_radius, Material * material)
		{
			int i = addVertex(center);
			Annulus * a = new Annulus(&m_vertices[i], normal, radius, hole_radius, material);
			m_primitives.push_back(a);
		}

		//void addCylinder(const Math::Vector3f & base, const Math::Vector3f orientation, double height, double radius, Material * material)
		//{
		//	int i = addVertex(base);
		//	Math::Vector3f top = base + orientation * height;
		//	int j = addVertex(top);
		//	Cylinder * cyl = new Cylinder(&m_vertices[i], &m_vertices[j], orientation, height, radius, material);
		//	m_primitives.push_back(cyl);
		//}
		void addCylinder(const Math::Vector3f & base, const Math::Vector3f & top, double radius, Material * material)
		{
			int i = addVertex(base);
			int j = addVertex(top);
			Cylinder * cyl = new Cylinder(&m_vertices[i], &m_vertices[j], radius, material);
			m_primitives.push_back(cyl);
		}
		


		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	void Geometry::addTriangle(const Triangle & triangle)
		///
		/// \brief	Adds a triangle.
		///
		/// \author	F. Lamarche, Université de Rennes 1
		/// \date	04/12/2013
		///
		/// \param	triangle	The triangle.
		////////////////////////////////////////////////////////////////////////////////////////////////////
		void addTriangle(const Triangle & triangle)
		{
			int i1 = addVertex(triangle.vertex(0)) ;
			int i2 = addVertex(triangle.vertex(1)) ;
			int i3 = addVertex(triangle.vertex(2)) ;
			addTriangle(i1,i2,i3,triangle.material(), triangle.getVertexNormals()) ;
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	void Geometry::addTriangle(const Math::Vector3 & p0, const Math::Vector3 & p1,
		/// 	const Math::Vector3 & p2, Material * material)
		///
		/// \brief	Adds a triangle.
		///
		/// \author	F. Lamarche, Université de Rennes 1
		/// \date	04/12/2013
		///
		/// \param	p0					The first vertex.
		/// \param	p1					The second vertex.
		/// \param	p2					The third vertex.
		/// \param [in,out]	material	If non-null, the material.
		////////////////////////////////////////////////////////////////////////////////////////////////////
		void addTriangle(const Math::Vector3f & p0, const Math::Vector3f & p1, const Math::Vector3f & p2, Material * material, const Math::Vector3f * normals = nullptr)
		{
			int i1 = addVertex(p0) ;
			int i2 = addVertex(p1) ;
			int i3 = addVertex(p2) ;
			addTriangle(i1,i2,i3,material, normals) ;
		}


		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	void Geometry::merge(const Geometry & geometry)
		///
		/// \brief	Merges the provided geometry with this one.
		///
		/// \author	F. Lamarche, Université de Rennes 1
		/// \date	04/12/2013
		///
		/// \param	geometry The geometry that should be merged
		////////////////////////////////////////////////////////////////////////////////////////////////////
		void merge(const Geometry & geometry)
		{
			::std::map<const Math::Vector3f*, int> vectorToIndex ;
			for(size_t cpt=0 ; cpt<geometry.getVertices().size() ; cpt++)
			{
				const Math::Vector3f * vertex = &geometry.getVertices()[cpt];
				auto it = vectorToIndex.find(vertex) ;
				if(it==vectorToIndex.end())
				{
					int index = addVertex(geometry.getVertices()[cpt]) ;
					vectorToIndex.insert(::std::make_pair(vertex, index)) ;
				}
			}
			::std::map<const Math::Vector2f*, int> textureToIndex;
			for (size_t cpt = 0; cpt < geometry.m_textureCoordinates.size(); ++cpt)
			{
				const Math::Vector2f * vertex = &geometry.m_textureCoordinates[cpt];
				auto it = textureToIndex.find(vertex);
				if (it == textureToIndex.end())
				{
					int index = addTextureCoordinates(geometry.m_textureCoordinates[cpt]);
					textureToIndex.insert(::std::make_pair(vertex, index));
				}
			}

			for(int cpt=0 ; cpt<(int)geometry.getPrimitives().size() ; cpt++)
			{
				Primitive * current = geometry.getPrimitives()[cpt] ;
				::std::vector<Math::Vector3f*> vertices;
				for (int i = 0; i < current->vertices().size(); ++i) {
					Math::Vector3f * vertex = current->vertices()[i];
					auto it = vectorToIndex.find(vertex);
					int index = it->second;
					Math::Vector3f * vertex_ptr = &m_vertices[index];
					vertices.push_back(vertex_ptr);
				}
				::std::vector<Math::Vector2f*> textureCoordinates;
				for (int i = 0; i < current->textureCoordinates().size(); ++i) {
					Math::Vector2f * tcoor = current->textureCoordinates()[i];
					auto it = textureToIndex.find(tcoor);
					if (it != textureToIndex.end()) {
						int index = it->second;
						Math::Vector2f * textureCoordinate_ptr = &m_textureCoordinates[index];
						textureCoordinates.push_back(textureCoordinate_ptr);
					}	
				}
				Primitive * prim = current->create(vertices, textureCoordinates, current->material(), current->getVertexNormals());
				m_primitives.push_back(prim);
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	bool Geometry::intersection(CastedRay & ray)
		///
		/// \brief	Computes the intersection between this geometry and the provided Ray. This algorithm
		///			is straightforward... 
		///
		/// \author	F. Lamarche, Université de Rennes 1
		/// \date	04/12/2013
		///
		/// \param [in,out]	ray	The ray.
		///
		/// \return	true if it succeeds, false if it fails.
		////////////////////////////////////////////////////////////////////////////////////////////////////
		bool intersection(CastedRay & ray)
		{
			for(auto it=m_primitives.begin(), end=m_primitives.end() ; it!=end ; ++it)
			{
				ray.intersect(*it) ;
			}
			return ray.validIntersectionFound() ;
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	void Geometry::translate(Math::Vector3 const & t)
		///
		/// \brief	Translate this geometry.
		///
		/// \author	F. Lamarche, Université de Rennes 1
		/// \date	04/12/2013
		///
		/// \param	t	The translation vector.
		////////////////////////////////////////////////////////////////////////////////////////////////////
		void translate(Math::Vector3f const & t)
		{
			for(auto it=m_vertices.begin(), end=m_vertices.end() ; it!=end ; ++it)
			{
				(*it) = (*it)+t ; 
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	void Geometry::scale(double v)
		///
		/// \brief	Applies a scale factor on this geometry.
		///
		/// \author	F. Lamarche, Université de Rennes 1
		/// \date	04/12/2013
		///
		/// \param	v	The scale factor.
		////////////////////////////////////////////////////////////////////////////////////////////////////
		void scale(double v)
		{
			for(auto it=m_vertices.begin(), end=m_vertices.end() ; it!=end ; ++it)
			{
				(*it) = (*it)*v ;
			}
			scalePrimitives(v);
			updateTriangles();
		}

		void scalePrimitives(double v) {
			for (Primitive * P : m_primitives)
				P->scale(v);
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	void Geometry::scaleX(double v)
		///
		/// \brief	Scales geometry on X axis.
		///
		/// \author	F. Lamarche, Université de Rennes 1
		/// \date	04/12/2013
		///
		/// \param	v	The scale factor on X axis.
		////////////////////////////////////////////////////////////////////////////////////////////////////
		void scaleX(double v)
		{
			for(auto it=m_vertices.begin(), end=m_vertices.end() ; it!=end ; ++it)
			{
				(*it)[0] = (*it)[0]*v ;
			}
			updateTriangles() ;
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	void Geometry::scaleY(double v)
		///
		/// \brief	Scales geometry on Y axis.
		/// 		
		/// \author	F. Lamarche, Université de Rennes 1
		/// \date	04/12/2013
		///
		/// \param	v	The scale factor.
		////////////////////////////////////////////////////////////////////////////////////////////////////
		void scaleY(double v)
		{
			for(auto it=m_vertices.begin(), end=m_vertices.end() ; it!=end ; ++it)
			{
				(*it)[1] = (*it)[1]*v ;
			}
			updateTriangles() ;
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	void Geometry::scaleZ(double v)
		///
		/// \brief	Scales the geometry on Z axis.
		///
		/// \author	F. Lamarche, Université de Rennes 1
		/// \date	04/12/2013
		///
		/// \param	v	The scale factor.
		////////////////////////////////////////////////////////////////////////////////////////////////////
		void scaleZ(double v)
		{
			for(auto it=m_vertices.begin(), end=m_vertices.end() ; it!=end ; ++it)
			{
				(*it)[2] = (*it)[2]*v ;
			}
			updateTriangles() ;
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	void Geometry::rotate(Math::Quaternion const & q)
		///
		/// \brief	Rotates this geometry.
		///
		/// \author	F. Lamarche, Université de Rennes 1
		/// \date	04/12/2013
		///
		/// \param	q	Quaternion describing the rotation.
		////////////////////////////////////////////////////////////////////////////////////////////////////
		void rotate(Math::Quaternion<double> const & q)
		{
			for(auto it=m_vertices.begin(), end=m_vertices.end() ; it!=end ; ++it)
			{
				(*it) = q.rotate(*it).v() ;
			}
			updateTriangles() ;
		}

		/// <summary>
		/// Computes the per vertex normals.
		/// </summary>
		/// <param name="angle">The angle limit for smoothing surface.</param>
		void computeVertexNormals(double angle)
		{
			double cosAngleLimit = cos(angle);
			::std::vector<Triangle *> triangles;
			for (auto it = m_primitives.begin(), end = m_primitives.end(); it != end; ++it)
			{
				Primitive * prim = *it;
				if (prim->isTriangle()) {
					Triangle * tri  = static_cast<Triangle *>(prim);
					triangles.push_back(tri);
				}
			}
			ComputeVertexNormals normalsComputation(triangles);
			normalsComputation.compute(cosAngleLimit);
		}
		void setMaterialForAllPrimitives(Material * mat)
		{
			for (Primitive * prim : getPrimitives()) {
				prim->setMaterial(mat);
			}
		}
		
	} ;

}


#endif
