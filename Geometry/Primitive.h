#ifndef _Geometry_Primitive_H
#define _Geometry_Primitive_H

#include <Math/Vectorf.h>
#include <Geometry/Ray.h>
#include <Geometry/Material.h>
#include <cassert>
#include <vector>


namespace Geometry {
	class Primitive
	{
	protected:
		/// \brief	The associated material.
		Material * m_material;
		/// \brief	Pointers to the three vertices
		std::vector<Math::Vector3f*> m_vertex; 
		/// \brief Pointers to the texture coordinates
		std::vector<Math::Vector2f*> m_textureCoordinates;
		/// \brief Pointers to the texture coordinates
		std::vector<Math::Vector2f*> m_normalMapCoordinates;

		Math::Vector3f m_vertexNormals[3];

		mutable int m_index;

		
	public:
		const RGBColor m_randColor;
		Primitive(Material * material = nullptr) :
			m_material(material),
			m_randColor(pow((double(rand()) / RAND_MAX),2), pow((double(rand()) / RAND_MAX), 2), pow((double(rand()) / RAND_MAX), 2)),
			m_index(rand())
		{}
		//Primitive(){}

		virtual ~Primitive() {}

		// tr�s sp�cial : constructeur et constructeur de copie virtuels
		// Permet de copier et de creer une primitive sans meme connaitre son type exact (triangle/sphere/cylindre/disque...)
		// exemple : j'ai un pointeur sur une primitive    \/\/\/\/\/\/\/\/\/ 
		///                                                Primitive * prim_ptr = new TessellatedCone(arguments); (TessellatedCone : pour l'exemple)
		// Primitive * prim_cpy_ptr = prim_ptr.clone();
		// Primitive * prim_same_type_ptr = prim_ptr.create(mat);
		// Primitive prim_same_type = *prim_same_type_ptr ;
		// Primitive prim_cpy = *prim_cpy_ptr ;
		// delete prim_cpy_ptr ;
		// delete prim_same_type_ptr ;
		// prim_same_type et prim_cpy sont des Cones
		virtual Primitive* create(const std::vector<Math::Vector3f*> & v, 
			const std::vector<Math::Vector2f*> & t,
			Material * material, 
			const Math::Vector3f* normals
		) const = 0;
		virtual Primitive* clone() const = 0;

		virtual void update() = 0;


		Material * material() const
		{
			return m_material;
		}

		virtual Math::Vector3f const & vertex(int i) const = 0;

		//virtual Math::Vector2f const & textureCoordinate(int i) const = 0;

		virtual Math::Vector3f center() const = 0;

		virtual RGBColor sampleTexture(double u, double v, const Math::Vector3f & intersection) const = 0;

		virtual Math::Vector3f samplePoint(double u, double v) const = 0;

		virtual Math::Vector3f sampleNormal(const Math::Vector3f & intersection, double u, double v) const = 0;

		virtual Math::Vector3f sampleNormalMap(const Math::Vector3f & intersection, double u, double v, const Math::Vector3f &) const = 0;

		/// <summary>
		/// Returns the direction of a reflected ray, from a surface normal and the direction of the incident ray.
		/// </summary>
		/// <param name="n">The n.</param>
		/// <param name="dir">The dir.</param>
		/// <returns></returns>
		static Math::Vector3f reflectionDirection(Math::Vector3f const & n, Math::Vector3f const & dir)
		{
			Math::Vector3f reflected(dir - n * (2.0f*(dir*n)));
			return reflected;
		}

		virtual bool intersection(Ray const & r, double & t, double & u, double & v)const  = 0;// , Math::Vector3f & xyz, bool & uv) const = 0;
		//bool intersection(Ray const & r, double & t, double & u, double & v)const { return true; }// , Math::Vector3f & xyz, bool & uv) const = 0;

		virtual double surface() const = 0;

		virtual Math::Vector3f randomUV(const Math::Vector3f & viewer) const = 0;
		
		virtual Math::Vector3f randomUV(double ubase, double vbase, double step, const Math::Vector3f & viewer) const = 0;

		virtual Math::Vector3f pointFromUV(const Math::Vector3f & barycentric) const = 0;

		virtual Math::Vector3f UVFromPoint(double u, double v, const Math::Vector3f & intersection) const = 0;
		
		virtual RGBColor sampleTexture(const Math::Vector3f & barycentic) const = 0;
		
		virtual Math::Vector3f randomPoint(const Math::Vector3f & viewer) const = 0;

		virtual Math::Vector3f randomPoint(double ubase, double vbase, double step, const Math::Vector3f & viewer) const = 0;

		virtual void boundingPoints(Math::Vector3f & min, Math::Vector3f & max) const = 0;

		const std::vector<Math::Vector3f*> & vertices() const {
			return m_vertex;
		}

		const std::vector<Math::Vector2f*> & textureCoordinates() const {
			return m_textureCoordinates;
		}

		const Math::Vector3f * getVertexNormals() const
		{
			return m_vertexNormals;
		}

		virtual void scale(double v) = 0;

		// Pour ne pas etre oblige d'utiliser la RTTI pour l'interpolation des normales
		virtual bool isTriangle() const = 0;

		void setMaterial(Material * mat) {
			m_material = mat;
		}
	};
}


#endif

