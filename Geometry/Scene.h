#ifndef _Geometry_Scene_H
#define _Geometry_Scene_H

#include <locale>
#include <windows.h>
#include <Windows.h>
#include <Geometry/Geometry.h>
#include <Geometry/PointLight.h>
#include <Geometry/DirectionalLight.h>
#include <Visualizer/Visualizer.h>
#include <Geometry/Camera.h>
#include <Geometry/BoundingBox.h>
#include <Math/RandomDirection.h>
#include <windows.h>
#include <System/aligned_allocator.h>
#include <Math/Constant.h>
#include <queue>
#include <functional>
#include <random>
#include <math.h>
#include <Geometry/LightSampler.h>
#include <Tree.h>
#include <iomanip>
#include <HilbertTree.h>
#include <chrono>
#include <iostream>
#include <fstream>
#include <direct.h>
#include <Geometry/Refraction.h>

namespace Geometry
{
	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// \class	Scene
	///
	/// \brief	An instance of a geometric scene that can be rendered using ray casting. A set of methods
	/// 		allowing to add geometry, lights and a camera are provided. Scene rendering is achieved by
	/// 		calling the Scene::compute method.
	///
	/// \author	F. Lamarche, Universit� de Rennes 1
	/// \date	03/12/2013
	////////////////////////////////////////////////////////////////////////////////////////////////////
	class Scene
	{
	protected:
		/// \brief	The visualizer (rendering target).
		Visualizer::Visualizer * m_visu;
		/// \brief	The scene geometry (basic representation without any optimization).
		::std::deque<::std::pair<BoundingBox, Geometry> > m_geometries;
		// Geometry m_geometry ;
		/// \brief	The pointlights.
		std::vector<PointLight> m_pointLights;
		/// \brief	The directional lights.
		std::vector<DirectionalLight> m_directionalLights;
		/// \brief	The camera.
		
		/// \brief The scene bounding box
		BoundingBox m_sceneBoundingBox;
		/// \brief number of diffuse samples for global illumination
		size_t m_diffuseSamples;
		/// \brief Number of specular samples 
		size_t m_specularSamples;
		/// \brief Number of light samples 
		size_t m_lightSamples;
		/// brief Rendering pass number
		int m_pass;
		/// <summary>
		/// The light sampler associated with the scene
		/// </summary>
		LightSampler m_lightSampler;

		::std::list<LightSampler> m_samplerList;

		// Trees sans agglomerer
		::std::deque<Tree *> m_Trees;

		// Tree en agglomerant
		Tree m_Tree;
		int m_nbNodes;

		// Mode de contruction
		// 0 : pas de tree
		// 1 : tree non agglomere
		// 2 : tree agglomere
		int m_mode;

		bool m_renderEmissive = true;
		bool m_renderPointLights = false;
		bool m_renderDirectionalLights = true;
		
		bool m_isNoise;
		bool m_isStratification = true;
		int m_stratification = 1;
		int m_u = 0;
		int m_v = 0;
		int m_stratification2 = 1;
		double m_step = 1.;
		double m_noiseRatio = 0.90;
		::chrono::high_resolution_clock::time_point m_lastInput;
		double m_seuilInput = 0.05;
		bool keys[256];
		double m_move_speed;
		double m_rotate_speed;

		bool m_isPathTracing = false;

		Texture * m_backgroundTexture;

		std::vector<std::pair<double, int>> m_records;


	public:
		Camera m_camera;
		bool m_indirect = false;
		::std::string m_sceneName = "Unnamed";
		double m_absorption = 0;
		bool m_isNormalMapping = true;

		/*
		* Methode permettant de calculer le tree depuis l'exterieur de la classe
		* met a 1 m_node pour utiliser le tree dans SendRay
		* SANS AGGLOMERATION
		* param min_vertices : nombre maximum de primitives dans les feuilles du tree resultat 
		*/
		void computeTree_median_each(int min_vertices) {
			//On delete tous les trees s'ils ont d�j� �t� calcul�s
			for (Tree * T : m_Trees) delete T;
			m_mode = 1;
			//d'abord on fusionne toutes les primitibves
			::std::deque<const Triangle*> deque_t;
			for (const ::std::pair<BoundingBox, Geometry> & p : m_geometries) {
				Tree * T = new Tree();
				for (const Primitive * P : p.second.getPrimitives()) {
					T->addPrimitive(P);
				}
				m_Trees.push_back(T);
				T->build_median(min_vertices);
				::std::cout << "un arbre construit" << ::std::endl;
			}
		}

		/*
		* Methode permettant de calculer le tree depuis l'exterieur de la classe
		* met a vrai 1 pour utiliser le tree dans SendRay
		* SANS AGGLOMERATION
		* double c_t, c_i couts attendus
		* param min_vertices : nombre maximum de primitives dans les feuilles du tree resultat
		*/
		void computeTree_SAH_each(double c_t, double c_i,unsigned int min_vertices) {
			//On delete tous les trees s'ils ont d�j� �t� calcul�s
			for (Tree * T : m_Trees) delete T;
			m_mode = 1;
			//d'abord on fusionne toutes les primitibves
			int cpt = 0;
			::std::deque<const Triangle*> deque_t;
			for (const ::std::pair<BoundingBox, Geometry> & p : m_geometries) {
				Tree * T = new Tree();
				for (const Primitive * P : p.second.getPrimitives()) {
					T->addPrimitive(P);
				}
				m_Trees.push_back(T);
				::std::cout << "un arbre construit " << ++cpt << " sur " << m_geometries.size() << ", (" << p.second.getPrimitives().size() << ")";
				T->build_SAH(c_t, c_i, min_vertices);
				::std::cout << " -- fini. hauteur : " << T->height() << ", nb noeuds : " << T->leaves()<< ::std::endl;
			}
		}
		
		/*
		* Methode permettant de calculer le tree depuis l'exterieur de la classe
		* met a 1 m_node pour utiliser le tree dans SendRay
		* AVEC AGGLOMERATION
		* param min_vertices : nombre maximum de primitives dans les feuilles du tree resultat
		*/
		void computeTree_median(int min_vertices) {
			m_mode = 2;
			//d'abord on fusionne toutes les primitibves
			for (const ::std::pair<BoundingBox, Geometry> & p : m_geometries) {
				for (const Primitive * P : p.second.getPrimitives()) {
					m_Tree.addPrimitive(P);
				}			
			}
			m_Tree.build_median(min_vertices);
			m_nbNodes = m_Tree.size();
			::std::cout << " BVH median fini. hauteur : " << m_Tree.height() << ", nb noeuds : " << m_Tree.leaves() << " ,nb triangles " << m_Tree.nbTriangles() << ::std::endl;
			::std::cout << "un arbre construit" << ::std::endl;
		}

		/* 
		* Methode permettant de calculer le tree depuis l'exterieur de la classe
		* met a vrai 1 pour utiliser le tree dans SendRay
		* AVEC AGGLOMERATION
		* double c_t, c_i couts attendus
		* param min_vertices : nombre maximum de primitives dans les feuilles du tree resultat
		*/
		void computeTree_SAH(double c_t, double c_i, unsigned int min_vertices) {
			m_mode = 2;
			//d'abord on fusionne toutes les primitibves
			for (const ::std::pair<BoundingBox, Geometry> & p : m_geometries) {
				for (const Primitive * P : p.second.getPrimitives()) {
					m_Tree.addPrimitive(P);
				}
			}

			m_Tree.build_SAH(c_t, c_i, min_vertices);

			
			m_nbNodes = m_Tree.size();
			::std::cout << " BVH SAH fini. hauteur : " << m_Tree.height() << ", nb noeuds : " << m_Tree.leaves() <<" ,nb triangles "<<m_Tree.nbTriangles()<< ::std::endl;
			::std::cout << "un arbre construit" << ::std::endl;
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	Scene::Scene(Visualizer::Visualizer * visu)
		///
		/// \brief	Constructor.
		///
		/// \author	F. Lamarche, Universit� de Rennes 1
		/// \date	03/12/2013
		///
		/// \param [in,out]	visu	If non-null, the visu.
		////////////////////////////////////////////////////////////////////////////////////////////////////
		Scene(Visualizer::Visualizer * visu)
			: m_visu(visu), m_diffuseSamples(30), m_specularSamples(30), m_lightSamples(0), m_mode(0 ), m_backgroundTexture(nullptr)
		{}
		~Scene()
		{}

		/// <summary>
		/// Prints stats about the geometry associated with the scene
		/// </summary>
		void printStats()
		{
			size_t nbTriangles = 0;
			for (auto it = m_geometries.begin(), end = m_geometries.end(); it != end; ++it)
			{
				nbTriangles += it->second.getPrimitives().size();
			}
			::std::cout << "Scene: " << nbTriangles << " primitive" << ::std::endl;
		}

		/// <summary>
		/// Computes the scene bounding box.
		/// </summary>
		/// <returns></returns>
		const BoundingBox & getBoundingBox()
		{
			return m_sceneBoundingBox;
		}

		/// <summary>
		/// Sets the number of diffuse samples
		/// </summary>
		/// <param name="number"> The number of diffuse samples</param>
		void setDiffuseSamples(size_t number)
		{
			m_diffuseSamples = number;
		}

		/// <summary>
		/// Sets the number of specular samples
		/// </summary>
		/// <param name="number"></param>
		void setSpecularSamples(size_t number)
		{
			m_specularSamples = number;
		}

		/// <summary>
		/// Sets the number of light samples if the scene contains surface area lights
		/// </summary>
		/// <param name="number">The number of samples</param>
		void setLightSamples(size_t number)
		{
			m_lightSamples = number;
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	void Scene::add(const Geometry & geometry)
		///
		/// \brief	Adds a geometry to the scene.
		///
		/// \author	F. Lamarche, Universit� de Rennes 1
		/// \date	03/12/2013
		///
		/// \param	geometry The geometry to add.
		////////////////////////////////////////////////////////////////////////////////////////////////////
		void add(const Geometry & geometry)
		{
			if (geometry.getVertices().size() == 0) { return; }
			BoundingBox box(geometry);
			m_geometries.push_back(::std::make_pair(box, geometry));
			m_geometries.back().second.computeVertexNormals(Math::piDiv4 / 2);
			if (m_geometries.size() == 1)
			{
				m_sceneBoundingBox = box;
			}
			else
			{
				m_sceneBoundingBox.update(box);
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	void Scene::add(PointLight * light)
		///
		/// \brief	Adds a poitn light in the scene.
		///
		/// \author	F. Lamarche, Universit� de Rennes 1
		/// \date	04/12/2013
		///
		/// \param [in,out]	light	If non-null, the light to add.
		////////////////////////////////////////////////////////////////////////////////////////////////////
		void add(const PointLight & light)
		{
			m_pointLights.push_back(light);
		}
		void add(const DirectionalLight & light)
		{
			m_directionalLights.push_back(light);
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	void Scene::setCamera(Camera const & cam)
		///
		/// \brief	Sets the camera.
		///
		/// \author	F. Lamarche, Universit� de Rennes 1
		/// \date	04/12/2013
		///
		/// \param	cam	The camera.
		////////////////////////////////////////////////////////////////////////////////////////////////////
		void setCamera(Camera const & cam)
		{
			m_camera = cam;
		}


		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	RGBColor Scene::sendRay(Ray const & ray, double limit, int depth, int maxDepth)
		///
		/// \brief	Sends a ray in the scene and returns the computed color
		///
		/// \author	F. Lamarche, Universit� de Rennes 1
		/// \date	04/12/2013
		///
		/// \param	ray			The ray.
		/// \param	depth   	The current depth.
		/// \param	maxDepth	The maximum depth.
		///
		/// \return	The computed color.
		////////////////////////////////////////////////////////////////////////////////////////////////////
		RGBColor sendRay(Ray const & ray, double depth, int maxDepth, int diffuseSamples, int specularSamples, bool indirect = false, const Material * currentMat = nullptr)
		{
			//if (depth == 0) m_pr++;
			if (depth >= maxDepth) return  RGBColor();
			
			RGBColor directColor, indirectColor, result;
			CastedRay cRay = CastedRay(ray);

			Intersect(cRay, 0, ::std::numeric_limits<double>::max());

			if (!cRay.validIntersectionFound())
				return backgroundColor(cRay);

			// Vecteurs et points
			const RayPrimitiveIntersection & rpi = cRay.intersectionFound();
			const Math::Vector3f & vecteurIncident = cRay.direction();
			const Primitive * pri = rpi.primitive();
			const Material * material = pri->material();
			//return tri->m_randColor;
			const Math::Vector3f & p = rpi.intersection(); // point d'intersection
			const double distancePToV = rpi.tRayValue();//(p - ray.source()).norm();
			const double u_coor = rpi.uTriangleValue();
			const double v_coor = rpi.vTriangleValue();
			Math::Vector3f & normal = pri->sampleNormal(p, u_coor, v_coor); // Par rapport au point
			if (normal*ray.direction() > 0.0) normal = -normal;
			if (m_isNormalMapping) {
				Math::Vector3f new_normal = pri->sampleNormalMap(p, u_coor, v_coor, normal);
				if (new_normal*ray.direction() < 0.0) normal = new_normal;
			}
			
			Math::Vector3f u = Primitive::reflectionDirection(normal, ray.direction());
			

			// Propri�t�s material
			 // Couleur de la texture du triangle a l'endroit touche
			directColor = directColor + material->getAmbient();

			if (m_renderEmissive && m_lightSampler.hasLights())
			{	
				const Material * emissiveMaterial = nullptr;
				Math::Vector3f light_normal;
				const PointLight &	Pl = m_isStratification && !m_indirect ?
					m_lightSampler.generate(emissiveMaterial, p, normal, pri, u_coor, v_coor, light_normal , m_u, m_v, m_step) :
						m_lightSampler.generate(emissiveMaterial, p, normal, pri, u_coor, v_coor, light_normal);
				directColor = directColor + computePhong(Pl, p,  normal, u,  material, emissiveMaterial, light_normal, true);
			}
			if (m_renderPointLights && !m_pointLights.empty()){
				for (const PointLight & Pl : m_pointLights) { // Cherche dans toutes les lights
					directColor = directColor + computePhong(Pl, p, normal, u,  material, nullptr, vecteurIncident);
				}
			}
			if (m_renderDirectionalLights && !m_directionalLights.empty()){
				for (const DirectionalLight & Dl : m_directionalLights) { // Cherche dans toutes les lights
					directColor = directColor + computePhong(Dl, p,  normal, u,  material);
				}
			}
			RGBColor text = pri->sampleTexture(u_coor, v_coor, p);
			directColor = directColor * text;
			
			if (!m_indirect && !material->getSpecular().isBlack()) {
				// Trace reflected ray
				directColor = directColor + material->getSpecular() * sendRay(Ray(p + u * 0.00001, u), depth + 1, maxDepth, diffuseSamples, specularSamples); // L�ger d�calage pour ne pas intersecter le triangle de d�part
			}
			
			if (m_indirect) {
				double a = 1 - m_absorption;
				if (double(rand()) / RAND_MAX < a) {
					bool isRefracted, isReflected;
					// On echantillonne la pdf du materiau
					Math::Vector3f scattered = (m_isStratification && !indirect) ?
					material->scatter(vecteurIncident, u, normal, currentMat, isRefracted, isReflected, m_u, m_v, m_step) :
					material->scatter(vecteurIncident, u, normal, currentMat, isRefracted, isReflected);

					// On �value la brdf pour les directions d'entr�e et de sortie
					RGBColor brdf = material->brdf(vecteurIncident, u, normal, scattered, isRefracted, isReflected);
					Ray scatteredRay = Ray(p + scattered * 0.00001, scattered);
					const Material * new_material = isRefracted ? material : currentMat;
					double inc = isRefracted ? 0.25 : 1;
					RGBColor incomingLuminance = sendRay(scatteredRay, depth + inc, maxDepth, diffuseSamples, specularSamples, true, new_material)/a; // biais, mais c'est plus styl�
					indirectColor = text * brdf * incomingLuminance;
					
					

					if (indirectColor.grey() > 0) {
						//return indirectColor;
						if (isRefracted) {
							return indirectColor;
						}					
						return directColor + indirectColor;
					}

					//if (!indirect)
					//	return RGBColor();
					
				}
			}
			return directColor;
		}

		RGBColor computePhong(const PointLight & L, const Math::Vector3f & p, const Math::Vector3f & normal, const Math::Vector3f & u, const Material * mat, const Material * emissiveMaterial, const Math::Vector3f & light_normal, bool emissive_surface = false) {
			RGBColor phong;

			double distanceToLight = (p - L.position()).norm(); // Distance p to light
			RGBColor Il = L.color() / (distanceToLight + 1.0);
			Math::Vector3f dirToLight = (L.position() - p).normalized();
			Math::Vector3f ln = light_normal;
			CastedRay cRayToLight = CastedRay(p, dirToLight);
			bool found = IntersectObstructed(cRayToLight, 0.0001, distanceToLight-0.0001, emissiveMaterial);
			if (!found)
			{
				 //Si on a touch� une autre primitive au meme materiau emissif, on doit resituer la pointlight (en faisant attention � la texture)
				// //-> On utilise le code de generate pour quand on touche directement une surface emissive (un peu tricky mais ca marche)
				//if (cRayToLight.validIntersectionFound()) {
				//	//return RGBColor();
				//	RayPrimitiveIntersection rpi = cRayToLight.intersectionFound();
				//	const Material * m = nullptr;
				//	PointLight new_L = m_lightSampler.generate(m, rpi.intersection(), Math::makeVector(0, 0, 0), rpi.primitive(), rpi.uTriangleValue(), rpi.vTriangleValue(), ln);
				//	distanceToLight = (p - new_L.position()).norm();
				//	dirToLight = (new_L.position() - p).normalized();
				//	Il = new_L.color() / (distanceToLight + 1.0);
				//}

				if (emissive_surface) {
					Il = Il * abs(ln*dirToLight); // Plus la surface emissive est inclin�e face � l'observateur, plus l'eclairement est fort
				}
//#pragma omp critical
//				{
//					std::cout << p << std::endl << L.position() << std::endl;
//					std::cout << dirToLight << std::endl << normal << std::endl;
//				}


				const double orientationDemi = dirToLight * normal;

				//const double orientationDemi = emissive_surface ? 1 : dirToLight * normal;
				// V�rifier que la light soit dans le bon demi espace
				if (orientationDemi > 0) {
					const RGBColor diffuse = mat->getDiffuse() * orientationDemi;
					RGBColor specular;
					if (!mat->getSpecular().isBlack()) {
						const double orientationQuart = u * dirToLight;
						if (orientationQuart > 0) {
							specular = mat->getSpecular() * pow((orientationQuart), mat->getShininess());  // Obtention des taches sp�culaires
						}
					}
					phong = (diffuse + specular) * Il;
				}
			}
			return phong;
		}
		RGBColor computePhong(const DirectionalLight & L, const Math::Vector3f & p, const Math::Vector3f & normal, const Math::Vector3f & u, const Material * mat) {
		//RGBColor computePhong(const DirectionalLight & L, const Math::Vector3f & p, const RGBColor Text, const Math::Vector3f & normal, const Math::Vector3f & u, const RGBColor & Kd, const RGBColor & Ks, const bool & isSpecular, const double n, int bounces) {
			RGBColor phong;
			const Math::Vector3f & d = L.generate();
			CastedRay cRayToLight = CastedRay(p, -d); // On part de la primitive
			bool found = IntersectObstructed(cRayToLight, 0.00001, std::numeric_limits<double>::max());
			if (!found){ // Du coup, on fait des trucs seulement si on touche rien...
				const double orientationDemi = -normal * d; // Bon demi espace ?
				if (orientationDemi > 0) {
					const RGBColor diffuse = mat->getDiffuse() * orientationDemi;
					RGBColor specular;
					if (!mat->getSpecular().isBlack()) {
						const double orientationQuart = -(u * d); // Bon quart d'espace ?
						if (orientationQuart > 0) 
							specular = mat->getSpecular() * pow((orientationQuart), mat->getShininess());  // Obtention des taches sp�culaires
					}
					phong = (diffuse + specular) * L.color();
				}
			}
			return phong;
		}


		void IntersectWithGeometries(CastedRay & cr) {
			
			for (const std::pair<BoundingBox, Geometry> & paire : m_geometries) {
				// Tester Bounding box
				double entryT, exitT;
				if (paire.first.intersect(cr, 0, ::std::numeric_limits<double>::max(), entryT, exitT))
				{
					for (const Primitive * P : paire.second.getPrimitives())
						cr.intersect(P);
				}
			}
		}
		
		void IntersectWithAllGeometries(CastedRay & cr) {
			for (const std::pair<BoundingBox, Geometry> & paire : m_geometries) {
				for (const Primitive * P : paire.second.getPrimitives()) {
					cr.intersect(P);
				}	
			}
		}
		void IntersectWithTrees(CastedRay & cr, double min, double max) {
			for (const Tree * tree : m_Trees) {
				double t_i;
				tree->intersect(cr, 0, ::std::numeric_limits<double>::max(), t_i);
			}
		}
		void IntersectWithTree(CastedRay & cr, double min, double max) {
			double t_i;
			m_Tree.intersect(cr, 0, ::std::numeric_limits<double>::max(), t_i);		
		}

		void Intersect(CastedRay & cr, double min, double max) {
			switch (m_mode) {
				case 0: IntersectWithAllGeometries(cr);
					break;
				case 1:IntersectWithTrees(cr,  min,  max);
					break;
				case 2: IntersectWithTree(cr, min,  max);
					break;
				default: ::std::cout << "mode mal initialise" << ::std::endl;
			}
		}

		bool IntersectObstructed(CastedRay & cr, double min, double max, const Material * mat = nullptr) {
			switch (m_mode) {
				case 0:
					return IntersectObstructedWithGeometries(cr, min, max, mat);
					break;
				case 1:
					return IntersectObstructedWithTrees(cr, min, max, mat);
					break;
				case 2:
					return IntersectObstructedWithTree(cr, min, max, mat);
					break;
				default:
					::std::cout << "mode mal initialise" << ::std::endl;
					return false;
			}
		}

		bool IntersectObstructedWithTree(CastedRay & cr, double min, double max, const Material * mat) {
			double t_i;
			return m_Tree.intersectObstructed(cr, min, max, t_i, min, max, mat);
		}
		bool IntersectObstructedWithTrees(CastedRay & cr, double min, double max, const Material * mat) {
			for (const Tree * tree : m_Trees) {
				double t_i;
				if(tree->intersectObstructed(cr, min, max, t_i, min, max, mat)) return true;
			}
			return false;
		}
		bool IntersectObstructedWithGeometries(CastedRay & cr,double min, double max, const Material * mat) {
			for (const std::pair<BoundingBox, Geometry> & paire : m_geometries) {
				// Tester Bounding box
				double entryT, exitT;
				if (paire.first.intersect(cr, min, max, entryT, exitT)){
					for (const Primitive * P : paire.second.getPrimitives()) {
						if (cr.intersect(P)){
							double dist = cr.intersectionFound().tRayValue();
							if(cr.intersectionFound().primitive()->material() != mat && dist > min && dist < max) return true;
						}						
					}
				}
			}
			return false;
		}

	

		RGBColor sendRayHeatMap(Ray const & ray, int depth, int maxDepth, int diffuseSamples, int specularSamples)
		{
			assert(m_mode == 2 && "if faut construire un unique arbre pour calculer la heatmap");
			int nb = m_Tree.NbBoxHit(ray, &m_Tree);	
			double ratio = pow((nb / (double)m_nbNodes), 1) / 10;
			return RGBColor(ratio*2, ratio*3, ratio);
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		RGBColor sendRayNormals(Ray const & ray, int depth, int maxDepth, int diffuseSamples, int specularSamples)
		{
			CastedRay cRay = CastedRay(ray);
			Intersect(cRay, 0, ::std::numeric_limits<double>::max());
			if (!cRay.validIntersectionFound()) return RGBColor(0, 0, 0);
			const RayPrimitiveIntersection & rti = cRay.intersectionFound();
			const Primitive * tri = rti.primitive();
			const Math::Vector3f & p = rti.intersection(); // point d'intersection
			const double u_coor = rti.uTriangleValue();
			const double v_coor = rti.vTriangleValue();
			Math::Vector3f & normal = tri->sampleNormal(p, u_coor, v_coor); // Par rapport au point
			//if (m_isNormalMapping) normal = tri->sampleNormalMap(p, u_coor, v_coor, normal);
			if (normal*ray.direction() > 0.0) normal = -normal;
			if (m_isNormalMapping) {
				Math::Vector3f new_normal = tri->sampleNormalMap(p, u_coor, v_coor	, normal);
				if (new_normal*ray.direction() < 0.0) normal = new_normal;
			}
			//if (m_isNormalMapping) {
			//	Math::Vector3f new_normal = tri->sampleNormalMap(p, u_coor, v_coor, normal);
			//	double cosinus = -new_normal * ray.direction();
			//	if (cosinus < 0.0) {
			//		double angle = acos(-cosinus);
			//		Math::Vector3f axis = new_normal ^ -ray.direction();
			//		Math::Quaternion<double> q(axis, angle);
			//		new_normal = q.rotate(-ray.direction());
			//	}
			//	//if (new_normal*ray.direction() < 0.0) 
			//	normal = new_normal;
			//}
			return RGBColor(pow((1 + normal[0]), 2) / 4, pow((1 + normal[1]), 2) / 4, pow((1 + normal[2]), 2) / 4);
		}

		

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	void Scene::compute(int maxDepth)
		///
		/// \brief	Computes a rendering of the current scene, viewed by the camera.
		/// 		
		/// \author	F. Lamarche, Universit� de Rennes 1
		/// \date	04/12/2013
		///
		/// \param	maxDepth	The maximum recursive depth.
		/// \param  subPixelDivision subpixel subdivisions to handle antialiasing
		////////////////////////////////////////////////////////////////////////////////////////////////////
		void compute(int maxDepth, int subPixelDivision = 1, int passPerPixel = 1, bool heatmap = false, bool hilbert = false, bool renderEmissive = false, bool isNoise = false, int stratification = 1)
		{
			//m_lightSampler.setSubSampling(stratification, m_emissivePointLights);
			m_stratification = stratification;
			m_stratification2 = pow(stratification, 2);
			m_indirect = true;
			m_step = 1. / stratification;
			m_renderEmissive = renderEmissive;
			m_isNoise = isNoise;
			int n = m_visu->height();
			vector<vector<int>> positions;
			if (hilbert) {
				if (!isPower(2, n) || n != m_visu->width()) {
					::std::cout << "hilbert : mauvaises dimensions" << ::std::endl;
					return;
				}
				HilbertTree H;
				::std::cout << "L'image fait " << n << " par " << n << ::std::endl;
				int nb = int(round(log(n) / log(2)));
				::std::cout << "construction de l'arbre de hilbert avec " << nb << " subdivisions" << ::std::endl;
				H.build(nb);
				::std::cout << "extraction du chemin" << ::std::endl;
				positions = H.getPath();
			}
			// We prepare the light sampler (the sampler only stores triangles with a non null renderEmissive component).
			for (auto it = m_geometries.begin(), end = m_geometries.end(); it != end; ++it)
			{
				// Pour afficher s'il ya des objets emissifs
				// ::std::cout << it->second.getEmissive() << ::std::endl;
				// Pour avoir uniquement des lumi�res surfaciques
				m_lightSampler.add(it->second);

				// Si l'on veut que les surfaces �missives soit quand m�me affich�es
				//if (m_lightSampler.hasLights()) {
				//	PointLight pl = m_lightSampler.generate();
				//	pl = PointLight(pl.position(), pl.color()*0.01);
				//	m_pointLights.push_back(pl);
				//}
			}

			// Step on x and y for subpixel sampling
			double step = 1.0f / subPixelDivision;
			// Table accumulating values computed per pixel (enable rendering of each pass)
			::std::vector<::std::vector<::std::pair<int, RGBColor> > > pixelTable(m_visu->width(), ::std::vector<::std::pair<int, RGBColor> >(m_visu->width(), ::std::make_pair(0, RGBColor())));

			// 1 - Rendering time
			LARGE_INTEGER frequency;        // ticks per second
			LARGE_INTEGER t1, t2;           // ticks
			double elapsedTime;
			// get ticks per second
			QueryPerformanceFrequency(&frequency);
			// start timer
			QueryPerformanceCounter(&t1);
			// Rendering pass number
			m_pass = 0;
			// Rendering
			for (int passPerPixelCounter = 0; passPerPixelCounter < passPerPixel; ++passPerPixelCounter)
			{
				for (double xp_ = -0.5; xp_ < 0.5; xp_ += step)
				{
					for (double yp_ = -0.5; yp_ < 0.5; yp_ += step)
					{
						::std::cout << "Pass: " << m_pass << "/" << passPerPixel * subPixelDivision * subPixelDivision << ::std::endl;
						++m_pass;
						// Sends primary rays for each pixel (uncomment the pragma to parallelize rendering)
						if (hilbert) {
#pragma omp parallel for schedule(dynamic)//, 10)//guided)//dynamic)
							for (int i = 0; i < positions.size(); ++i) {
								int x = positions[i][0];
								int y = positions[i][1];
								double xp = xp_ + (double(rand()) / RAND_MAX) * step;
								double yp = yp_ + (double(rand()) / RAND_MAX) * step;
								RGBColor result;
								if (heatmap)
									result = sendRayHeatMap(m_camera.getRay(((double)x + xp) / m_visu->width(), ((double)y + yp) / m_visu->height()), 0, maxDepth, m_diffuseSamples, m_specularSamples);
								else
									result = sendRay(m_camera.getRay(((double)x + xp) / m_visu->width(), ((double)y + yp) / m_visu->height()), 0, maxDepth, m_diffuseSamples, m_specularSamples);

								// Noise
								if (m_isNoise) // n�cessaire d'encapsuler pour les perfs et pas calculer un rdn
								{
									std::random_device rd;  //Will be used to obtain a seed for the random number engine
									std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
									std::uniform_real_distribution<> dis(0.0, 1.0);
									if ((dis(gen) > m_noiseRatio)) result = result * RGBColor(0.5, 0.5, 0.5);
								}

								// Accumulation of ray casting result in the associated pixel
								::std::pair<int, RGBColor> & currentPixel = pixelTable[x][y];
								currentPixel.first++;
								currentPixel.second = currentPixel.second + result;
								// Pixel rendering (with simple tone mapping)
//#pragma omp critical (visu)
								m_visu->plot(x, y, pixelTable[x][y].second / (double)(pixelTable[x][y].first) * 10);
								// Update par "LIGNE"
								if (i%n == 0) {
#pragma omp critical (visu)
									m_visu->update();
								}

							}
						}
						else {
#pragma omp parallel for schedule(dynamic)//, 10)//guided)//dynamic)
							for (int y = 0; y < n; ++y)
							{
								for (int x = 0; x < n; ++x)
								{
									double xp = xp_ + (double(rand()) / RAND_MAX) * step;
									double yp = yp_ + (double(rand()) / RAND_MAX) * step;
									RGBColor result;
									if (heatmap)
										result = sendRayHeatMap(m_camera.getRay(((double)x + xp) / m_visu->width(), ((double)y + yp) / m_visu->height()), 0, maxDepth, m_diffuseSamples, m_specularSamples);
									else
										result = sendRay(m_camera.getRay(((double)x + xp) / m_visu->width(), ((double)y + yp) / m_visu->height()), 0, maxDepth, 0, m_diffuseSamples, m_specularSamples);

									// Noise
									if (m_isNoise) // n�cessaire d'encapsuler pour les perfs et pas calculer un rdn
									{
										std::random_device rd;  //Will be used to obtain a seed for the random number engine
										std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
										std::uniform_real_distribution<> dis(0.0, 1.0);
										if ((dis(gen) > m_noiseRatio)) result = result * RGBColor(0.5, 0.5, 0.5);
									}

									// Accumulation of ray casting result in the associated pixel
									::std::pair<int, RGBColor> & currentPixel = pixelTable[x][y];
									currentPixel.first++;
									currentPixel.second = currentPixel.second + result;
									// Pixel rendering (with simple tone mapping)
//#pragma omp critical (visu)
									m_visu->plot(x, y, pixelTable[x][y].second / (double)(pixelTable[x][y].first) * 10);
								}
								// Update par LIGNE
//#pragma omp critical (visu)
//								m_visu->update();
							}
							// Updates the rendering context (per pass)
							m_visu->update();
							// We print time for each pass
							QueryPerformanceCounter(&t2);
							elapsedTime = (double)(t2.QuadPart - t1.QuadPart) / (double)frequency.QuadPart;
							double remainingTime = (elapsedTime / m_pass)*(passPerPixel * subPixelDivision * subPixelDivision - m_pass);
							::std::cout << "time: " << elapsedTime << "s. " << ", remaining time: " << remainingTime << "s. " << ", total time: " << elapsedTime + remainingTime << ::std::endl;
						}
						
					}
				}
			}
			// stop timer
			QueryPerformanceCounter(&t2);
			elapsedTime = (double)(t2.QuadPart - t1.QuadPart) / (double)frequency.QuadPart;
			::std::cout << "time: " << elapsedTime << "s. " << ::std::endl;
			//saveImagePPM(pixelTable, int(elapsedTime));
			saveImageBMP(pixelTable, int(elapsedTime));
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////
		/// \fn	void Scene::computeRealTime(int maxDepth)
		///
		/// \brief	Computes a rendering of the current scene, viewed by the camera, each frame update.
		/// 		
		/// \author	Paul-Elie Pipelin
		/// \date	30/01/2019
		///
		/// \param	maxDepth	The maximum recursive depth.
		/// \param  subPixelDivision subpixel subdivisions to handle antialiasing
		////////////////////////////////////////////////////////////////////////////////////////////////////
		void computeRealTime(int maxDepth, int subPixelDivision = 1, double facteur = 1, float moveSpeed = 1.0f, bool heatmap = false, bool renderEmissive = false, bool isNoise = false, int stratification = 1, int total_time = numeric_limits<int>::max())
		{
			print_guide();
			m_move_speed = moveSpeed;
			m_rotate_speed = 0.1;
			m_stratification = stratification;
			m_stratification2 = pow(stratification, 2);
			double max_grey = 0;
			m_step = 1. / stratification;
			m_renderEmissive = renderEmissive;
			m_isNoise = isNoise;
			int n = m_visu->height();
			int automatic = 0;
			bool bilinear = false;
			bool normals = false;
			bool fps = false;
			bool locked = false;
			vector<vector<int>> positions;
			// We prepare the light sampler (the sampler only stores triangles with a non null renderEmissive component).
			for (auto it = m_geometries.begin(), end = m_geometries.end(); it != end; ++it)
			{
				// Pour afficher s'il ya des objets emissifs
				// ::std::cout << it->second.getEmissive() << ::std::endl;
				// Pour avoir uniquement des lumi�res surfaciques, sert pour les lampions de tibetHouse
				m_lightSampler.add(it->second);
			}

			// Step on x and y for subpixel sampling
			double step = 1.0f / subPixelDivision;
			// Table accumulating values computed per pixel (enable rendering of each pass)
			::std::vector<::std::vector<::std::pair<int, RGBColor> > > pixelTable(m_visu->width() / facteur, ::std::vector<::std::pair<int, RGBColor> >(m_visu->width() / facteur, ::std::make_pair(0, RGBColor())));
			bool flush = false;
			bool keyboardInterrupt = false;
			bool decreasingFactor = false;
			::chrono::high_resolution_clock::time_point lastFrame = ::chrono::high_resolution_clock::now();
			::chrono::high_resolution_clock::time_point lastStopMove = ::chrono::high_resolution_clock::now();

			// 1 - Rendering time
			LARGE_INTEGER frequency;        // ticks per second
			LARGE_INTEGER t1, t2;           // ticks
			double elapsedTime;
			// get ticks per second
			QueryPerformanceFrequency(&frequency);
			// start timer
			QueryPerformanceCounter(&t1);
			// Rendering
			m_pass = 0;
			while(true)
			{
				const ::chrono::duration<double, std::ratio<1, 1>> timeSinceLastFrame = ::chrono::high_resolution_clock::now() - lastFrame;
				const ::chrono::duration<double, std::ratio<1, 1>> timeSinceLastInput = ::chrono::high_resolution_clock::now() - m_lastInput;
				::chrono::duration<double, std::ratio<1, 1>> timeSinceLastMove = ::chrono::high_resolution_clock::now() - lastStopMove;
				double energy = m_visu->energy() / (m_visu->height()*m_visu->width());
				//m_records.push_back(std::make_pair(energy, m_pass));
				const double currentFps = 1 / timeSinceLastFrame.count();
				if(fps)::std::cout << "FPS : " << currentFps << " facteur : " <<facteur<<" pass : "<<m_pass<<", "<< elapsedTime<<" s"<< " e="<< energy<<::std::endl;
				lastFrame = ::chrono::high_resolution_clock::now();
				// Pas r�ussi � faire de keyboardInterrupt dans toutes les boucles � cause de l'asynchronisme du pragma
				// !flush empeche un segfault 
				// m_seuilInput * 40 correspond � 2s 
				if (/*!locked &&*/ !keyboardInterrupt && facteur > 1 && !flush && timeSinceLastMove.count() > m_seuilInput * 10  && automatic==0) {
					lastStopMove = ::chrono::high_resolution_clock::now(); // pour qu'a chaque palier on laisse 0.5(ou autre) secondes le temps de se rendre compte si c'est bien la zone que l'on veut cibler
					::std::cout << "Baisse Facteur : " << facteur << ::std::endl;
					--facteur;
					flush = true;
				}
				//::std::cout << keyboardInterrupt <<","<< currentFps<<","<< flush<<::std::endl;
				if (keyboardInterrupt)
				{
					lastStopMove = ::chrono::high_resolution_clock::now();
				}
				if (!locked && keyboardInterrupt && currentFps < 15 && facteur < 10 && ((!decreasingFactor && automatic == 1) || automatic == 0)) {
					if(facteur == 1) facteur = 3;
					else ++facteur;
					::std::cout << "Augmentation Facteur : " << facteur << ::std::endl;
					flush = true;
				}
				decreasingFactor = false;
				if (flush) {
					flushPixelTable(pixelTable, facteur);
					m_pass = 0;
					m_records.clear();
					QueryPerformanceCounter(&t1);
				}
				keyboardInterrupt = flush = false; // n�cessaire car le keyboardInterrupt ne peut pas globaliser les event
				for (double xp_ = -0.5; xp_ < 0.5 && !keyboardInterrupt; xp_ += step)
				{
					for (double yp_ = -0.5; yp_ < 0.5 && !keyboardInterrupt; yp_ += step)
					{
						++m_pass;
						QueryPerformanceCounter(&t2);
						if (facteur == 1 && ((double)(t2.QuadPart - t1.QuadPart) / (double)frequency.QuadPart) > total_time) {
							saveImageBMP(pixelTable, int(elapsedTime));
							return;
						}
						double new_facteur = facteur, save = facteur;
						bool safe = true;
						if (facteur > 1) {
							QueryPerformanceCounter(&t2);
							elapsedTime = (double)(t2.QuadPart - t1.QuadPart) / (double)frequency.QuadPart;
							listenKeyboard(elapsedTime, timeSinceLastInput, keyboardInterrupt, flush, heatmap, facteur, automatic, bilinear, m_move_speed , fps, m_renderEmissive, locked, decreasingFactor, pixelTable, normals, m_rotate_speed, m_records);
							new_facteur = facteur;
							facteur = save;
							safe = new_facteur == save;
						}
					
						const int fixedForOMP = n - facteur;
#pragma omp parallel for schedule(dynamic)//, 10)//guided)//dynamic)
						for (int y = 0; y < fixedForOMP; y += int(facteur))
						{
							for (int x = 0; x < fixedForOMP; x += facteur)
							{
								double xp = xp_ + (double(rand()) / RAND_MAX) * step;
								double yp = yp_ + (double(rand()) / RAND_MAX) * step;
								//double xp = xp_;
								//double yp = yp_;
								//double xp = 0;
								//double yp = 0;
								//double xp = double(rand()) / RAND_MAX;
								//double yp = double(rand()) / RAND_MAX;
								
								RGBColor result;
								if (heatmap)
									result = sendRayHeatMap(m_camera.getRay(((double)x + xp) / m_visu->width(), ((double)y + yp) / m_visu->height()), 0, maxDepth, m_diffuseSamples, m_specularSamples);
								else if(normals)
									result = sendRayNormals(m_camera.getRay(((double)x + xp) / m_visu->width(), ((double)y + yp) / m_visu->height()), 0, maxDepth, m_diffuseSamples, m_specularSamples);
								else
									result = sendRay(m_camera.getRay(((double)x + xp) / m_visu->width(), ((double)y + yp) / m_visu->height()), 0, maxDepth, m_diffuseSamples, m_specularSamples);
								

								// Noise
								if (m_isNoise) // n�cessaire d'encapsuler pour les perfs et pas calculer un rdn
								{
									std::random_device rd;  //Will be used to obtain a seed for the random number engine
									std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
									std::uniform_real_distribution<> dis(0.0, 1.0);
									if ((dis(gen) > m_noiseRatio)) result = result * RGBColor(0.5, 0.5, 0.5);
								}

								// Accumulation of ray casting result in the associated pixel
								if (x / facteur >= pixelTable.size() || y / facteur > pixelTable[x / facteur].size()) ::std::cout << x / facteur << "," << y / facteur << ::std::endl;
								::std::pair<int, RGBColor> & currentPixel = pixelTable[x / facteur][y / facteur];
								currentPixel.first++;
								currentPixel.second = currentPixel.second + result;

								// Pixel rendering (with simple tone mapping)
								
								if (facteur == 1) {
//#pragma omp critical (visu)
									 m_visu->plot(x, y, currentPixel.second / (double)(currentPixel.first) * 10);
								}
							}
							// Update par LIGNE
//#pragma omp critical (visu)
//							m_visu->update();
						}
						facteur = new_facteur;
						

						if (facteur == 1 && facteur == save)
						{
							for (int y = 0; y < n; y += facteur)
							{
								for (int x = 0; x < n; x += facteur)
								{
									m_visu->plot(x, y, pixelTable[x][y].second / (double)(pixelTable[x][y].first) * 10);
								}
							}
						}
						else
						{
							bilinear ? agrandissement_bilineaire(pixelTable, facteur) : agrandissement(pixelTable, facteur );
						}
						m_visu->update();
						// Updates the rendering context (per pass)
						if (facteur == 1) {
							QueryPerformanceCounter(&t2);
							elapsedTime = (double)(t2.QuadPart - t1.QuadPart) / (double)frequency.QuadPart;
							listenKeyboard(elapsedTime, timeSinceLastInput, keyboardInterrupt, flush, heatmap, facteur, automatic, bilinear, m_move_speed, fps, m_renderEmissive, locked, decreasingFactor, pixelTable, normals, m_rotate_speed, m_records);
						}
						
						if (facteur != 1 || keyboardInterrupt) break;
						if (m_stratification > 1) {
							m_u++;
							
							if (m_u >= m_stratification) {
								m_u = 0;
								m_v++;
								if (m_v >= m_stratification) {
									m_v = 0;
									std::cout << "reset" << std::endl;
								}
							}
						}
						//double energy = m_visu->energy() / (m_visu->height()*m_visu->width());
						//m_records.push_back(std::make_pair(energy, m_pass));
					}
					if (facteur != 1 || keyboardInterrupt) break;
				}
			}
		}

		void listenKeyboard(
			double elapsedTime,
			const ::chrono::duration<double, std::ratio<1, 1>> & timeSinceLastInput,
			bool & keyboardInterrupt,
			bool & flush,
			bool & heatmap,
			double & facteur,
			int & automatic,
			bool & bilinear,
			double & moveSpeed,
			bool & fps,
			bool & renderEmissive,
			bool & locked,
			bool & decreasingFactor,
			::std::vector<::std::vector<::std::pair<int, RGBColor> > > & pixelTable,
			bool & normals,
			double & rotateSpeed,
			const std::vector<std::pair<double,int>> & m_records
			) {
			keys[0x48] = GetAsyncKeyState(0x48); // H 
			keys[0x5A] = GetAsyncKeyState(0x5A); // Z // avance
			keys[0x51] = GetAsyncKeyState(0x51); // Q // translation gauche
			keys[0x53] = GetAsyncKeyState(0x53); // S // recule
			keys[0x44] = GetAsyncKeyState(0x44); // D // translation droite
			keys[0x54] = GetAsyncKeyState(0x54); // T // toggle
			keys[0x41] = GetAsyncKeyState(0x41); // A // toggle -> automatic
			keys[VK_ADD] = GetAsyncKeyState(0x6B); // + // toggle facteur++
			keys[VK_SUBTRACT] = GetAsyncKeyState(0x6D); // - // toggle -> facteur--
			keys[0x42] = GetAsyncKeyState(0x42); // B // toggle -> bilineaire
			keys[VK_UP] = GetAsyncKeyState(VK_UP); // regarde en haut
			keys[VK_DOWN] = GetAsyncKeyState(VK_DOWN); // regarde en bas
			keys[VK_SPACE] = GetAsyncKeyState(VK_SPACE); // s'�l�ve
			keys[VK_LCONTROL] = GetAsyncKeyState(VK_LCONTROL); // descend
			keys[VK_LEFT] = GetAsyncKeyState(VK_LEFT); // rotation gauche
			keys[VK_RIGHT] = GetAsyncKeyState(VK_RIGHT); // rotation droite
			keys[0x46] = GetAsyncKeyState(0x46); // F : fps
			keys[0x49] = GetAsyncKeyState(0x49); // I :  toggle emissive
			keys[VK_F1] = GetAsyncKeyState(0x70); // f1 :  lock
			keys[VK_F2] = GetAsyncKeyState(0x71); // f2 : save
			keys[VK_F3] = GetAsyncKeyState(VK_F3); // f2 : save
			keys[0x4E] = GetAsyncKeyState(0x4E); // N :  normals
			keys[0x47] = GetAsyncKeyState(0x47); // G :  indirect
			keys[0x4C] = GetAsyncKeyState(0x4C); // D :  m_renderDirectionnalLights
			keys[0x50] = GetAsyncKeyState(0x50); // P :  m_renderPointLights
			keys[VK_RCONTROL] = GetAsyncKeyState(VK_RCONTROL); // ctrl : - m_stratification 
			keys[VK_RSHIFT] = GetAsyncKeyState(VK_RSHIFT); // shift : s+ m_stratification
			keys[0x55] = GetAsyncKeyState(0x55); // U : toggle stratification
			keys[VK_NUMPAD7] = GetAsyncKeyState(VK_NUMPAD7); // 8 : move speed up ( * 1.10)
			keys[VK_NUMPAD4] = GetAsyncKeyState(VK_NUMPAD4); // 5 : move speed down ( / 1.10)
			keys[VK_NUMPAD8] = GetAsyncKeyState(VK_NUMPAD8); // 8 : rotate speed up ( * 1.10)
			keys[VK_NUMPAD5] = GetAsyncKeyState(VK_NUMPAD5); // 5 : rotate speed down ( / 1.10)
			keys[VK_NUMPAD9] = GetAsyncKeyState(VK_NUMPAD9); // 9 : scale plane distance up ( * 1.10)
			keys[VK_NUMPAD6] = GetAsyncKeyState(VK_NUMPAD6); // 6 : scale plane distance down ( / 1.10)
			keys[VK_F4] = GetAsyncKeyState(VK_F4); // save records
			keys[0x4B] = GetAsyncKeyState(0x4B); // K normal mapping

			//cout << ((keys[VK_SPACE] || keys[VK_LCONTROL] || keys[0x5A] || keys[0x53] || keys[0x51] || keys[0x44] || keys[VK_UP] || keys[VK_DOWN] || keys[VK_LEFT] || keys[VK_RIGHT])&& 0x8000 && timeSinceLastInput.count() > m_seuilInput) << endl;
			if (keys[VK_F1] && 0x8000 && (timeSinceLastInput.count() > m_seuilInput * 4)) {
				m_lastInput = ::chrono::high_resolution_clock::now();
				//keyboardInterrupt = true;
				//flush = true;
				locked = !locked;
				cout << (locked ? "Keyboard locked" : "Keyboard unlocked") << endl;
			}

			// Il est n�cessaire de faire m_seuilInput * 4 car  il s'agit d'un toggle est non d'un d�placement qui serait lui continu dans le temps

			if ( !locked && (keys[0x55] || keys[VK_RCONTROL] || keys[VK_RSHIFT] || keys[0x50] || keys[0x4C] || keys[0x4E] || keys[0x47] || keys[VK_ADD] ||  keys[0x48] || keys[VK_SUBTRACT] || keys[0x41] || keys[0x42] || keys[0x46] || keys[0x49] || keys[0x4B]) && 0x8000 && (timeSinceLastInput.count() > m_seuilInput * 4)) {
				m_lastInput = ::chrono::high_resolution_clock::now();

				if (keys[0x48]) {
					keyboardInterrupt = true;
					flush = true;
					heatmap = !heatmap;
					::std::cout << "Toggle heatmaps " << heatmap << ::std::endl;
				}
				if (keys[VK_ADD]) {
					keyboardInterrupt = true;
					flush = true;
					facteur++;
					cout << "Facteur : " << facteur << endl;
				}
				if (keys[VK_SUBTRACT]) {
					if (facteur > 1) {
						keyboardInterrupt = true;
						flush = true;
						facteur--;
						decreasingFactor = true;
						cout << "Facteur : " << facteur << endl;
					}
					
				}
				if (keys[0x41]) {
					keyboardInterrupt = true;
					flush = true;
					automatic++;
					automatic = automatic % 3;
					
					cout <<(automatic==0 ? "Automatic" : automatic == 1  ?"Assiste (augmente le facteur si deplacements)" : "Manuel") << " ("<<automatic<<")"<<endl;
				}
				if (keys[0x42]) {
					keyboardInterrupt = true;
					flush = true;
					bilinear = !bilinear;
					cout << (bilinear ? "Bilin�aire" : "Duplication") << endl;
				}
				if (keys[0x46]) {
					fps = !fps;
					cout << (fps ? "Affichage des fps et du facteur" : "Masquage des fps et du facteur") << endl;
				}
				if (keys[0x49]) {
					keyboardInterrupt = true;
					flush = true;
					renderEmissive = !renderEmissive;
					cout << (renderEmissive ? "Lumi�res surfaciques" : "Lumi�res ponctuelles/directionnelles") << endl;
				}
				if (keys[0x4E]) {
					keyboardInterrupt = true;
					flush = true;
					normals = !normals;
					cout << (normals ? "Affichage des normales" : "Pas d'affichage des normales") << endl;
				}
				if (keys[0x4B]) {
					keyboardInterrupt = true;
					flush = true;
					m_isNormalMapping = !m_isNormalMapping;
					cout << (m_isNormalMapping ? "Normal mapping" : "No normal mapping") << endl;
				}
				if (keys[0x47]) {
					keyboardInterrupt = true;
					flush = true;
					m_indirect = !m_indirect;
					cout << (m_indirect ? "Indirect" : "Pas d'indirect") << endl;
				}
				if (keys[0x4C]) {
					if (m_directionalLights.empty()) {
						cout << "La scene ne comporte pas de lumieres directionnelles." << endl;

					}
					else {
						keyboardInterrupt = true;
						flush = true;
						m_renderDirectionalLights = !m_renderDirectionalLights;
						cout << (m_renderDirectionalLights ? "Lumiere directionnelles activees" : "Lumieres directionnelles d�sactivees") << endl;
					}		
				}
				if (keys[0x50]) {
					if (m_pointLights.empty()) {
						cout << "La scene de comporte pas de lumieres ponctuelles." << endl;
					}
					else {
						keyboardInterrupt = true;
						flush = true;
						m_renderPointLights = !m_renderPointLights;
						cout << (m_renderPointLights ? "Lumiere ponctuelles activees" : "Lumieres ponctuelles desactivees") << endl;
					}
				}
				if (keys[VK_RCONTROL] && m_stratification > 1) {
					keyboardInterrupt = true;
					flush = true;
					m_stratification--;
					m_stratification2 = pow(m_stratification, 2);
					m_step = 1. / m_stratification;
					m_u = 0;
					m_v = 0;
					::std::cout << "Stratification : " << m_stratification2 << ::std::endl;
				}
				if (keys[VK_RSHIFT]) {
					keyboardInterrupt = true;
					flush = true;
					m_stratification++;
					m_stratification2 = pow(m_stratification, 2);
					m_step = 1. / m_stratification;
					m_u = 0;
					m_v = 0;
					::std::cout << "Stratification : " << m_stratification2 << ::std::endl;
				}
				if (keys[0x55]) {
					keyboardInterrupt = true;
					flush = true;
					m_isStratification = !m_isStratification;
					::std::cout << (m_isStratification ? "Stratification activee" : "Stratification desactivee") <<" ("<< m_stratification2<<" shadow rays)" << ::std::endl;
				}
				
			}
			// X : horizontal
			// Y : Hauteur
			// Z : Profondeur
			// Mouvements
			if (!locked && (keys[VK_SPACE] || keys[VK_LCONTROL] || keys[0x5A] || keys[0x53] || keys[0x51] || keys[0x44] || keys[VK_UP] || keys[VK_DOWN] || keys[VK_LEFT] || keys[VK_RIGHT]) && 0x8000/*check if high-order bit is set (1 << 15)*/ && timeSinceLastInput.count() > m_seuilInput) {
				//::std::cout << "Goes " << (keys[VK_SPACE] ? "UP" : "DOWN") << ::std::endl;
				m_lastInput = ::chrono::high_resolution_clock::now();
				keyboardInterrupt = true;
				flush = true;

				//Translations
				// Axe Y
				if (keys[VK_SPACE] || keys[VK_LCONTROL]) {
					Math::Vector3f translation(.0f);
					translation[2] = keys[VK_SPACE] ? moveSpeed : -moveSpeed;
					m_camera.translate(translation);
				}

				// Axe Z
				if (keys[0x5A] || keys[0x53] || keys[0x51] || keys[0x44]) {
					Math::Vector3f translation(.0f);
					if (keys[0x5A] || keys[0x53]) {
						translation[1] += keys[0x53] ? -1 : 1;
					}
					// Axe X
					if (keys[0x51] || keys[0x44]) {
						translation[0] += keys[0x51] ? -1 : 1;
					}
					translation = translation.normalized();
					translation[0] *= moveSpeed;
					translation[1] *= moveSpeed;
					translation[2] *= moveSpeed;
					m_camera.translateLocal(translation);
				}
				// Rotations
				if (keys[VK_UP] || keys[VK_DOWN] || keys[VK_RIGHT] || keys[VK_LEFT]) {
					Math::Vector3f vx, vy;
					// Haut/Bas
					if (keys[VK_UP] || keys[VK_DOWN]) {
						m_camera.rotate(keys[VK_UP] ? 'u' : 'd', rotateSpeed);
					}
					// Droite/Gauche
					if (keys[VK_RIGHT] || keys[VK_LEFT]) {
						m_camera.rotate(keys[VK_RIGHT] ? 'r' : 'l', rotateSpeed);
					}
				}
			}
			// Camera parameters
			if (!locked && (keys[VK_NUMPAD7] || keys[VK_NUMPAD4] || keys[VK_NUMPAD8] || keys[VK_NUMPAD5] || keys[VK_NUMPAD9] || keys[VK_NUMPAD6])) {
				if (keys[VK_NUMPAD9] || keys[VK_NUMPAD6]) {
					keyboardInterrupt = true;
					flush = true;
					m_camera.m_planeDistance *= keys[VK_NUMPAD9] ? 1.1 : 1. / 1.1;
					m_camera.computeParameters();
					::std::cout << "Plane distance " << (keys[VK_NUMPAD9] ? "increased" : "decreased") << " to " << m_camera.m_planeDistance << "." << ::std::endl;
				}
				if (keys[VK_NUMPAD7] || keys[VK_NUMPAD4]) {
					moveSpeed *= keys[VK_NUMPAD7] ? 1.1 : 1. / 1.1;
					::std::cout << "Camera move speed " << (keys[VK_NUMPAD7] ? "increased" : "decreased") << " to " << moveSpeed << "." << ::std::endl;
				}
				if (keys[VK_NUMPAD8] || keys[VK_NUMPAD5]) {
					rotateSpeed *= keys[VK_NUMPAD8] ? 1.1 : 1. / 1.1;
					::std::cout << "Camera rotate speed " << (keys[VK_NUMPAD8] ? "increased" : "decreased") << " to " << rotateSpeed << "." << ::std::endl;
				}
				
			}
			if (!locked && keys[VK_F2]) {
				::std::cout << "Elapsed time : " << elapsedTime << ::std::endl;
				saveImageBMP(pixelTable, int(elapsedTime));
			}
			if (!locked && keys[VK_F3]) {
				::std::cout << "Elapsed time : " << elapsedTime << ::std::endl;
				saveImagePPM(pixelTable, int(elapsedTime));
			}
			if (!locked && keys[VK_F4]) {
				::std::cout << "Elapsed time : " << elapsedTime << ::std::endl;
				saveRecordsCSV(pixelTable, int(elapsedTime));
			}
		}

		void saveImagePPM(const ::std::vector<::std::vector<::std::pair<int, RGBColor> > > & pixelTable, int elapsedTime = 0) {
			::std::cout << "Saving PPM..." << ::std::endl;
			::std::ofstream myfile;
			mkdir("renderedImages");
			myfile.open("renderedImages/" + m_sceneName
				+ "_" + ::std::to_string(pixelTable.size()) + "x" + ::std::to_string(pixelTable[0].size())
				+ "_" + ::std::to_string(m_pass) + "pass"
				+ ((elapsedTime <= 0) ? "" : ("_" + ::std::to_string(elapsedTime) + "s"))
				+ ((m_absorption == 0) ? "" : ("_" + ::std::to_string(m_absorption) + "probaDAbsorption"))
				+ "_"+ (m_isStratification ? ::std::to_string(m_stratification2):"1") +"strat"
				+ ".ppm");
			myfile << "P3 \n" << pixelTable.size() << " " << pixelTable[0].size() << "\n"; // RGB + taille
			myfile << "255 \n"; // Valeur max
			for (int j = 0; j < pixelTable[0].size(); ++j) {
				for (int i = 0; i < pixelTable.size(); ++i) {
					if (pixelTable[i][j].first == 0)
					{
						myfile << 0;
						myfile << " " << 0;
						myfile << " " << 0;
						myfile << "  ";
					}
					else {
						const double tmpR = pixelTable[i][j].second[0] / pixelTable[i][j].first*6;
						const double tmpG = pixelTable[i][j].second[1] / pixelTable[i][j].first*6;
						const double tmpB = pixelTable[i][j].second[2] / pixelTable[i][j].first*6;
						//::std::cout << tmp << ::std::endl;
						myfile << ((int((tmpR / (tmpR + 1)) * 255)));
						myfile << " " << (int((tmpG / (tmpG + 1) * 255)));
						myfile << " " << (int((tmpB / (tmpB + 1) * 255)));
						myfile << "  ";
					}
				}
				myfile << "\n";
			}
			::std::cout << "Saved PPM !"<< ::std::endl;
			myfile.close();
		}
	
		void saveImageBMP(const ::std::vector<::std::vector<::std::pair<int, RGBColor> > > & pixelTable, int elapsedTime = 0) {
			::std::cout << "Saving BMP..." << ::std::endl;
			std::string path = "renderedImages/" + compute_name(pixelTable, elapsedTime) + ".bmp";
			m_visu->saveBMP(path);
			::std::cout << "Saved BMP !"<< ::std::endl;
		}

		void saveRecordsCSV(const ::std::vector<::std::vector<::std::pair<int, RGBColor> > > & pixelTable, int elapsedTime = 0) {
			::std::cout << "Saving Records as CSV file..." << ::std::endl;
			::std::ofstream myfile;
			mkdir("records");
			std::string path = "records/" + compute_name(pixelTable, elapsedTime) + ".csv";
			myfile.open(path);
			for (int i = 0; i < m_records.size()-1; ++i) {
				myfile << m_records[i].second << ";";
			}
			myfile << m_records[m_records.size() - 1].second << std::endl;
			for (int i = 0; i < m_records.size() - 1; ++i) {
				myfile << m_records[i].first << ";";
			}
			myfile << m_records[m_records.size() - 1].first;
			myfile.close();
			::std::cout << "Saved CSV !" << ::std::endl;
		}

		bool isPower(int x, long int y)
		{
			// The only power of 1 is 1 itself 
			if (x == 1)
				return (y == 1);

			// Repeatedly comput power of x 
			long int pow = 1;
			while (pow < y)
				pow *= x;

			// Check if power of x becomes y 
			return (pow == y);
		}		
		RGBColor interpolation_bilineaire_(::std::vector<::std::vector<::std::pair<int, RGBColor> > > & I, double x, double y) {
			const unsigned int nbl = I.size();
			const unsigned int nbc = I[0].size();
			assert(x < nbc && y < nbl);
			const int c = (int)y;
			const int l = (int)x;
			//distances en x et y au pixel I[l][c]
			const double dx = x - l;
			const double dy = y - c;
			//cout<<" l : "<<l<<", c : "<<c<<endl;
			//v0 v1 v2 v3 valeurs des quatres pixels
			RGBColor v0 = I[l][c].second;
			RGBColor v1 = I[l + 1][c].second;
			RGBColor v2 = I[l][c + 1].second;
			RGBColor v3 = I[l + 1][c + 1].second;
			//u0 u1 interpolations sur l'axe x des valeurs de respectivement v0 <-> v1,  et v2 <-> v3;
			RGBColor u0 = v0*(1 - dx) + v1*dx;
			RGBColor u1 = v2*(1 - dx) + v3*dx;
			//w interpolations sur l'axe y des valeurs de u0 <-> u1
			RGBColor w = u0*(1 - dy) + u1*dy;
			return w / (double)(I[x][y].first) * 10;
		}
		void agrandissement_bilineaire(::std::vector<::std::vector<::std::pair<int, RGBColor> > > & I, double facteur) {
			const unsigned int nbl = I.size();
			const unsigned int nbc = I[0].size();
			const unsigned int new_nbl = (unsigned int)(facteur*nbl);
			const unsigned int new_nbc = (unsigned int)(facteur*nbc);
			//::std::vector<::std::vector<RGBColor> >  Iag(m_visu->width(), ::std::vector<RGBColor>(m_visu->width(), RGBColor()));
#pragma omp parallel for schedule(dynamic)
			for (int i = 0; i < new_nbl - (int)facteur; ++i) {
				for (int j = 0; j < new_nbc - (int)facteur; ++j) {
					RGBColor rgb = interpolation_bilineaire_(I, i / facteur, j / facteur);
					m_visu->plot(i, j, rgb);
				}
			}
		}

		void agrandissement(::std::vector<::std::vector<::std::pair<int, RGBColor> > > & I, double facteur) {
			unsigned int nbl = I.size();
			unsigned int nbc = I[0].size();
			unsigned int new_nbl = (unsigned int)(facteur*nbl);
			unsigned int new_nbc = (unsigned int)(facteur*nbc);
#pragma omp parallel for schedule(dynamic)
			for (int i = 0; i < new_nbl - (int)facteur; ++i) {
				for (int j = 0; j < new_nbc - (int)facteur; ++j) {
					::std::pair<int, RGBColor> p = I[i / facteur][j / facteur];
					m_visu->plot(i, j, p.second/(p.first*.1));
				}
			}
		}
		std::string compute_name(const ::std::vector<::std::vector<::std::pair<int, RGBColor> > > & pixelTable, int elapsedTime = 0) {
			std::string name = m_sceneName;
			name = name + "_" + ::std::to_string(pixelTable.size()) + "x" + ::std::to_string(pixelTable[0].size());
			name = name + "_" + ::std::to_string(m_pass) + "pass";
			name = name + ((elapsedTime <= 0) ? "" : ("_" + ::std::to_string(elapsedTime) + "s"));
			name = name + ((m_absorption == 0) ? "" : ("_" + ::std::to_string(m_absorption) + "probaDAbsorption"));
			name = name + "_" + (m_isStratification ? ::std::to_string(m_stratification2) : "1") + "strat";
			return name;
		}
		void flushPixelTable(::std::vector<::std::vector<::std::pair<int, RGBColor> > > & pixelTable, double facteur) {
			for (::std::vector < ::std::pair<int, RGBColor> > &vect : pixelTable)
			{
				vect.clear();
			}
			pixelTable.clear();
			pixelTable = ::std::vector<::std::vector<::std::pair<int, RGBColor> > >(m_visu->width() / facteur, ::std::vector<::std::pair<int, RGBColor> >(m_visu->width() / facteur, ::std::make_pair(0, RGBColor())));
		}
		void setBackgroundTexture(const std::string & filename)
		{
			m_backgroundTexture = new Texture(filename);
			std::cout << "background texture : " << filename << std::endl;
		}
		RGBColor backgroundColor(const Ray & ray)
		{
			//return RGBColor();
			//return RGBColor(0.5, 0.4, 1)*((cos(v*M_TAU) + 1) / 2) + RGBColor(0, 0, 1)*0.03;
			const Math::Vector3f n = ray.direction();
			const double v = 0.5 - asin(n[2]) / M_PI;	
			//return RGBColor(0.5, 0.4, 1)*((cos(v*M_TAU) + 1) / 2) + RGBColor(0, 0, 1)*0.03;
			if (m_backgroundTexture && m_backgroundTexture->isValid()) {
				const Math::Vector3f n = ray.direction();
				const double u = atan2(n[1], n[0]) / M_TAU + 0.5;
				return m_backgroundTexture->pixel_bilinear(Math::makeVector(u, v));
			}
			return RGBColor();
			//return RGBColor(0.5, 0.4, 1)*((cos(v*M_TAU) + 1) / 2) + RGBColor(0, 0, 1)*0.03;
		}

		void print_guide() {
			std::cout << std::endl;
			std::cout << "##############################################" << std::endl;
			std::cout << "Bienvenue dans le rendu \"temps reel\"." << std::endl;
			std::cout << "Liste des commandes : " << std::endl;
			std::cout << "Z Q S D :  d�placements avant gauche arriere droite" << std::endl;
			std::cout << "Espace Ctrl : monter descendre" << std::endl;
			std::cout << "fl�ches directionnelles :  rotations cam�ra" << std::endl;
			std::cout << "F :  afficher les FPS, energie et temps" << std::endl;
			std::cout << "G : illumination globale" << std::endl;
			std::cout << "I : lumieres surfaciques" << std::endl;
			std::cout << "L : lumieres directionelles" << std::endl;
			std::cout << "P : lumieres ponctuelles" << std::endl;
			std::cout << "K : normal mapping" << std::endl;
			std::cout << "N : visualiser les normales" << std::endl;
			std::cout << "H : visualiser le BVH (ne marche sur toutes les sc�nes)" << std::endl;
			std::cout << "7/4 (pad) :  augmenter/diminuer la vitesse de translation la cam" << std::endl;
			std::cout << "8/5 (pad) :  augmenter/diminuer la vitesse de rotation la cam" << std::endl;
			std::cout << "9/6 (pad) :  augmenter/diminuer la focale de la cam (distance du plan image)" << std::endl;
			std::cout << "+/- (pad) :  augmenter/diminuer le facteur de taille des pixels (on l'apelle focus ensuite \/)" << std::endl;
			std::cout << "A : changer de mode de focus :" << std::endl;
			std::cout << "      Automatic : focus automatique, d�focus automatique lors de deplacements" << std::endl;
			std::cout << "      Assiste : focus manuel, d�focus automatique lors de deplacement" << std::endl;
			std::cout << "      Manuel : focus manuel, d�focus manuel (attention)" << std::endl;
			std::cout << "f1 : lock, pour faire autre chose en attendant un rendu sans tout casser" << std::endl;
			std::cout << "f2 : save en BMP" << std::endl;
			std::cout << "f3 : save en PPM" << std::endl;
			std::cout << "f4 : save l'evolution de l'energie en CSV" << std::endl;
			std::cout << "U : stratification" << std::endl;
			std::cout << "rshift/rctrl : augmenter/diminuer le facteur de stratification" << std::endl;
			
			std::cout << "##############################################" << std::endl;
			std::cout << std::endl;

		}
	};

	

}

#endif
