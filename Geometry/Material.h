#ifndef _Geometry_Material_H
#define _Geometry_Material_H

#include <Geometry/RGBColor.h>
#include <Geometry/Texture.h>
#include "Math/RandomDirection.h";
#include "Geometry/Refraction.h"

namespace Geometry
{
	/** \brief A material definition. */
	class Material
	{
	protected:
		/// <summary> The ambient color </summary>
		RGBColor m_ambientColor ;
		/// <summary> the diffuse color</summary>
		RGBColor m_diffuseColor ;
		/// <summary> The specular color</summary>
		RGBColor m_specularColor ;
		/// <summary> The shininess</summary>
		double    m_shininess ;
		/// <summary> The emissive color</summary>
		RGBColor m_emissiveColor ;
		/// <summary> The filename of the itexture image</summary>
		std::string m_textureFile;
		/// <summary> The texture or nullptr if no texture is associated with this material</summary>
		Texture * m_texture; 
		/// <summary> The filename of the normal map image</summary>
		std::string m_normalMapFile;
		/// <summary> The normal map or nullptr if no normal map is associated with this material</summary>
		Texture * m_normalMap;


		

	public:

		/// REFRACTION sujet � changements
		// Transmitance = part de lumi�re allant dans le mat�riau
		double m_transmitance = 0.0;
		RGBColor  m_transparency = RGBColor::white();
		// Densit� du mat�riau -> pour snell-descartes
		double m_density;

		double m_blur = 0;


		/// <summary>
		/// Initializes a new instance of the <see cref="Material"/> class.
		/// </summary>
		/// <param name="ambientColor">The ambient color.</param>
		/// <param name="diffuseColor">The diffuse color.</param>
		/// <param name="specularColor">The specular color.</param>
		/// <param name="shininess">The shininess.</param>
		/// <param name="emissiveColor">The emissive color.</param>
		Material(RGBColor const & ambientColor = RGBColor(), RGBColor const & diffuseColor = RGBColor(),
		RGBColor specularColor = RGBColor(), double shininess = 1.0, RGBColor const & emissiveColor = RGBColor(), std::string const & textureFile = "", double transmitance = 0, const RGBColor &  transparency = RGBColor::black(), double density = 1, double blur = 0, const std::string & normalMapFile = "")
		: m_ambientColor(ambientColor), m_diffuseColor(diffuseColor), m_specularColor(specularColor),
		m_shininess(shininess), m_emissiveColor(emissiveColor), m_textureFile(textureFile), m_texture(NULL),
		m_transmitance(transmitance),m_transparency(transparency), m_density(density), m_blur(blur),
		m_normalMapFile(normalMapFile), m_normalMap(NULL)
		{}

		/// <summary>
		/// Sets the ambient color.
		/// </summary>
		/// <param name="color">The color.</param>
		void setAmbient(RGBColor const & color)
		{
			m_ambientColor = color;
		}

		/// <summary>
		/// Gets the ambient color.
		/// </summary>
		/// <returns>The ambiant color</returns>
		const RGBColor & getAmbient() const
		{ return m_ambientColor ; }

		/// <summary>
		/// Sets the diffuse color.
		/// </summary>
		/// <param name="color">The color.</param>
		void setDiffuse(RGBColor const & color)
		{
			m_diffuseColor = color;
		}
		
		/// <summary>
		/// Gets the diffuse color.
		/// </summary>
		/// <returns></returns>
		const RGBColor & getDiffuse() const
		{ return m_diffuseColor ; }

		/// <summary>
		/// Sets the specular color.
		/// </summary>
		/// <param name="color">The color.</param>
		void setSpecular(RGBColor const & color)
		{
			m_specularColor = color;
		}

		/// <summary>
		/// Gets the specular color.
		/// </summary>
		/// <returns></returns>
		const RGBColor & getSpecular() const
		{ return m_specularColor ; }

		/// <summary>
		/// Sets the shininess.
		/// </summary>
		/// <param name="s">The shininess.</param>
		void setShininess(double s)
		{
			m_shininess = s;
		}

		/// <summary>
		/// Gets the shininess.
		/// </summary>
		/// <returns></returns>
		const double & getShininess() const
		{ return m_shininess ; }

		/// <summary>
		/// Sets the emissive color.
		/// </summary>
		/// <param name="color">The color.</param>
		void setEmissive(RGBColor const & color)
		{
			m_emissiveColor = color;
		}

		/// <summary>
		/// Gets the emissive color.
		/// </summary>
		/// <returns></returns>
		const RGBColor & getEmissive() const
		{ return m_emissiveColor ; }

		/// <summary>
		/// Sets the texture file.
		/// </summary>
		/// <param name="textureFile">The texture file.</param>
		void setTextureFile(const ::std::string & textureFile)
		{
			m_textureFile = textureFile;
			::std::cout << "Loading texture: "<< m_textureFile << "..." << ::std::flush;
			m_texture = new Texture(m_textureFile);
			if (!m_texture->isValid())
			{
				delete m_texture;
				m_texture = NULL;
				::std::cout << "discarded" << ::std::endl;
			}
			else
			{
				::std::cout << "OK" << ::std::endl;
			}
		}

		void setNormalMapFile(const ::std::string & normalMapFile)
		{
			m_normalMapFile = normalMapFile;
			::std::cout << "Loading normal map: " << m_normalMapFile << "..." << ::std::flush;
			m_normalMap = new Texture(m_normalMapFile);
			if (!m_normalMap->isValid())
			{
				delete m_normalMap;
				m_normalMap = NULL;
				::std::cout << "discarded" << ::std::endl;
			}
			else
			{
				::std::cout << "OK" << ::std::endl;
			}
		}

		/// <summary>
		/// Gets the texture file.
		/// </summary>
		/// <returns></returns>
		const ::std::string & getTextureFile() const
		{
			return m_textureFile;
		}

		/// <summary>
		/// Returns the texture
		/// </summary>
		/// <returns></returns>
		const Texture & getTexture() const
		{
			return *m_texture;
		}

		const Texture & getNormalMap() const
		{
			return *m_normalMap;
		}

		/// <summary>
		/// Tests if a texture is associated with this material.
		/// </summary>
		/// <returns> true if this materail has a texture, false otherwise</returns>
		bool hasTexture() const
		{
			return m_texture != NULL;
		}

		bool hasNormalMap() const
		{
			return m_normalMap != NULL;
		}

		bool isTransparent() const
		{
			return m_transmitance > 0;
		}

		Math::Vector3f scatter(
			const Math::Vector3f & incident,
			const Math::Vector3f & reflected,
			const Math::Vector3f & normal,
			const Material * & currentMat,
			bool & isRefracted,
			bool & isReflected,
			double u = 0.0,
			double v = 0.0,
			double step = 1.0
			)
		const
		{
			Math::Vector3f scattered;
			isRefracted = false;
			isReflected = false;
			if (isTransparent())
			{
				
				bool entering = currentMat == nullptr;
				double n1 = entering ? 1.204 : currentMat->m_density; // 1.204 = densit� de l'air � 20�C
				double n2 = entering ? m_density : 1.204;
				bool critical;
				double R = 1, T = 0;
				Math::Vector3f refracted = refract(incident, normal, n1, n2, critical, R, T, reflected);

				
				if (!critical) {
					if (double(rand()) / RAND_MAX < T*m_transmitance) {
						isRefracted = true;
						isReflected = false;
						scattered = refracted;
						if (m_blur > ::std::numeric_limits<double>::epsilon()) {
							Math::RandomDirection rand(refracted, 5000 / m_blur);
							do {
								scattered = rand.generate();
							} while (scattered * normal > 0); // On veut qu'il soit dans l'autre demi-espace
						}
						return scattered;
					}
				}		
			}

			if (!isRefracted && m_shininess >= 10000) {
				isReflected = true;
				return reflected;
			}
			if (m_shininess <= 1) {
				Math::RandomDirection randDir(normal, m_shininess);
				return randDir.generate(u, v, step);
			}
			Math::RandomDirection randDir(reflected, m_shininess);
			return randDir.generate();
			
		}

		RGBColor brdf(const Math::Vector3f & incident,
			const Math::Vector3f & reflected,
			const Math::Vector3f & scattered,
			const Math::Vector3f & normal,
			bool isRefracted,
			bool isReflected
		)const
		{
			if (isRefracted)
				return m_transparency;
			if (isReflected)
				return m_specularColor;
			// Phong diffuse specular brdf
			double cos_diffuse = normal * scattered;
			double cos_specular = reflected * scattered;
			RGBColor diffuse = m_diffuseColor * cos_diffuse;
			RGBColor specular = !m_specularColor.isBlack() ? m_specularColor * pow(cos_specular, m_shininess) : RGBColor();
			RGBColor phong = diffuse + specular;
			//if (m_shininess == 1) phong = phong / M_PI;
			return phong;
		}

		// Attention � bien delete
		static Material * glass(const RGBColor & diffuse = RGBColor(1,1,1), double blur = 0, double transparency = 1.0)
		{
			return new Material(RGBColor(), diffuse, RGBColor(1.0, 1.0, 1.0), 400.f, RGBColor(), "", transparency, RGBColor::white(), 2.5, blur);
		}





	};
}

#endif
