#pragma once
#include <iostream>
#include <vector>
#include <list>
using namespace std;

//CLASSE ARBRE DE HILBERT
class HilbertTree
{

private:
	//chaque noeud a quatre fils (c'est un quatree)
	HilbertTree * H1;
	HilbertTree * H2;
	HilbertTree * H3;
	HilbertTree * H4;
	
	//chaque noeud a un etat
	//il en existe 4 differents :
	// H (=0) qui se derive par construction en AHHB 
	// A (=1) qui se derive par construction en HAAR
	// R (=2) qui se derive par construction en BRRA 
	// B (=3) qui se derive par construction en RBBH
	int m_etat;//0h1a2rb3

	//chaque noeud a une position, il en y a 4 :
	//en bas � gauche (=0)
	//en bas � droite (=1)
	//en haut � gauche (=2)
	//en haut � droite (=3) 
	int m_pos;//0bg 1bd 2hg 3h
	
	//profondeur du noeud
	int m_depth;

	//est une feuille ?
	bool m_leaf;

	//Fonction permettant d'initialiser les quatres fils
	void bsons() {
		m_leaf = false;
		H1 = new HilbertTree();
		H1->m_depth = m_depth + 1;
		H2 = new HilbertTree();
		H2->m_depth = m_depth + 1;
		H3 = new HilbertTree();
		H3->m_depth = m_depth + 1;
		H4 = new HilbertTree();
		H4->m_depth = m_depth + 1;
	}

	//Fonction permettant de donner aux fils une valeur d'etat et de position
	void setEtPos(int e1, int e2, int e3, int e4, int p1, int p2, int p3, int p4) {
		H1->m_etat = e1;
		H2->m_etat = e2;
		H3->m_etat = e3;
		H4->m_etat = e4;
		H1->m_pos = p1;
		H2->m_pos = p2;
		H3->m_pos = p3;
		H4->m_pos = p4;
	}

	//Fonction parcourant tous les fils recursivement de gauche a droite et stockant la variable position des feuilles
	//-> renvoie la liste des positions de gauche � droite
	list<int> getPathPos() {
		if (m_leaf) {
			list<int> l = { m_pos };
			return l;
		}
		list<int> lh1 = H1->getPathPos();
		list<int> lh2 = H2->getPathPos();
		list<int> lh3 = H3->getPathPos();
		list<int> lh4 = H4->getPathPos();
		//a.splice(a.end(), b) : attache b � la fin de a (juste un pointeur qui bouge)
		lh3.splice(lh3.end(), lh4);
		lh2.splice(lh2.end(), lh3);
		lh1.splice(lh1.end(), lh2);
		return lh1;
	}

public:

	//Constructeur par defaut (seul constructeur)
	HilbertTree() :
		m_leaf(true), m_etat(0), m_depth(0), m_pos(0)
	{}

	//Destructeur
	~HilbertTree() {
		if (!m_leaf){
			delete H1;
			delete H2;
			delete H3;
			delete H4;
		}
	}

	//Fonction permettant de construire l'abre de hilbert a partir du noeud courant jusqu'a une profondeur depth
	void build(int depth) {
		//Si on a atteint la profondeur souhaitee, on ne fait rien
		if (m_depth >= depth) {
			return;
		}
		//Sinon, on construit les fils
		bsons();
		//puis on initialise leur variables d'etat et de position a partir de la variable d'etat courante
		//0 : H, 1 : A, R : 2, B : 3
		if (m_etat == 0) { //0 -> 1003 (h -> ahhb)
			setEtPos(1, 0, 0, 3, 0, 2, 3, 1);
		}
		else if (m_etat == 1) {//1-> 0112 (a->haar) 
			setEtPos(0, 1, 1, 2, 0, 1 ,3 ,2);
		}
		else if (m_etat == 2) {//2->3221 (r->brra) 
			setEtPos(3, 2, 2, 1, 3, 1, 0, 2);
		}
		else if (m_etat == 3) {//3-> 2330 (b->rbbh) 
			setEtPos(2, 3, 3, 0, 3, 2, 0, 1);
		}
		
		//Il ne reste qu'a construir de la m�me mani�re les 4 fils.
		H1->build(depth);
		H2->build(depth);
		H3->build(depth);
		H4->build(depth);
	}

	// nombre de feuilles de l'arbre (= nombre de pixels de la bitmap carree que l'on veut parcourir...)
	int leaves() {
		return m_leaf ? 1 : H1->leaves() + H2->leaves() + H3->leaves() + H4->leaves();
	}
	
	

	//Fonction permettant de recuperer tout le chemin de parcours
	// sous la forme d'un vecteur de doublet (x,y) de taille n*n    (->leaves()*leaves())
	vector<vector<int>> getPath() {
		list<int> pathPosList = getPathPos();
		//initialisation des positions sous forme de vecteur pour le parcours
		vector<int> pos(pathPosList.begin(), pathPosList.end());
		//on commence � (0,0)
		int x(0), y(0);
		//initialisation du chemin � retourner
		vector<vector<int>> path;
		//insertion de la position (0,0)
		path.push_back({ x, y });
		for (size_t i = 0; i < pos.size() - 1; ++i) {
			//le mouvement se fait par rapport a une pos et sa suivante.
			int before = pos[i];
			int after = pos[i + 1];
			//rappel : 0 -> bas a gauche,  1 -> bas a droite, 2 -> haut a gauche, 3-> haut a droite
			//donc selon deux positions on sait si on est all� a gauche (x--), � droite(x++), en haut (y++) ou en bas (y--)
			//CEPENDANT, lorsqu'on se d�place d'une feuille a l'autre, le mouvement est invers� (d'ou le  i%4 == 3 ? a : b;)
			if ((before == 0 && after == 1) || (before == 2 && after == 3)) i%4 == 3 ? x-- : x++;
			else if ((before == 1 && after == 0) || (before == 3 && after == 2)) i%4==3 ? x++ : x--;
			else if ((before == 0 && after == 2) || (before == 1 && after == 3)) i % 4 == 3 ? y--: y++;
			else if ((before == 2 && after == 0) || (before == 3 && after == 1)) i % 4 == 3 ? y++ : y--;
			//une fois qu'on a modifi� les coordoonn�es, on vient ajouter notre point au chemin
			path.push_back({ x, y });
		}
		return path;
	};


};

