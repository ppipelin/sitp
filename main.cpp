#include <Geometry/Texture.h>
#include <Math/Vectorf.h>
#include <Geometry/Ray.h>
#include <Geometry/Triangle.h>
#include <Geometry/CastedRay.h>
#include <stdlib.h>
#include <iostream>
#include <Geometry/RGBColor.h>
#include <Geometry/Material.h>
#include <Geometry/PointLight.h>
#include <Geometry/Camera.h>
#include <Geometry/Cube.h>
#include <Geometry/TessellatedDisk.h>
#include <Geometry/TessellatedCylinder.h>
#include <Geometry/TessellatedCone.h>
#include <Visualizer/Visualizer.h>
#include <Geometry/Scene.h>
#include <Geometry/Cornel.h>
#include <Geometry/Loader3ds.h>
#include <Geometry/BoundingBox.h>
#include <omp.h>
#include <Tree.h>


/// <summary>
/// The directory of the 3D objetcs
/// </summary>
const std::string m_modelDirectory = "..\\..\\Models";
const std::string m_textureDirectory = m_modelDirectory + "\\Textures";

using Geometry::RGBColor ;

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \fn	void createGround(Geometry::Scene & scene)
///
/// \brief	Adds a ground to the scene.
///
/// \author	F. Lamarche, Université de Rennes 1
/// \date	03/12/2013
///
/// \param [in,out]	scene	The scene.
////////////////////////////////////////////////////////////////////////////////////////////////////
void createGround(Geometry::Scene & scene)
{
	Geometry::BoundingBox sb = scene.getBoundingBox();
	// Non emissive 
	Geometry::Material * material = new Geometry::Material(RGBColor(), RGBColor(0.5, 0.5, 0.5), RGBColor(0.5, 0.5, 0.5)*8, 1000.0f); // Non existing material...
	//Geometry::Material * material = new Geometry::Material(RGBColor(), RGBColor(0., 0., 0.), RGBColor(1., 1., 1.), 10000.0f);
	//Geometry::Material * material = new Geometry::Material(RGBColor(), RGBColor(1.0f, 1.0f, 1.0f), RGBColor(0.f, 0.f, 0.f), 100.0f);

	//Geometry::Material * material = new Geometry::Material(RGBColor(), RGBColor(1.0,1.0,1.0), RGBColor(), 1000.0f, RGBColor(0.5, 0.5, 0.5)*200); // Non existing material...

	Geometry::Square square(material);
	Math::Vector3f scaleV = (sb.max() - sb.min()) ;
	double scale = ::std::max(scaleV[0], scaleV[1])*2.0;
	square.scaleX(scale);
	square.scaleY(scale);
	Math::Vector3f center = (sb.min() + sb.max()) / 2.0;
	center[2] = sb.min()[2];
	square.translate(center);
	scene.add(square);
	::std::cout << "Bounding box: " << sb.min() << "/ " << sb.max() << ", scale: "<<scale<< ::std::endl;
	::std::cout << "center: " << (sb.min() + sb.max()) / 2.0 << ::std::endl;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \fn	void createGround(Geometry::Scene & scene)
///
/// \brief	Adds a sirface area light to the scene
///
/// \author	F. Lamarche, Université de Rennes 1
/// \date	03/12/2013
///
/// \param [in,out]	scene	The scene.
////////////////////////////////////////////////////////////////////////////////////////////////////
void createSurfaceLight(Geometry::Scene & scene, double value)
{
	Geometry::BoundingBox sb = scene.getBoundingBox();
	Geometry::Material * material = new Geometry::Material(RGBColor(), RGBColor(), RGBColor(), 100.0f, RGBColor(value,value,value));
	Geometry::Square square(material);
	Math::Vector3f scaleV = (sb.max() - sb.min());
	//double scale = ::std::max(scaleV[0], scaleV[1])*0.1;
	double factor = 0.5;
	square.scaleX(scaleV[0] * factor);
	square.scaleY(scaleV[1] * factor);
	Math::Vector3f center = (sb.min() + sb.max()) / 2.0;
	center[2] = sb.max()[2]*3;
	square.translate(center);
	scene.add(square);
}

/// <summary>
/// Initializes a scene in a bathroom
/// </summary>
/// <param name="scene"></param>
void initBathroom(Geometry::Scene & scene)
{
	scene.m_sceneName = "Bathroom";
	Geometry::BoundingBox box(Math::makeVector(0.0f, 0.0f, 0.0f), Math::makeVector(0.0f, 0.0f, 0.0f));
	Geometry::Loader3ds loader(m_modelDirectory + "\\Bathroom\\bathroom2.3ds", m_modelDirectory + "");
	// We remove the specular components of the materials...
	::std::vector<Geometry::Material*> materials = loader.getMaterials();
	for (auto it = materials.begin(), end = materials.end(); it != end; ++it)
	{
		//(*it)->setSpecular(RGBColor());
	}

	for (size_t cpt = 0; cpt < loader.getMeshes().size(); ++cpt)
	{
		loader.getMeshes()[cpt]->scale(10);
		scene.add(*loader.getMeshes()[cpt]);
		//box.update(*loader.getMeshes()[cpt]);
	}
	{
		Geometry::Camera camera(Math::makeVector(-1.0, 5.78, 10.23), Math::makeVector(-0.73, 0.64, -0.21), 1.0f, 1.0f, 1.0f);
		//camera.translateLocal(Math::makeVector(2000.f, 3000.f, 700.f));
		scene.setCamera(camera);
	}
	Geometry::DirectionalLight dl(Math::makeVector(0.9, 0.9, -0.5), RGBColor::white()*0.5);
	scene.add(dl);
	createGround(scene);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \fn	void initDiffuse(Geometry::Scene & scene)
///
/// \brief	Adds a Cornell Box with diffuse material on each walls to the scene. This Cornel box
/// 		contains two cubes.
///
/// \author	F. Lamarche, Université de Rennes 1
/// \date	03/12/2013
///
/// \param [in,out]	scene	The scene.
////////////////////////////////////////////////////////////////////////////////////////////////////
void initDiffuse(Geometry::Scene & scene)
{
	Geometry::Material * material = new Geometry::Material(RGBColor(), RGBColor(), RGBColor(0.95f,0.95f,0.95f), 1, RGBColor()) ;
	Geometry::Material * material2 = new Geometry::Material(RGBColor(), RGBColor(1, 1, 1), RGBColor(), 0, RGBColor()) ;
	Geometry::Material * cubeMat = new Geometry::Material(RGBColor(), RGBColor(1, 0, 0), RGBColor(), 0, RGBColor(1, 0, 0));
	//Geometry::Material * cubeMat = new Geometry::Material(RGBColor(), RGBColor(1, 0, 0), RGBColor(), 20, RGBColor(10,0 ,0 )) ;
	Geometry::Cornel geo(material2, material2, material2, material2, material2, material2) ; 

	geo.scaleX(10) ;
	geo.scaleY(10) ;
	geo.scaleZ(10) ;
	scene.add(geo) ;

	Geometry::Cube tmp(cubeMat) ;
	tmp.translate(Math::makeVector(1.5,-1.5,0.0)) ;
	scene.add(tmp) ;
	
	Geometry::Cube tmp2(material) ;
	tmp2.translate(Math::makeVector(2,1,-4)) ;
	scene.add(tmp2) ;

	
	

	// 2.2 Adds point lights in the scene 
	{
		Geometry::PointLight pointLight(Math::makeVector(0.0f, 0.f, 2.0f), RGBColor(0.5f, 0.5f, 0.5f));
		scene.add(pointLight);
	}
	{
		Geometry::PointLight pointLight2(Math::makeVector(4.f, 0.f, 0.f), RGBColor(0.5f, 0.5f, 0.5f));
		scene.add(pointLight2);
	}
	{
		Geometry::Camera camera(Math::makeVector(-4.0f, 0.0f, 0.0f), Math::makeVector(0.0f, 0.0f, 0.0f), 0.3f, 1.0f, 1.0f);
		scene.setCamera(camera);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \fn	void initDiffuse(Geometry::Scene & scene)
///
/// \brief	Adds a Cornell Box with diffuse material on each walls to the scene. This Cornel box
/// 		contains two cubes.
///
/// \author	F. Lamarche, Université de Rennes 1
/// \date	03/12/2013
///
/// \param [in,out]	scene	The scene.
////////////////////////////////////////////////////////////////////////////////////////////////////
void initSphere(Geometry::Scene & scene)
{
	Geometry::Material * material = new Geometry::Material(RGBColor(), RGBColor(), RGBColor(0.95f, 0.95f, 0.95f), 1, RGBColor());
	Geometry::Material * material2 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(0.9, 0.9, 0.9), 1000, RGBColor(1.f,0.f,1.f)*3);
	Geometry::Material * cubeMat = new Geometry::Material(RGBColor::white(), RGBColor::white(), RGBColor(), 0, RGBColor());
	Geometry::Material * lightMat1 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 0, RGBColor::white()*3);
	Geometry::Material * mat2 = new Geometry::Material(RGBColor(), RGBColor(1.0, 5.0, 1.0), RGBColor(), 0, RGBColor());
	Geometry::Material * glass = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 0, RGBColor(), "", 1, 2);

	//Geometry::Material * cubeMat = new Geometry::Material(RGBColor(), RGBColor(1.0f,0.0,0.0), RGBColor(0.0,0.0,0.0), 20.0f, RGBColor(10.0,0,0)) ;
	//Geometry::Cornel geom(material2, material2, material2, material2, material2, material2);
	//geom.translate(Math::makeVector(3.0, 3.0, 3.0));
	//geom.scale(20);
	Geometry::Geometry geo;
	geo.addSphere(Math::makeVector(10.f, 10.f, 10.f), 5, material2);
	//cubeMat->setTextureFile(m_modelDirectory + "\\building360.png");
	geo.addSphere(Math::makeVector(0.f, 0.f, 0.f), 2, material);
	geo.addSphere(Math::makeVector(6, -6, -6), 4, glass);
	geo.addSphere(Math::makeVector(3, 3, 3), 2, mat2);
	//geo.add(Math::makeVector(6, -6, -6), 4, lightMat1);
	scene.add(geo);
	scene.setBackgroundTexture(m_modelDirectory + "\\building360.png");
	//sscene.add(geom);
	


	// 2.2 Adds point lights in the scene 
	{
		//Geometry::PointLight pointLight(Math::makeVector(-3.f, 0.0f, 0.0f), RGBColor(3.5f, 0.f, 0.f));
		//scene.add(pointLight);
		//Geometry::PointLight pointLight2(Math::makeVector(3.0f, 0.0f, 0.0f), RGBColor(0.f, 3.5f, 0.f));
		//scene.add(pointLight2);
		//scene.add(Geometry::PointLight(Math::makeVector(0.0f, -3.0f, 0.0f), RGBColor(0.f, 2.5f, 3.5f)));
		scene.add(Geometry::PointLight(Math::makeVector(0.0f, 3.0f, 0.0f), RGBColor(2.5, 2.5f, 2.5f)));
		scene.add(Geometry::PointLight(Math::makeVector(0.0f, -500.0f, 0.0f), RGBColor(0.f, 100.f, 100.f)));
	}
	{
		Geometry::Camera camera(Math::makeVector(-5.0f, 0.0f, 0.0f), Math::makeVector(0.0f, 0.0f, 0.0f), 0.3f, 1.0f, 1.0f);
		scene.setCamera(camera);
	}
}
void initSurfacique(Geometry::Scene & scene)
{
	scene.m_sceneName = "Surfacique";
	Geometry::Material * material = new Geometry::Material(RGBColor(), RGBColor(), RGBColor(0.95f, 0.95f, 0.95f), 1, RGBColor());
	Geometry::Material * material2 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 3.f, RGBColor());
	Geometry::Material * glass = new Geometry::Material(RGBColor(), RGBColor(), RGBColor(), 20.f, RGBColor(), "",1,RGBColor::red(), 10);
	Geometry::Material * m5 = new Geometry::Material(RGBColor(), RGBColor(1.0, 0.0, 0.0), RGBColor(), 1.f, RGBColor());
	Geometry::Material * m6 = new Geometry::Material(RGBColor(), RGBColor(0.0, 1.0, 0.0), RGBColor(), 1.f, RGBColor());
	Geometry::Material * cubeMat = new Geometry::Material(RGBColor(), RGBColor(1, 0.0, 0.0), RGBColor(), 200.f, RGBColor());
	Geometry::Material * lightMat1 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 0.5f, RGBColor(1., 1., 1.0)*0.);
	Geometry::Material * lightMat2 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 0, RGBColor::white()*.5);
	Geometry::Material * lightMat3 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(.8,.8,.8), 20.0f, RGBColor());
	lightMat1->setTextureFile( m_modelDirectory + "\\earth.png");
	//lightMat2->setTextureFile(m_modelDirectory + "\\TibetHouse\\3d69c2de.png");
	//Geometry::Material * cubeMat = new Geometry::Material(RGBColor(), RGBColor(1.0f,0.0,0.0), RGBColor(0.0,0.0,0.0), 20.0f, RGBColor(10.0,0,0)) ;
	Geometry::Cornel geo(material2, material2, material2, material2, m5, m6);
	//Geometry::Cornel geo(material, material, material, material, material, material);

	geo.scaleX(10);
	geo.scaleY(10);
	geo.scaleZ(10);
	scene.add(geo);

	Geometry::Cube tmp(glass);
	tmp.rotate(Math::Quaternion<double>(Math::makeVector(0.0f, 0.0f, 1.0f), 2));
	tmp.translate(Math::makeVector(-0.4, -0.4, -1.0));
	tmp.scale(4);
	scene.add(tmp);


	Geometry::Cube tmp2(cubeMat);
	tmp2.translate(Math::makeVector(2, 1, -4));
	//scene.add(tmp2);
	Geometry::Geometry g;
	g.addSphere(Math::makeVector(2.5, 0., -5. / 3.), 2., lightMat1);
	scene.add(g);
	Geometry::Geometry geom;
	geom.addSphere(Math::makeVector(1.,  3., -11./3.), 4. / 3., glass);
	geom.addAnnulus(Math::makeVector(1., 0., 4.95), Math::makeVector(0., 0., -1.), 1.5, 0.8, lightMat2);
	//geom.addSphere(Math::makeVector(0, 2, 0), 1, lightMat2);
	geom.addSphere(Math::makeVector(1., -3., -0./3.), 4./3., lightMat3);
	//geom.scale(1);
	scene.add(geom);

	{
		Geometry::PointLight pointLight(Math::makeVector(0.0f, 0.f, 2.0f), RGBColor(2.f, 2.f, 2.f));
		scene.add(pointLight);
	}
	{
		Geometry::Camera camera(Math::makeVector(-4.0f, 0.0f, 0.0f), Math::makeVector(0.0f, 0.0f, 0.0f), 0.3f, 1.0f, 1.0f);
		scene.setCamera(camera);
	}
}

void initTransparent(Geometry::Scene & scene)
{
	scene.m_sceneName = "Transparent";
	Geometry::Material * material1 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 20.f, RGBColor());
	Geometry::Material * material2 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 1.f, RGBColor());
	Geometry::Material * material3 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 1.f, RGBColor());
	material3->setTextureFile(m_textureDirectory + "\\text_damier.png");

	Geometry::Material * glass = Geometry::Material::glass(RGBColor(0.7,0.6,1), 10);
	glass->m_transmitance = 0.9;
	Geometry::Material * transp = Geometry::Material::glass();
	//transp->setNormalMapFile(m_textureDirectory + "\\lapis_nm.png");
	//transp->setTextureFile(m_textureDirectory + "\\lapis.png");
	//transp->setEmissive(RGBColor(0.3, 0.3, 0.6)*1);
	//transp->m_transparency = RGBColor(0.8, .8, 1.0);
	RGBColor diff = RGBColor::red();
	RGBColor spec = RGBColor::white();
	RGBColor trans = RGBColor::cyan();
	double density = 2.5;
	double shininess = 10000;
	double t = 1;
	Geometry::Material * g10 = new Geometry::Material(RGBColor(), diff, spec, shininess, RGBColor(), "", 1*t, trans, density);
	Geometry::Material * g09 = new Geometry::Material(RGBColor(), diff, spec, shininess, RGBColor(), "", .9*t, trans, density);
	Geometry::Material * g08 = new Geometry::Material(RGBColor(), diff, spec, shininess, RGBColor(), "", .8*t, trans, density);
	Geometry::Material * g07 = new Geometry::Material(RGBColor(), diff, spec, shininess, RGBColor(), "", .7*t, trans, density);
	Geometry::Material * g06 = new Geometry::Material(RGBColor(), diff, spec, shininess, RGBColor(), "", .6*t, trans, density);
	Geometry::Material * g05 = new Geometry::Material(RGBColor(), diff, spec, shininess, RGBColor(), "", .5*t, trans, density);
	Geometry::Material * g04 = new Geometry::Material(RGBColor(), diff, spec, shininess, RGBColor(), "", .4*t, trans, density);
	Geometry::Material * g03 = new Geometry::Material(RGBColor(), diff, spec, shininess, RGBColor(), "", .3*t, trans, density);
	Geometry::Material * g02 = new Geometry::Material(RGBColor(), diff, spec, shininess, RGBColor(), "", .2*t, trans, density);
	Geometry::Material * g01 = new Geometry::Material(RGBColor(), diff, spec, shininess, RGBColor(), "", .1*t, trans, density);
	Geometry::Material * g00 = new Geometry::Material(RGBColor(), diff, spec, shininess, RGBColor(), "", 0*t, trans, density);


	Geometry::Material * m5 = new Geometry::Material(RGBColor(), RGBColor(1.0, 0.0, 0.0), RGBColor(), 1.f, RGBColor());
	Geometry::Material * m6 = new Geometry::Material(RGBColor(), RGBColor(0.0, 1.0, 0.0), RGBColor(), 1.f, RGBColor());
	Geometry::Material * cubeMat = new Geometry::Material(RGBColor(), RGBColor(1, 1.0, 1.0), RGBColor(), 200.f, RGBColor());
	Geometry::Material * lightMat1 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 0, RGBColor::white() );
	Geometry::Material * lightMat2 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 0, RGBColor::white());
	//Geometry::Loader3ds loader(m_modelDirectory + "\\Monkey\\monkey.3ds", m_modelDirectory + "\\Monkey\\texture");
	//lightMat1->setTextureFile(m_modelDirectory + "\\TibetHouse\\3d69c2de.png");
	Geometry::Loader3ds loader(m_modelDirectory + "\\Glass\\Glass.3ds", m_modelDirectory + "\\Glass");
	//Geometry::Loader3ds loader(m_modelDirectory + "\\Diamond\\diamond.3ds", m_modelDirectory + "\\Diamond");
	//Geometry::Loader3ds loader(m_modelDirectory + "\\Katana\\katana.3ds", m_modelDirectory + "\\Katana");
	Geometry::Geometry monkeyMesh = *loader.getMeshes()[0];
	monkeyMesh.setMaterialForAllPrimitives(glass);
	monkeyMesh.scale(2);
	//monkeyMesh.translate(Math::makeVector(0.0, 2.0, 0.));
	//monkeyMesh.rotate(Math::Quaternion<double>(Math::makeVector(0.0, 0.0, 1.0), -4/2));
	monkeyMesh.rotate(Math::Quaternion<double>(Math::makeVector(0.0, 1.0, 0.0), 0.5));
	monkeyMesh.translate(Math::makeVector(0.0, -2.0, -2.5));
	//monkeyMesh.rotate(Math::Quaternion<double>(Math::makeVector(1.0, 0.0, 0.0), 3.14/3));
	//scene.add(monkeyMesh);
	Geometry::Cornel geo(material2, material2, material2, material2, m5, m6,true);
	geo.scaleX(10);
	geo.scaleY(10);
	geo.scaleZ(10);
	scene.add(geo);

	Geometry::Geometry geom;
	geom.addAnnulus(Math::makeVector(0., 0., 4.99), Math::makeVector(0., 0., -1.), 5, 4, lightMat2);
	//geom.addAnnulus(Math::makeVector(0., 3., 3.), Math::makeVector(1.1, -1., -1.), 2.5, 0, lightMat2);
	//geom.addSphere(Math::makeVector(0., 0., 4.2), 0.8, lightMat2);
	scene.add(geom);

	Geometry::Geometry poto;
	poto.addCylinder(Math::makeVector(-10., 0., 5.), Math::makeVector(-10., 0., -5.), 6, material3);
	//scene.add(poto);

	Geometry::Geometry sphere;
	sphere.addSphere(Math::makeVector(0, 0, 0), 3, glass);
	scene.add(sphere);

	//glass->setDiffuse(RGBColor(1.0, 0.0, 1.0));
	Geometry::Geometry g;
	g.addSphere(Math::makeVector(3.0, 3.0, -4.0), 1, g10);
	g.addSphere(Math::makeVector(0.0, 3.0, -4.0), 1, g09);
	g.addSphere(Math::makeVector(-3.0, 3.0, -4.0), 1, g08);
	g.addSphere(Math::makeVector(3.0, 0.0, -4.0), 1, g07);
	g.addSphere(Math::makeVector(0.0, 0.0, -4.0), 1, g06);
	g.addSphere(Math::makeVector(-3.0, 0.0, -4.0), 1, g05);
	g.addSphere(Math::makeVector(3.0, -3.0, -4.0), 1, g04);
	g.addSphere(Math::makeVector(0.0, -3.0, -4.0), 1, g03);
	g.addSphere(Math::makeVector(-3.0, -3.0, -4.0), 1, g02);
	//g.addSphere(Math::makeVector(3.0, 3.0, -4.0), 1, g01);
	//g.addSphere(Math::makeVector(3.0, 3.0, -4.0), 1, g00);
	//g.addSphere(Math::makeVector(0., 0., 0.), 1.95, glass2);
	//g.setEmissive(1);
	//scene.add(g);

	Geometry::Cube cube(m5);
	cube.translate(Math::makeVector(0, 0, -1));
	cube.scale(1.5);
	//scene.add(cube);
	{
		Geometry::PointLight pointLight(Math::makeVector(0.0f, 0.f, 2.0f), RGBColor(2.f, 2.f, 2.f));
		scene.add(pointLight);
	}
	{
		Geometry::Camera camera(Math::makeVector(19.0f, 0.f, 0.0f), Math::makeVector(0.0f, 0.0f, 0.0f), 1.5f, 1.0f, 1.0f);

		//Geometry::Camera camera(Math::makeVector(1.06f, -4.7f, 0.53f), Math::makeVector(-3.9f, 0.27f, 0.25f), 0.4f, 1.0f, 1.0f);
		scene.setCamera(camera);
	}
}

void initCaustics(Geometry::Scene & scene)
{
	scene.m_sceneName = "Caustics";
	Geometry::Material * material1 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 20.f, RGBColor());
	Geometry::Material * material2 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 1.f, RGBColor());	

	Geometry::Material * m5 = new Geometry::Material(RGBColor(), RGBColor(1.0, 0.0, 0.0), RGBColor(), 1.f, RGBColor());
	Geometry::Material * m6 = new Geometry::Material(RGBColor(), RGBColor(0.0, 1.0, 0.0), RGBColor(), 1.f, RGBColor());
	Geometry::Material * cubeMat = new Geometry::Material(RGBColor(), RGBColor(1, 1.0, 1.0), RGBColor(1,1,1), 20000.f, RGBColor());
	Geometry::Material * lightMat1 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 0, RGBColor::white());
	Geometry::Material * lightMat2 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 0, RGBColor::white());

	Geometry::Cornel geo(material2, material2, material2, material2, m5, m6, true, true);
	geo.scaleX(10);
	geo.scaleY(10);
	geo.scaleZ(10);
	scene.add(geo);

	Geometry::Geometry geom;
	geom.addAnnulus(Math::makeVector(3., 3., -2.5), Math::makeVector(1.5,1.5, 0.5), 2, 0, lightMat2);
	//scene.add(geom);

	Geometry::Geometry cyl;
	//cyl.addCylinder(Math::makeVector(0, 0, -5), Math::makeVector(0, 0, -4), 3, cubeMat);
	cyl.addCylinder(Math::makeVector(0, 0, -5), Math::makeVector(0, 0, -4), 2.9, cubeMat);
	//cyl.translate(Math::makeVector(0, 0, -5));
	
	scene.add(cyl);

	Geometry::Geometry sphere;
	sphere.addSphere(Math::makeVector(0, 0, 0), 3, Geometry::Material::glass());
	//scene.add(sphere);

	
	{
		Geometry::PointLight pointLight(Math::makeVector(0.0f, 0.f, 2.0f), RGBColor(2.f, 2.f, 2.f));
		scene.add(pointLight);
		Geometry::DirectionalLight dl(-Math::makeVector(1.5, 1.5, 0.5), RGBColor::white(), 0.0);
		scene.add(dl);
	}
	{
		//Geometry::Camera camera(Math::makeVector(19.0f, 0.f, 0.0f), Math::makeVector(0.0f, 0.0f, 0.0f), 1.5f, 1.0f, 1.0f);
		Geometry::Camera camera(Math::makeVector(4.0f, 0.0f, 0.0f), Math::makeVector(0.0f, 0.0f, 0.0f), 0.3f, 1.0f, 1.0f);
		//Geometry::Camera camera(Math::makeVector(-3.5f, -1.f, 0.0f), Math::makeVector(0.0f, 0.0f, 0.0f), 0.3f, 1.0f, 1.0f);
		scene.setCamera(camera);
	}
}

void initDemo(Geometry::Scene & scene)
{
	scene.m_sceneName = "Demo";
	Geometry::Material * material1 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 1.f, RGBColor());
	Geometry::Material * material2 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor::white(), 40.f, RGBColor());
	Geometry::Material * material3 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor::white(), 10.f, RGBColor());
	Geometry::Material * material4 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 1.f, RGBColor::white());
	//material4->setTextureFile(m_modelDirectory + "\\spirale2.png");
	Geometry::Material * glass = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor::white(), 8000, RGBColor(), "", 0.85, RGBColor::white(), 1.25);
	Geometry::Material * m5 = new Geometry::Material(RGBColor(), RGBColor(1.0, 0.2, 0.2), RGBColor(), 1.f, RGBColor());
	Geometry::Material * m6 = new Geometry::Material(RGBColor(), RGBColor(0.2, 1.0, 0.2), RGBColor(), 1.f, RGBColor());
	Geometry::Material * lightMat1 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 0, RGBColor(1.1,0.55,0.15)*5);
	Geometry::Loader3ds loader(m_modelDirectory + "\\Teapot\\teapot.3ds", m_modelDirectory + "\\Teapot");
	
	Geometry::Geometry mesh = *loader.getMeshes()[0];
	mesh.setMaterialForAllPrimitives(material1);
	mesh.rotate(Math::Quaternion<double>(Math::makeVector(0, 0, 1), M_TAU*0.815));
	mesh.translate(Math::makeVector(1.5/3.0, 2.3/3., -5./3.));
	mesh.scale(3);
	scene.add(mesh);
	
	Geometry::Cornel geo(material1, material1, material1, material1, m5, m6, true);
	geo.scaleX(10);
	geo.scaleY(10);
	geo.scaleZ(10);
	scene.add(geo);
	
	Geometry::Geometry cylinder;
	cylinder.addCylinder(Math::makeVector(-2.5, -2.0, 1.0), Math::makeVector(-2.5, -2.0, -5.0), 1.8, material2);
	cylinder.addDisk(Math::makeVector(-2.5, -2.0, 1.0), Math::makeVector(0.0, 0.0, 1.), 1.8, material2);
	scene.add(cylinder);

	Geometry::Geometry capsule;
	capsule.addCylinder(Math::makeVector(0.0, 0.0, -1.0), Math::makeVector(0.0, 0.0, 1.0), 1, material4);
	//capsule.addSphere(Math::makeVector(0.0, 0.0, -1.0), 1, material1);
	//capsule.addSphere(Math::makeVector(0.0, 0.0, 1.0), 1, material1);
	//capsule.rotate(Math::Quaternion<double>(Math::makeVector(0.5, 0.5, 0.5), 1))
	//scene.add(capsule);

	Geometry::Geometry  disk;
	disk.addDisk(Math::makeVector(0.0, 0.0, 0.0), Math::makeVector(1.0, 0.0, 0.0), 2, glass);
	//scene.add(disk);
	
	Geometry::Square square(lightMat1);
	square.scale(2.5);
	square.translate(Math::makeVector(0.0, 0.0, 4.999));
	//square.translate(Math::makeVector(0.0, 0.0, 0.1));
	scene.add(square);

	Geometry::Geometry spheres;
	
	double radius = .8;
	Math::Vector3f center = Math::makeVector(2.5, -2.0, -3.0);
	spheres.addSphere(Math::makeVector(0.0,0.0,0.0), radius, glass);
	for (int i = 0; i < 5; ++i) {
		double phi = M_TAU * double(i) / 5;
		Math::Vector3f xyz = Math::makeVector(sin(phi), cos(phi),0.0);
		spheres.addSphere(xyz*(1.3*radius), radius*0.3, material3);
	}
	for (int i = 0; i < 3; ++i) {
		double phi = M_TAU * double(i) / 3;
		double theta1 = 5 * M_PI / 6;
		double theta2 = M_PI / 6;
		Math::Vector3f xyz1 = Math::makeVector(sin(phi)*sin(theta1), cos(phi)*sin(theta1), cos(theta1));
		Math::Vector3f xyz2 = Math::makeVector(sin(phi)*sin(theta2), cos(phi)*sin(theta2), cos(theta2));
		spheres.addSphere(xyz1 * (1.3*radius), radius*0.3, material3);
		spheres.addSphere(xyz2 * (1.3*radius), radius*0.3, material3);
	}
	spheres.rotate(Math::Quaternion<double>(Math::makeVector(.5, -.8, 0.0), 0.5));
	spheres.translate(center);
	scene.add(spheres);

	Geometry::Geometry geom;
	geom.addAnnulus(Math::makeVector(0., 0., 4.), Math::makeVector(0., 0., -1.), 1, 0.5, lightMat1);
	//geom.addAnnulus(Math::makeVector(0., 3., 3.), Math::makeVector(1.1, -1., -1.), 2.5, 0, lightMat2);
	//geom.addSphere(Math::makeVector(0., 0., 4.2), 0.8, lightMat2);
	//scene.add(geom);
	{
		Geometry::PointLight pointLight(Math::makeVector(0.0f, 0.f, 2.0f), RGBColor(2.f, 2.f, 2.f));
		scene.add(pointLight);
	}
	{
		Geometry::Camera camera(Math::makeVector(19.0f, 0.f, 0.0f), Math::makeVector(0.0f, 0.0f, 0.0f), 1.5f, 1.0f, 1.0f);
		scene.setCamera(camera);
	}
}
void initPrimitives(Geometry::Scene & scene)
{
	scene.m_sceneName = "Primitives";
	Geometry::Material * material1 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 1.f, RGBColor());
	Geometry::Material * caps = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 1.f, RGBColor());

	material1->setNormalMapFile(m_textureDirectory + "\\bricks_nm.png");
	Geometry::Material * material2 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor::white(), 400.f, RGBColor());
	Geometry::Material * material3 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor::white(), 1.f, RGBColor());
	Geometry::Material * material4 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 1.f, RGBColor::white());
	Geometry::Material * text_cyl = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(1,1,1), 500000.f, RGBColor());
	text_cyl->setTextureFile(m_textureDirectory + "\\stairs.png");
	text_cyl->setNormalMapFile(m_textureDirectory + "\\stairs_nm.png");
	Geometry::Material * text_sphere = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 1.f, RGBColor::white()*3);
	text_sphere->setTextureFile(m_textureDirectory + "\\earth2.png");
	Geometry::Material * text_disk = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 1.f, RGBColor::white() * 3);
	text_disk->setTextureFile(m_textureDirectory + "\\windows.png");
	
	Geometry::Material * glass = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor::white(), 8000, RGBColor(), "", 0.85, RGBColor::white(), 1.25);
	Geometry::Material * m5 = new Geometry::Material(RGBColor(), RGBColor(1.0, 0.2, 0.2), RGBColor(), 1.f, RGBColor());
	Geometry::Material * m6 = new Geometry::Material(RGBColor(), RGBColor(0.2, 1.0, 0.2), RGBColor(), 1.f, RGBColor());
	m5->setNormalMapFile(m_textureDirectory + "\\bricks_nm.png");
	m6->setNormalMapFile(m_textureDirectory + "\\bricks_nm.png");
	Geometry::Material * lightMat1 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 0, RGBColor(1.1, 0.55, 0.15) * 5);
	Geometry::Material * light_cyl = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 0, RGBColor(1,0.5,0.5)*2);

	Geometry::Cornel geo(material1, material1, material1, material1, m5, m6, true);
	geo.scaleX(10);
	geo.scaleY(10);
	geo.scaleZ(10);
	scene.add(geo);

	Geometry::Geometry sphere;
	sphere.rotate(Math::Quaternion<double>(Math::makeVector(0, 0, 1), M_PI/2));
	sphere.addSphere(Math::makeVector(-3.5, 2.0, -2.5), 1.2, text_sphere);
	scene.add(sphere);

	Geometry::Geometry cornel;
	Geometry::Material * sol = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 1.f, RGBColor());
	Geometry::Material * plafond = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 1.f, RGBColor());
	sol->setTextureFile(m_textureDirectory + "\\concrete.png");
	sol->setNormalMapFile(m_textureDirectory + "\\concrete_nm.png");
	plafond->setTextureFile(m_textureDirectory + "\\plaster.png");
	plafond->setNormalMapFile(m_textureDirectory + "\\plaster_nm.png");
	cornel.addDisk(Math::makeVector(0., 0., -4.99), Math::makeVector(0, 0, 1), 7.5, material1);
	cornel.addDisk(Math::makeVector(0., 0., 4.99), Math::makeVector(0, 0, 1), 7.5, plafond);
	cornel.addDisk(Math::makeVector(-4.99, 0., 0.), Math::makeVector(1, 0, 0), 7.5, material1);
	cornel.addDisk(Math::makeVector(0., -4.99, 0.), Math::makeVector(0, 1, 0), 7.5, m5);
	cornel.addDisk(Math::makeVector(0., 4.99, 0.), Math::makeVector(0, 1, 0), 7.5, m6);
	//cornel.addDisk(Math::makeVector(0., 0., -4.99), Math::makeVector(0, 0, 1), 9, text_cyl);
	scene.add(cornel);

	Geometry::Geometry sphere_trans;
	sphere_trans.rotate(Math::Quaternion<double>(Math::makeVector(0, 0, 1), M_PI / 2));
	Geometry::Material * g1 = Geometry::Material::glass();
	g1->m_transparency = RGBColor(1.0, 0.8, 0.8);
	Geometry::Material * g2 = Geometry::Material::glass();
	g2->setNormalMapFile(m_textureDirectory + "\\lapis_nm.png");
	g2->m_transparency = RGBColor(.5, 0.5, 1.0);
	Geometry::Material * g3 = Geometry::Material::glass();
	g3->setNormalMapFile(m_textureDirectory + "\\bricks_nm.png");
	g3->m_transmitance = 0.7;
	Geometry::Material * g4 = Geometry::Material::glass();
	g4->setNormalMapFile(m_textureDirectory + "\\organic_nm.png");

	sphere_trans.addSphere(Math::makeVector(2.5, 3.0, -2.5), 1, g1);
	sphere_trans.addSphere(Math::makeVector(2.5, 3.0, -0.5), 1, g2);
	sphere_trans.addSphere(Math::makeVector(2.5, 3.0, 1.5), 1, g3);
	sphere_trans.addSphere(Math::makeVector(2.5, 3.0, 3.5), 1, g4);

	scene.add(sphere_trans);

	Geometry::Geometry disk;
	disk.addAnnulus(Math::makeVector(0.0, 0.0, 0.0), Math::makeVector(1.0, -1.0, 1.0), 2,1.5, text_disk);
	disk.translate(Math::makeVector(1.0, -3.0, -1.5));
	scene.add(disk);

	Geometry::Geometry cylinder;
	cylinder.addCylinder(Math::makeVector(-2.5, -2.0, 1.0), Math::makeVector(-2.5, -2.0, -5.0), 1.8, text_cyl);
	cylinder.addDisk(Math::makeVector(-2.5, -2.0, 1.0), Math::makeVector(0.0, 0.0, 1.), 1.8, material2);
	scene.add(cylinder);

	Geometry::Geometry cylinder_light;
	cylinder_light.addCylinder(Math::makeVector(0.0, 0.0, 7.0), Math::makeVector(0.0, 0.0, -6.0), 0.1, light_cyl);
	cylinder_light.rotate(Math::Quaternion<double>(Math::makeVector(0.5, 0.5, 0.5), 1));
	cylinder_light.translate(Math::makeVector(-2, 3, 1));
	scene.add(cylinder_light);

	Geometry::Geometry capsule;
	capsule.addCylinder(Math::makeVector(0.0, 0.0, -1.0), Math::makeVector(0.0, 0.0, 1.0), 1, caps);
	capsule.addSphere(Math::makeVector(0.0, 0.0, -1.0), 1, light_cyl);
	capsule.addSphere(Math::makeVector(0.0, 0.0, 1.0), 1, light_cyl);
	capsule.scale(0.5);
	capsule.rotate(Math::Quaternion<double>(Math::makeVector(0.5, 0.5, 0.5), 1));
	capsule.translate(Math::makeVector(-3.0, -2.0, 3.0));
	scene.add(capsule);


	Geometry::Geometry geom;
	geom.addAnnulus(Math::makeVector(0., 0., 4.), Math::makeVector(0., 0., -1.), 1, 0.5, lightMat1);
	//geom.addAnnulus(Math::makeVector(0., 3., 3.), Math::makeVector(1.1, -1., -1.), 2.5, 0, lightMat2);
	//geom.addSphere(Math::makeVector(0., 0., 4.2), 0.8, lightMat2);
	//scene.add(geom);
	{
		Geometry::PointLight pointLight(Math::makeVector(0.0f, 0.f, 2.0f), RGBColor(2.f, 2.f, 2.f));
		scene.add(pointLight);
	}
	{
		Geometry::Camera camera(Math::makeVector(19.0f, 0.f, 0.0f), Math::makeVector(0.0f, 0.0f, 0.0f), 1.5f, 1.0f, 1.0f);
		//Geometry::Camera camera(Math::makeVector(19.0f, 0.f, 0.0f), Math::makeVector(0.0f, 0.0f, 0.0f), 20.0f, 1.0f, 1.0f);
		scene.setCamera(camera);
	}
}

void initDisk(Geometry::Scene & scene)
{
	scene.m_sceneName = "Disk";
	Geometry::Material * material = new Geometry::Material(RGBColor(), RGBColor(0, 0, 0.0), RGBColor(0.95f, 0.95f, 0.95f), 1, RGBColor());
	Geometry::Material * material2 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 0, RGBColor());
	Geometry::Material * cubeMat = new Geometry::Material(RGBColor(), RGBColor(1, 1.0, 1.0), RGBColor(), 0.0f, RGBColor());
	Geometry::Material * lightMat1 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 0, RGBColor(1., 1., 1.0)*2);
	Geometry::Material * lightMat2 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 0, RGBColor(1.0, .5, 0.5) * 2);
	Geometry::Material * lightMat3 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(.3, .3, .3), 20.0f, RGBColor());
	
	Geometry::Cornel geo(material2, material2, material2, material2, material2, material2);
	geo.scaleX(10);
	geo.scaleY(10);
	geo.scaleZ(10);
	scene.add(geo);

	Geometry::Geometry geom;
	geom.addSphere(Math::makeVector(-2., 0.0, 0.0), 2, cubeMat);
	cubeMat->setTextureFile(m_modelDirectory + "\\earth2.png");
	lightMat1->setTextureFile(m_modelDirectory + "\\windows.png");
	//geom.addAnnulus(Math::makeVector(2, 2, 0), Math::makeVector(1.0, 0.8, 0.0), 3, 2.9, lightMat1);
	//geom.addAnnulus(Math::makeVector(2, 2, 0), Math::makeVector(1.0, 0.8, 0.0), 2, 1, lightMat1);
	geom.addAnnulus(Math::makeVector(2, 0, 0), Math::makeVector(1.0, .5, 0.5), 2, 1, lightMat1);
	//geom.addDisk(Math::makeVector(2, -3, 0), Math::makeVector(1.0, -0.4, 0.0), 1, lightMat2);
	//geom.scale(1);
	scene.add(geom);

	{
		Geometry::PointLight pointLight(Math::makeVector(0.0f, 0.f, 2.0f), RGBColor(2.f, 2.f, 2.f));
		scene.add(pointLight);
	}
	{
		Geometry::Camera camera(Math::makeVector(3.0f, 3.33f, 1.5f), Math::makeVector(-0.7f, -0.6f, -0.3f), 0.3f, 1.0f, 1.0f);
		//Geometry::Camera camera(Math::makeVector(-4.0f, 0.0f, 0.0f), Math::makeVector(0.0f, 0.0f, 0.0f), 0.3f, 1.0f, 1.0f);
		scene.setCamera(camera);
	}
}

void initCylinder(Geometry::Scene & scene)
{
	scene.m_sceneName = "Cylinder";
	Geometry::Material * material = new Geometry::Material(RGBColor(), RGBColor(0, 0, 0.0), RGBColor(0.95f, 0.95f, 0.95f), 1, RGBColor());
	Geometry::Material * material2 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 0, RGBColor());
	Geometry::Material * cubeMat = new Geometry::Material(RGBColor(), RGBColor(1, 1.0, 1.0), RGBColor(), 0.0f, RGBColor::white());
	cubeMat->setTextureFile(m_modelDirectory + "\\windows.png");
	Geometry::Material * lightMat1 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 0, RGBColor(1., 1., 1.0) *0);
	Geometry::Material * lightMat2 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 0, RGBColor(1.0, .5, 0.5) * 0);

	Geometry::Cornel geo(lightMat1, material2, material2, material2, material2, material2);
	geo.scaleX(10);
	geo.scaleY(10);
	geo.scaleZ(10);
	scene.add(geo);

	Geometry::Geometry geom;
	//geom.addSphere(Math::makeVector(-2., 0.0, 0.0), 2, cubeMat);
	//void addCylinder(const Math::Vector3f & base, const Math::Vector3f orientation, double height, double radius, Material * material)
	geom.addCylinder(Math::makeVector(0.0, 0.0, 0.0), Math::makeVector(1.5, -1.0, 1.0), 1.0, cubeMat);
	//geom.addAnnulus(Math::makeVector(3, 0, 0), Math::makeVector(1.0, .5, 0.5), 2, 1, cubeMat);
	scene.add(geom);

	{
		Geometry::PointLight pointLight(Math::makeVector(0.0f, 0.f, 2.0f), RGBColor(2.f, 2.f, 2.f));
		scene.add(pointLight);
	}
	{
		Geometry::Camera camera(Math::makeVector(3.0f, 3.33f, 1.5f), Math::makeVector(-0.7f, -0.6f, -0.3f), 0.3f, 1.0f, 1.0f);
		//Geometry::Camera camera(Math::makeVector(-4.0f, 0.0f, 0.0f), Math::makeVector(0.0f, 0.0f, 0.0f), 0.3f, 1.0f, 1.0f);
		scene.setCamera(camera);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \fn	void initSpecular(Geometry::Scene & scene)
///
/// \brief	Adds a Cornel box in the provided scene. Walls are specular and the box contains two 
/// 		cubes.
///
/// \author	F. Lamarche, Université de Rennes 1
/// \date	03/12/2013
////////////////////////////////////////////////////////////////////////////////////////////////////
void initSpecular(Geometry::Scene & scene)
{
	scene.m_sceneName = "Specular";
	Geometry::Material * material = new Geometry::Material(RGBColor(), RGBColor(0,0,0.0), RGBColor(0.7f,0.7f,0.7f), 100, RGBColor()) ;
	Geometry::Material * material2 = new Geometry::Material(RGBColor(), RGBColor(0,0,1.0f), RGBColor(0,0,0), 1000, RGBColor()) ;
	//Geometry::Material * cubeMat = new Geometry::Material(RGBColor(), RGBColor(1.0f,0.0,0.0), RGBColor(0.0,0.0,0.0), 20.0f, RGBColor(10.0,0,0)) ;
	Geometry::Material * cubeMat = new Geometry::Material(RGBColor(), RGBColor(1.0f, 0.0, 0.0), RGBColor::black(), 20.0f, RGBColor());
	Geometry::Cornel geo(material, material, material, material, material, material) ; //new Geometry::Cube(material2) ;////new TessellatedCone(4, material) ; //new Geometry::TessellatedCylinder(5, 1, 1, material) ;////////new Geometry::Cube(material) ;////; //new Geometry::Cube(material) ; //new Geometry::TessellatedCylinder(100, 2, 1, material) ; //

	geo.scaleX(10) ;
	geo.scaleY(10) ;
	geo.scaleZ(10) ;
	scene.add(geo) ;

	Geometry::Cube tmp(cubeMat) ;
	tmp.translate(Math::makeVector(1.5,-1.5,0.0)) ;
	scene.add(tmp) ;

	Geometry::Cube tmp2(cubeMat) ;
	tmp2.translate(Math::makeVector(2,1,-4)) ;
	scene.add(tmp2) ;

	// 2.2 Adds point lights in the scene 
	{
		Geometry::PointLight pointLight(Math::makeVector(0.0f, 0.f, 2.0f), RGBColor(0.5f, 0.5f, 0.5f)*5);
		scene.add(pointLight);
	}
	{
		Geometry::PointLight pointLight2(Math::makeVector(4.f, 0.f, 0.f), RGBColor(0.5f, 0.5f, 0.5f)*5);
		scene.add(pointLight2);
	}
	// Sets the camera
	{
		Geometry::Camera camera(Math::makeVector(-4.0f, 0.0f, 0.0f), Math::makeVector(0.0f, 0.0f, 0.0f), 0.3f, 1.0f, 1.0f);
		scene.setCamera(camera);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \fn	void initDiffuseSpecular(Geometry::Scene & scene)
///
/// \brief	Adds a Cornel box in the provided scene. The cornel box as diffuse and specular walls and 
/// 		contains two boxes.
///
/// \author	F. Lamarche, Université de Rennes 1
/// \date	03/12/2013
///
/// \param [in,out]	scene	The scene.
////////////////////////////////////////////////////////////////////////////////////////////////////
void initDiffuseSpecular(Geometry::Scene & scene)
{
	scene.m_sceneName = "DiffuseSpecular";
	Geometry::Material * material = new Geometry::Material(RGBColor(), RGBColor(0,0,0.0), RGBColor(0.7f,0.7f,0.7f), 100, RGBColor()) ;
	Geometry::Material * material2 = new Geometry::Material(RGBColor(), RGBColor(1,1,1.0f), RGBColor(0,0,0), 1000, RGBColor()) ;
	Geometry::Material * cubeMat = new Geometry::Material(RGBColor(), RGBColor(1.0f, 0.0, 0.0), RGBColor::black(), 20.0f, RGBColor());
	Geometry::Material * cubeMat2 = new Geometry::Material(RGBColor(), RGBColor(1.0f, 0.0, 0.0), RGBColor::black(), 20.0f, RGBColor());
	//Geometry::Material * cubeMat = new Geometry::Material(RGBColor(), RGBColor(0.0f,0.0,0.0), RGBColor(0.0,0.0,0.0), 20.0f, RGBColor(10.0,0,0)) ;
	//Geometry::Material * cubeMat2 = new Geometry::Material(RGBColor(), RGBColor(0.0f,0.0,0.0), RGBColor(0.0,0.0,0.0), 20.0f, RGBColor(0.0,10,0)) ;
	Geometry::Cornel geo(material2, material2, material, material, material, material) ; //new Geometry::Cube(material2) ;////new TessellatedCone(4, material) ; //new Geometry::TessellatedCylinder(5, 1, 1, material) ;////////new Geometry::Cube(material) ;////; //new Geometry::Cube(material) ; //new Geometry::TessellatedCylinder(100, 2, 1, material) ; //

	geo.scaleX(10) ;
	geo.scaleY(10) ;
	geo.scaleZ(10) ;
	scene.add(geo) ;


	Geometry::Cube tmp(cubeMat2) ;
	tmp.translate(Math::makeVector(1.5,-1.5,0.0)) ;
	scene.add(tmp) ;

	Geometry::Cube tmp2(cubeMat) ;
	tmp2.translate(Math::makeVector(2,1,-4)) ;
	scene.add(tmp2) ;

	// 2.2 Adds point lights in the scene 
	{
		Geometry::PointLight pointLight(Math::makeVector(0.0f, 0.f, 2.0f), RGBColor(0.5f, 0.5f, 0.5f));
		scene.add(pointLight);
	}
	{
		Geometry::PointLight pointLight2(Math::makeVector(4.f, 0.f, 0.f), RGBColor(0.5f, 0.5f, 0.5f));
		scene.add(pointLight2);
	}
	{
		Geometry::Camera camera(Math::makeVector(-4.0f, 0.0f, 0.0f), Math::makeVector(0.0f, 0.0f, 0.0f), 0.3f, 1.0f, 1.0f);
		scene.setCamera(camera);
	}
}

/// <summary>
/// Intializes a scene containing a garage.
/// </summary>
/// <param name="scene"></param>
void initGarage(Geometry::Scene & scene)
{
	scene.m_sceneName = "Garage";
	Geometry::Loader3ds loader(m_modelDirectory+"\\garage.3ds", m_modelDirectory+"");

	for (size_t cpt = 0; cpt < loader.getMeshes().size(); ++cpt)
	{
		scene.add(*loader.getMeshes()[cpt]);
	}

	// 2.2 Adds point lights in the scene 
	Geometry::BoundingBox sb = scene.getBoundingBox();
	Math::Vector3f position = sb.max();
	Geometry::PointLight light1(position, RGBColor::white()*300);
	scene.add(light1);
	position[1] = -position[1];
	Geometry::PointLight light2(position, RGBColor::white()*300);
	scene.add(light2);
	{
		Geometry::Camera camera(Math::makeVector(750.0f, -1500.f, 1000.f)*0.85f, Math::makeVector(200.0f, 0.0f, 0.0f), 0.3f, 1.0f, 1.0f);
		scene.setCamera(camera);
	}
	createGround(scene);
}

/// <summary>
/// Initializes a scene containing a guitar.
/// </summary>
/// <param name="scene"></param>
void initGuitar(Geometry::Scene & scene)
{
	scene.m_sceneName = "Guitar";
	Geometry::Loader3ds loader(m_modelDirectory+"\\guitar2.3ds", m_modelDirectory+"");

	for (size_t cpt = 0; cpt < loader.getMeshes().size(); ++cpt)
	{
		scene.add(*loader.getMeshes()[cpt]);
	}
	// 2.2 Adds point lights in the scene 
	Geometry::BoundingBox sb = scene.getBoundingBox();
	Math::Vector3f position = sb.max();
	Geometry::PointLight light1(position + Math::makeVector(0.f, 0.f, /*70.f*/100.f), RGBColor::white()*100);
	scene.add(light1);
	position[1] = -position[1];
	Geometry::PointLight light2(position+Math::makeVector(0.f,0.f,200.f), RGBColor::white()*100);
	scene.add(light2);
	{
		Geometry::Camera camera(Math::makeVector(-500., -1000., 1000.)*1.05, Math::makeVector(500.f, 0.0f, 0.0f), 0.6f, 1.0f, 1.0f);
		camera.translateLocal(Math::makeVector(100.0f, -100.f, -200.f));
		scene.setCamera(camera);
	}
	createGround(scene);
}

/// <summary>
/// Initializes a scene containing a dog
/// </summary>
/// <param name="scene"></param>
void initDog(Geometry::Scene & scene)
{
	scene.m_sceneName = "Dog";
	Geometry::Loader3ds loader(m_modelDirectory+"\\Dog\\dog.3ds", m_modelDirectory+"\\dog");

	for (size_t cpt = 0; cpt < loader.getMeshes().size(); ++cpt)
	{
		scene.add(*loader.getMeshes()[cpt]);
	}

	// 2.2 Adds point lights in the scene 
	Geometry::BoundingBox sb = scene.getBoundingBox();
	Math::Vector3f position = sb.max();
	Geometry::PointLight light1(position, RGBColor::white()*2);
	scene.add(light1);
	position[1] = -position[1];
	Geometry::PointLight light2(position, RGBColor::white()*2);
	scene.add(light2);
	{
		Geometry::Camera camera(Math::makeVector(10.f, 10.f, 6.f)*0.5, Math::makeVector(0.f, 0.0f, 2.5f), .7f, 1.0f, 1.0f);
		camera.translateLocal(Math::makeVector(-0.4f, 0.f, 0.9f));
		scene.setCamera(camera);
	}
	createGround(scene);
}

/// <summary>
/// Initializes a scene containing a temple.
/// </summary>
/// <param name="scene"></param>
void initTemple(Geometry::Scene & scene, bool watchBell = false)
{
	scene.m_sceneName = "Temple";
	Geometry::BoundingBox box(Math::makeVector(0.0f,0.0f,0.0f), Math::makeVector(0.0f, 0.0f, 0.0f));
	Geometry::Loader3ds loader(m_modelDirectory+"\\Temple\\Temple of St Seraphim of Sarov N270116_2.3ds", m_modelDirectory+"\\Temple");

	for (size_t cpt = 0; cpt < loader.getMeshes().size(); ++cpt)
	{
		//loader.getMeshes()[cpt]->translate(Math::makeVector(20.f, 0.f, 40.0f));
		Geometry::Geometry mesh = *loader.getMeshes()[cpt];
		for (Geometry::Primitive * prim : mesh.getPrimitives())
		{
			prim->material()->setAmbient(0);
			prim->material()->setShininess(300);
		}

		scene.add(mesh);
		box.update(mesh);
	}

	// 2.2 Adds point lights in the scene 
	Geometry::BoundingBox sb = scene.getBoundingBox();
	Math::Vector3f position = sb.max();
	//Geometry::PointLight light1(position, RGBColor::white()*60.0);
	Geometry::PointLight light1(position, RGBColor::white()*100.0);
	//scene.add(light1);
	position[1] = -position[1];
	//Geometry::PointLight light2(position, RGBColor::white()*30);
	Geometry::PointLight light2(position, RGBColor::white() * 50);
	//scene.add(light2);
	{
		Geometry::Camera camera;
		if (watchBell)
		{
			camera = Geometry::Camera(Math::makeVector(55.7, 0.0, -16.4), Math::makeVector(-0.91, 0.0, -0.4), 0.3f, 1.0f, 1.0f); // Caméra qui se positionne au mauvais endroit pourtant :/
		}
		else {
			camera = Geometry::Camera(Math::makeVector(20.0f, -100.0f, 15.0f), Math::makeVector(-20.f, 0.f, -40.f), 0.3f, 1.0f, 1.0f);
			camera.translateLocal(Math::makeVector(40.f, 0.f, 0.f));
		}
		scene.setCamera(camera);
	}
	createGround(scene);
	createSurfaceLight(scene, 100);
	//createSurfaceLigth(scene, 500);
}

/// <summary>
/// Initializes a scene containing a robot.
/// </summary>
/// <param name="scene"></param>
void initRobot(Geometry::Scene & scene)
{
	scene.m_sceneName = "Robot";
	Geometry::BoundingBox box(Math::makeVector(0.0f, 0.0f, 0.0f), Math::makeVector(0.0f, 0.0f, 0.0f));
	Geometry::Loader3ds loader(m_modelDirectory+"\\Robot.3ds", m_modelDirectory+"");

	for (size_t cpt = 0; cpt < loader.getMeshes().size(); ++cpt)
	{
		//loader.getMeshes()[cpt]->translate(Math::makeVector(20.f, 0.f, 40.0f));
		scene.add(*loader.getMeshes()[cpt]);
		box.update(*loader.getMeshes()[cpt]);
	}

	// 2.2 Adds point lights in the scene 
	Geometry::BoundingBox sb = scene.getBoundingBox();
	Math::Vector3f position = sb.max();
	Geometry::PointLight light1(position, RGBColor::white()*60.0);
	scene.add(light1);
	position[1] = -position[1];
	Geometry::PointLight light2(position, RGBColor::white() * 30);
	scene.add(light2);
	{
		Geometry::Camera camera(Math::makeVector(100.0f, -50.0f, 0.0f), Math::makeVector(0.f, 0.f, -20.f), 0.3f, 1.0f, 1.0f);
		camera.translateLocal(Math::makeVector(0.f, 40.f, 50.f));
		scene.setCamera(camera);
	}
	createGround(scene);
}

/// <summary>
/// Initializes a scene containing a grave stone
/// </summary>
/// <param name="scene"></param>
void initGraveStone(Geometry::Scene & scene)
{
	scene.m_sceneName = "Gravestone";
	Geometry::BoundingBox box(Math::makeVector(0.0f, 0.0f, 0.0f), Math::makeVector(0.0f, 0.0f, 0.0f));
	Geometry::Loader3ds loader(m_modelDirectory+"\\gravestone\\GraveStone.3ds", m_modelDirectory+"\\gravestone");

	::std::vector<Geometry::Material*> materials = loader.getMaterials();
	for (auto it = materials.begin(), end = materials.end(); it != end; ++it)
	{
		//(*it)->setSpecular(RGBColor());
		//(*it)->setSpecular((*it)->getSpecular()*0.05);
	}

	for (size_t cpt = 0; cpt < loader.getMeshes().size(); ++cpt)
	{
		//loader.getMeshes()[cpt]->translate(Math::makeVector(20.f, 0.f, 40.0f));
		scene.add(*loader.getMeshes()[cpt]);
		box.update(*loader.getMeshes()[cpt]);
	}

	// 2.2 Adds point lights in the scene 
	Geometry::BoundingBox sb = scene.getBoundingBox();
	Math::Vector3f position = sb.max();
	Geometry::PointLight light1(position, RGBColor::white()*1500*0.1);
	scene.add(light1);
	position[1] = -position[1];
	Geometry::PointLight light2(position, RGBColor::white() * 1000*0.2);
	scene.add(light2);
	{
		Geometry::Camera camera(Math::makeVector(0.f, -300.0f, 200.0f), Math::makeVector(0.f, 0.f, 60.f), 0.3f, 1.0f, 1.0f);
		camera.translateLocal(Math::makeVector(0.f, 80.f, 120.f));
		scene.setCamera(camera);
	}
	createGround(scene);
	//createSurfaceLigth(scene, 400);
}

/// <summary>
/// Initializes a scene containing a boat
/// </summary>
/// <param name="scene"></param>
void initBoat(Geometry::Scene & scene)
{
	scene.m_sceneName = "Boat";
	Geometry::BoundingBox box(Math::makeVector(0.0f, 0.0f, 0.0f), Math::makeVector(0.0f, 0.0f, 0.0f));
	Geometry::Loader3ds loader(m_modelDirectory+"\\Boat\\boat.3ds", m_modelDirectory+"\\boat");
	//// We remove the specular components of the materials...
	::std::vector<Geometry::Material*> materials = loader.getMaterials();
	for (auto it = materials.begin(), end = materials.end(); it != end; ++it)
	{
		//(*it)->setSpecular(RGBColor());
	}

	for (size_t cpt = 0; cpt < loader.getMeshes().size(); ++cpt)
	{
		//loader.getMeshes()[cpt]->translate(Math::makeVector(20.f, 0.f, 40.0f));
		scene.add(*loader.getMeshes()[cpt]);
		box.update(*loader.getMeshes()[cpt]);
	}

	// 2.2 Adds point lights in the scene 
	Geometry::BoundingBox sb = scene.getBoundingBox();
	Math::Vector3f position = sb.max();
	Geometry::PointLight light1(position, RGBColor::white()*3000);
	scene.add(light1);
	position[1] = -position[1];
	Geometry::PointLight light2(position, RGBColor::white() * 8000);
	scene.add(light2);
	{
		Geometry::Camera camera(Math::makeVector(5000.f, 5000.f, 200.0f), Math::makeVector(0.f, 0.f, 60.f), 0.3f, 1.0f, 1.0f);
		camera.translateLocal(Math::makeVector(2000.f, 3000.f, 700.f));
		scene.setCamera(camera);
	}
	createGround(scene);
}

/// <summary>
/// Initializes a scene containing a tibet house
/// </summary>
/// <param name="scene"></param>
void initTibetHouse(Geometry::Scene & scene)
{
	scene.m_sceneName = "TibetHouse";
	Geometry::BoundingBox box(Math::makeVector(0.0f, 0.0f, 0.0f), Math::makeVector(0.0f, 0.0f, 0.0f));
	Geometry::Loader3ds loader(m_modelDirectory+"\\TibetHouse\\TibetHouse.3ds", m_modelDirectory+"\\TibetHouse");
	// We remove the specular components of the materials... and add surface light sources :)
	::std::vector<Geometry::Material*> materials = loader.getMaterials();
	
	for (auto it = materials.begin(), end = materials.end(); it != end; ++it)
	{
		//(*it)->setSpecular(RGBColor());
		if ((*it)->getTextureFile() == m_modelDirectory + "\\TibetHouse" + "\\3D69C2DE.png")
		{
			(*it)->setEmissive(RGBColor::white()*10.0);
		}
	}

	for (size_t cpt = 0; cpt < loader.getMeshes().size(); ++cpt)
	{
		Geometry::Geometry g = *loader.getMeshes()[cpt];
		//loader.getMeshes()[cpt]->translate(Math::makeVector(20.f, 0.f, 40.0f));
		scene.add(g);
		
		box.update(*loader.getMeshes()[cpt]);
	}

	// 2.2 Adds point lights in the scene 
	Geometry::BoundingBox sb = scene.getBoundingBox();
	Math::Vector3f position = sb.max();
	Geometry::PointLight light1(position, RGBColor::white() *20/**50.0*/);
	scene.add(light1);
	position[1] = -position[1];
	Geometry::PointLight light2(position, RGBColor::white() *20 /** 200*/);
	scene.add(light2);
	Geometry::PointLight light3(Math::makeVector(5.f, 35.f, 5.f), RGBColor::white() *20/* * 200*/); //*50
	scene.add(light3);
	{
		Geometry::Camera camera(Math::makeVector(20.f, 0.f, 0.0f), Math::makeVector(5.f, 35.f, 0.f), 0.3f, 1.0f, 1.0f);
		camera.translateLocal(Math::makeVector(0.f, 5.f, 0.f)/*+Math::makeVector(0.0,5.0,0.0)*/);
		scene.setCamera(camera);
	}
	createGround(scene);
}

/// <summary>
/// Initializes a scene containing a tibet house. Camera is placed inside the house.
/// </summary>
/// <param name="scene"></param>
void initTibetHouseInside(Geometry::Scene & scene)
{
	scene.m_sceneName = "TibetHouseInside";
	Geometry::BoundingBox box(Math::makeVector(0.0f, 0.0f, 0.0f), Math::makeVector(0.0f, 0.0f, 0.0f));
	Geometry::Loader3ds loader(m_modelDirectory+"\\TibetHouse\\TibetHouse.3ds", m_modelDirectory+"\\TibetHouse");
	// We remove the specular components of the materials... and add surface light sources :)
	::std::vector<Geometry::Material*> materials = loader.getMaterials();
	for (auto it = materials.begin(), end = materials.end(); it != end; ++it)
	{
		//(*it)->setSpecular(RGBColor());
		if ((*it)->getTextureFile() == m_modelDirectory + "\\TibetHouse" + "\\3D69C2DE.png")
		{
			(*it)->setEmissive(RGBColor::white()*500.0);
		}
	}

	for (size_t cpt = 0; cpt < loader.getMeshes().size(); ++cpt)
	{
		//loader.getMeshes()[cpt]->translate(Math::makeVector(20.f, 0.f, 40.0f));
		scene.add(*loader.getMeshes()[cpt]); 
		box.update(*loader.getMeshes()[cpt]);
	}

	// 2.2 Adds point lights in the scene 
	Geometry::BoundingBox sb = scene.getBoundingBox();
	Math::Vector3f position = sb.max();
	Geometry::PointLight light1(position, RGBColor::white()*100.0);
	scene.add(light1);
	position[1] = -position[1];
	Geometry::PointLight light2(position, RGBColor::white() *50);
	scene.add(light2);
	Geometry::PointLight light3(Math::makeVector(5.f, 35.f, 5.f), RGBColor::white() * 10);
	scene.add(light3);
	{
		Geometry::Camera camera(Math::makeVector(20.f, 0.f, 5.0f), Math::makeVector(5.f, 35.f, 5.f), 0.3f, 1.0f, 1.0f);
		camera.translateLocal(Math::makeVector(10.f-2, 30.f, 0.f));
		scene.setCamera(camera);
	}
	createGround(scene);
}

/// <summary>
/// Initializes a scene containing a medieval city
/// </summary>
/// <param name="scene"></param>
void initMedievalCity(Geometry::Scene & scene)
{
	scene.m_sceneName = "MedievaCity";
	Geometry::BoundingBox box(Math::makeVector(0.0f, 0.0f, 0.0f), Math::makeVector(0.0f, 0.0f, 0.0f));
	Geometry::Loader3ds loader(m_modelDirectory+"\\Medieval\\MedievalCity.3ds", m_modelDirectory+"\\Medieval\\texture");
	// We remove the specular components of the materials...
	::std::vector<Geometry::Material*> materials = loader.getMaterials();
	for (auto it = materials.begin(), end = materials.end(); it != end; ++it)
	{
		//(*it)->setSpecular(RGBColor());
	}

	for (size_t cpt = 0; cpt < loader.getMeshes().size(); ++cpt)
	{
		//loader.getMeshes()[cpt]->translate(Math::makeVector(20.f, 0.f, 40.0f));
		//loader.getMeshes()[cpt]->rotate(Math::Quaternion<double>(Math::makeVector(0.0, 1.0, 0.0), Math::pi));
		scene.add(*loader.getMeshes()[cpt]);
		box.update(*loader.getMeshes()[cpt]);
	}

	// 2.2 Adds point lights in the scene 
	Geometry::BoundingBox sb = scene.getBoundingBox();
	Math::Vector3f position = sb.max();
	Geometry::DirectionalLight Dl = Geometry::DirectionalLight(Math::makeVector(0.5, 0.5, -1.0), RGBColor(1., .6, .3)*0.5);
	scene.add(Dl);
	/*Geometry::PointLight light1(position+Math::makeVector(0.0,0.0,150.0), RGBColor(1.0, 0.6, 0.3)*200.0);
	scene.add(light1);
	position[1] = -position[1];
	Geometry::PointLight light2(position + Math::makeVector(0.0, 0.0, 1000.0), RGBColor::white() * 100);
	scene.add(light2);*/
	//Geometry::PointLight light3(Math::makeVector(5.f, 35.f, 5.f), RGBColor::white() * 50);
	//scene.add(light3);
	{
		//Geometry::Camera camera(Math::makeVector(0.f, 200.f, 800.0f), Math::makeVector(0.0, 0.0, 0.0), 0.3f, 1.0f, 1.0f);
		Geometry::Camera camera(Math::makeVector(0.f, 300.f, 1000.0f), Math::makeVector(0.0,0.0,0.0), 0.3f, 1.0f, 1.0f);
		camera.translateLocal(Math::makeVector(0.0, 800., -100.0));
		//camera.translateLocal(Math::makeVector(0.0, 900., -110.0));
		scene.setCamera(camera);
	}
	Geometry::Geometry geo;
	Geometry::Material * lightMat = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 20.0f, RGBColor(1.5, 1., 1.)*200);
	Geometry::Material * lightMat2 = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor(), 20.0f, RGBColor(1., 1.5, 1.) * 200);

	geo.addSphere(Math::makeVector(0, 0, 500), 100, lightMat);
	geo.addSphere(Math::makeVector(0, 200, 500), 100, lightMat2);

	//scene.add(geo);
	createGround(scene);
}

/// <summary>
/// Initializes a scene containing a sombrero
/// </summary>
/// <param name="scene"></param>
void initSombrero(Geometry::Scene & scene)
{
	scene.m_sceneName = "Sombrero";
	Geometry::BoundingBox box(Math::makeVector(0.0f, 0.0f, 0.0f), Math::makeVector(0.0f, 0.0f, 0.0f));
	Geometry::Loader3ds loader(m_modelDirectory+"\\sombrero\\sombrero.3ds", m_modelDirectory+"\\sombrero");
	// We remove the specular components of the materials...
	::std::vector<Geometry::Material*> materials = loader.getMaterials();
	for (auto it = materials.begin(), end = materials.end(); it != end; ++it)
	{
		//(*it)->setSpecular(RGBColor()); 
	}

	for (size_t cpt = 0; cpt < loader.getMeshes().size(); ++cpt)
	{
		scene.add(*loader.getMeshes()[cpt]);
		//box.update(*loader.getMeshes()[cpt]);
	}

	// 2.2 Adds point lights in the scene 
	Geometry::BoundingBox sb = scene.getBoundingBox();
	Math::Vector3f position = sb.max();
	Geometry::PointLight light1(position, RGBColor::white()*50.0);
	//scene.add(light1);
	position[1] = -position[1];
	Geometry::PointLight light2(position, RGBColor::white() * 50);
	//scene.add(light2);
	{
		Geometry::Camera camera(Math::makeVector(300.f, 0.f, 100.0f), Math::makeVector(0.f, 0.f, 0.f), 0.3f, 1.0f, 1.0f);
		//camera.translateLocal(Math::makeVector(2000.f, 3000.f, 700.f));
		scene.setCamera(camera);
	}
	Geometry::DirectionalLight dl(Math::makeVector(0.9,0.9,-0.5), RGBColor::white()*0.5);
	scene.add(dl);
	createGround(scene);
}

void initCorridor(Geometry::Scene & scene)
{
	scene.m_sceneName = "Corridor";
	Geometry::Geometry Geo;
	Geometry::Material * mat = new Geometry::Material(RGBColor(), RGBColor::white(), RGBColor::white()*0, 1, RGBColor());
	mat->setTextureFile(m_textureDirectory + "\\alien.png");
	mat->setNormalMapFile(m_textureDirectory + "\\alien_nm.png");
	Geo.addAnnulus(Math::makeVector(30, 0, 30), Math::makeVector(0, 0, 1), 15, 0, mat);
	Geo.addSphere(Math::makeVector(60, 0, 30), 10, mat);
	Geo.addCylinder(Math::makeVector(0, 0, 30), Math::makeVector(0, 0, 40), 10, mat);
	//scene.add(Geo);
	Geometry::BoundingBox box(Math::makeVector(0.0f, 0.0f, 0.0f), Math::makeVector(0.0f, 0.0f, 0.0f));
	Geometry::Loader3ds loader(m_modelDirectory + "\\Corridor\\couloir.3ds", m_modelDirectory + "\\Corridor\\texture");
	// We remove the specular components of the materials...
	::std::vector<Geometry::Material*> materials = loader.getMaterials();
	Geometry::Material * orange = materials[0];
	Geometry::Material * blanc = materials[1];
	blanc->setSpecular(RGBColor());
	blanc->setShininess(1.0f);
	//blanc->setAmbient(blanc->getDiffuse()*0.1);
	orange->setSpecular(RGBColor());
	orange->setShininess(0.1f);
	//orange->setAmbient(orange->getDiffuse()*0.03);


	for (size_t cpt = 0; cpt < loader.getMeshes().size(); ++cpt)
	{
		Geometry::Geometry G = *loader.getMeshes()[cpt];
		G.scale(50);
		//G.setMaterialForAllPrimitives(mat);
		scene.add(G);
		box.update(*loader.getMeshes()[cpt]);
	}
	

	Geometry::DirectionalLight dl(Math::makeVector(0.0, 0.5, -0.8), RGBColor(1.0, .8, 0.6)*2,0.2);
	scene.add(dl);

	{
		Geometry::Camera camera(Math::makeVector(48.0, -2.0, 3.0), Math::makeVector(1.0, 0.0, 0.0), 0.8f, 1.0f, 1.0f);
		scene.setCamera(camera);
		scene.setBackgroundTexture(m_textureDirectory + "\\building360.png");
		//scene.setBackgroundTexture(m_modelDirectory + "\\bridge.png");
		//scene.setBackgroundTexture(m_modelDirectory + "\\ponton.png");

	}

}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \fn	void waitKeyPressed()
///
/// \brief	Waits until a key is pressed.
/// 		
/// \author	F. Lamarche, Université de Rennes 1
/// \date	03/12/2013
////////////////////////////////////////////////////////////////////////////////////////////////////
void waitKeyPressed()
{
  SDL_Event event;
  bool done = false;
  while( !done ) {
    while ( SDL_PollEvent(&event) ) {
      switch (event.type) {
        case SDL_KEYDOWN:
        /*break;*/
        case SDL_QUIT:
          done = true;
        break;
        default:
        break;
      }
    }/*while*/
  }/*while(!done)*/
}

std::vector<double> lerp(double pos1, double pos2, double nb_steps) {
	std::vector<double> res(nb_steps);
	double step = (pos2 - pos1) / nb_steps;
	for (int i = 0; i < nb_steps; ++i) {
		res[i] = pos1 + pos2 * step * i;
	}
}

std::vector<Math::Vector3f> lerp(Math::Vector3f pos1, Math::Vector3f pos2, double nb_steps) {
	std::vector<Math::Vector3f> res(nb_steps);
	Math::Vector3f step = (pos2 - pos1) / nb_steps;
	for (int i = 0; i < nb_steps; ++i) {
		res[i] = pos1 + pos2 * step * i;
	}
}


////////////////////////////////////////////////////////////////////////////////////////////////////
/// \fn	int main(int argc, char ** argv)
///
/// \brief	/*/*Main*/*/ entry-point for this application.
///
/// \author	F. Lamarche, Université de Rennes 1
/// \date	03/12/2013
///
/// \param	argc	Number of command-line arguments.
/// \param	argv	Array of command-line argument strings.
///
/// \return	Exit-code for the process - 0 for success, else an error code.
////////////////////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char ** argv)
{
	omp_set_num_threads(8);

	// 1 - Initializes a window for rendering
	const int n = pow(2, 9); // Pour utiliser le parcours de hilbert
	const int side = 1000;
	Visualizer::Visualizer visu(side, side);
	//Visualizer::Visualizer visu(n, n); // Pour utiliser le parcours de Hilbert

	// 2 - Initializes the scene
	Geometry::Scene scene(&visu);

	float moveSpeed = 1.f; // 5 for medieval 15 for guitare
	// 2.1 initializes the0. geometry (choose only one initialization)
	//initBathroom(scene);
	//initSurfacique(scene); moveSpeed = 0.4f;
	//initTransparent(scene); moveSpeed = 0.1f;
	//initCaustics(scene); moveSpeed = 0.1f;
	//initDemo(scene); moveSpeed = 0.1f;
	//initPrimitives(scene); moveSpeed = 0.1f;
	//initCylinder(scene); moveSpeed = 0.1f;
	//initSphere(scene);
	//initDisk(scene);
	//initDiffuse(scene);
	//initDiffuseSpecular(scene);
	//initSpecular(scene);
	//initGuitar(scene); moveSpeed = 15.f;
	//initDog(scene); 
	//initGarage(scene); moveSpeed = 30.f;
	//initTemple(scene, true); // Pour afficher la cloche
	//initRobot(scene);
	//initGraveStone(scene); moveSpeed = 30.f;
	//initBoat(scene); moveSpeed = 30.f;
	//initSombrero(scene); moveSpeed = 5.0f;
	//initTibetHouse(scene);
	//initTibetHouseInside(scene);
	//initMedievalCity(scene); moveSpeed = 5.f;
	initCorridor(scene);

	// Shows stats
	scene.printStats();

	// 3 - Computes the scene
	unsigned int passPerPixel = 64 / 8;	// Number of rays per pixel 
	unsigned int subPixelSampling = 4;	// Antialiasing
	scene.m_absorption = 0.0;
	unsigned int maxBounce = 5;
	//unsigned int maxBounce = 2;			// Maximum number of bounces

	//scene.setDiffuseSamples(16);
	//scene.setSpecularSamples(16);
	scene.setDiffuseSamples(1);
	scene.setSpecularSamples(1);
	//scene.setDiffuseSamples(32);
	//scene.setSpecularSamples(32);
	//scene.setDiffuseSamples(16);
	//scene.setSpecularSamples(16);
	//scene.setDiffuseSamples(4);
	//scene.setSpecularSamples(4);
	scene.setLightSamples(32);
	::chrono::high_resolution_clock::time_point tmp = ::chrono::high_resolution_clock::now();
	// Commenter les deux lignes pour ne pas utiliser de structure acceleratrice
	// Sinon, decommenter une unique ligne
	scene.computeTree_SAH(.5,1,8); // 8 Decommenter pour utiliser le BVH SAH
	//scene.computeTree_median(1); // 16 Decommenter pour utiliser le BVH median
	const ::chrono::duration<double, std::ratio<1, 1>> timeSinceLastFrame = ::chrono::high_resolution_clock::now() - tmp;
	::std::cout << "Temps generation arbre : " << timeSinceLastFrame .count() << " secondes" << ::std::endl; // 20s sans multi

	double facteur = 1.f;

	bool hilbert = false; // True pour utiliser le parcours de hilbert
	bool heatmap = false; // True pour rendre la heatmap
	bool renderEmissive = true;
	bool isNoise = false;
	int stratification = 1;
	scene.m_indirect = true;
	
	//scene.compute(maxBounce, subPixelSampling, passPerPixel, heatmap, hilbert, renderEmissive, isNoise, stratification);
	scene.computeRealTime(maxBounce, subPixelSampling, facteur, moveSpeed, heatmap, renderEmissive, isNoise, stratification); //10h
	//for (int i = 0; i < 100; ++i) {
	//	scene.m_sceneName = "_" +  scene.m_sceneName;
	//	Geometry::Camera camera(Math::makeVector(1.06f, -4.7f, 0.53f), Math::makeVector(-3.9f, 0.27f, 0.25f), 0.4f, 1.0f, 1.0f);

	//	scene.m_camera.setTarget(Math::makeVector(-3.9f, 0.27f, 0.25f));
	//	scene.computeRealTime(maxBounce, subPixelSampling, facteur, moveSpeed, heatmap, renderEmissive, isNoise, stratification, 100); //10h
	//	scene.m_camera.translate(Math::makeVector(0., 0.09, 0.02));
	//}
	

	// 4 - waits until a key is pressed
	waitKeyPressed();

	return 0;
}